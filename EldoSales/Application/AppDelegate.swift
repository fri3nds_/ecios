//
//  AppDelegate.swift
//  EldoSales
//
//  Created by Nikita Bondar on 14.06.2021.
//

import PromiseKit
import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    // MARK: - Properties

    let moduleFactory: ModuleFactory = MainModuleFactory(services: Services(),
                                                         storage: .standard,
                                                         theme: MainTheme(colors: MainColors(), images: MainImages(), fonts: MainFonts()))
    var window: UIWindow?

    // MARK: - Interface

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        PromiseKit.conf.Q = (map: .global(), return: .main)

        if #available(iOS 13.0, *) {} else {
            let childController = moduleFactory.auth()
            let rootViewController = NavigationController(rootViewController: childController)

            rootViewController.navigationBar.prefersLargeTitles = false
            rootViewController.navigationBar.barTintColor = moduleFactory.theme.colors.navigation
            rootViewController.navigationBar.tintColor = moduleFactory.theme.colors.background
            rootViewController.navigationBar.titleTextAttributes = [.foregroundColor: moduleFactory.theme.colors.background]
            rootViewController.navigationBar.isTranslucent = false
            rootViewController.navigationBar.shadowImage = UIImage()

            window = UIWindow()
            window?.rootViewController = rootViewController

            window?.makeKeyAndVisible()
        }
        return true
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        let rootViewController = window?.rootViewController

        if let navigationController = rootViewController as? UINavigationController {
            return navigationController.topViewController?.supportedInterfaceOrientations ?? navigationController.supportedInterfaceOrientations
        }

        return rootViewController?.supportedInterfaceOrientations ?? [.portrait, .landscapeLeft, .landscapeRight]
    }

}

