//
//  BadgesCell.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

protocol BadgesCellDelegate: CollectionCellDelegate {
    // MARK: - Interface

    func badgesCell(_ badgesCell: BadgesCell, didTap item: BadgesItem, badge: BadgeItem)
    func badgesCell(_ badgesCell: BadgesCell, item: BadgesItem, didChange contentOffset: CGPoint)
}

class BadgesCell: CollectionCell {
    // MARK: - Delegate

    private weak var storedDelegate: BadgesCellDelegate? {
        delegate as? BadgesCellDelegate
    }

    // MARK: - Properties

    private var storedItem: BadgesItem? {
        item as? BadgesItem
    }

    // MARK: - Views

    private var badgesViews: [BadgeView] = []

    private weak var scrollView: UIScrollView!

    // MARK: - Life cycle

    override func setup(with theme: Theme) {
        guard !isSetuped else { return }

        super.setup(with: theme)

        backgroundColor = .clear
        contentView.backgroundColor = theme.colors.background

        let scrollView = UIScrollView()
        scrollView.delegate = self
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        contentView.addSubview(scrollView)
        self.scrollView = scrollView
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        performLayout()
    }

    // MARK: - Interface

    override func fill(with item: CollectionItem, animated: Bool = false) {
        super.fill(with: item, animated: animated)

        guard let item = item as? BadgesItem else { return }

        reloadBadges(item.badges)
        scrollView.setContentOffset(item.contentOffset, animated: false)

        setNeedsLayout()
    }

    // MARK: - Private. Layout

    private func performLayout() {
        scrollView.pin
            .all()

        var lastBadgeView: BadgeView?

        for badgeView in badgesViews {
            badgeView.pin
                .vertically()
                .left(to: lastBadgeView == nil ? scrollView.edge.left : lastBadgeView!.edge.right)
                .marginLeft(lastBadgeView == nil ? pin.safeArea.left + 16 : 8)
                .sizeToFit(.height)

            lastBadgeView = badgeView
        }

        scrollView.contentSize = CGSize(width: (lastBadgeView?.frame.maxX ?? 0) + 16, height: frame.height)
    }

    // MARK: - Private. Help

    private func reloadBadges(_ badges: [BadgeItem]) {
        badgesViews.forEach { $0.removeFromSuperview() }
        badgesViews.removeAll()

        for badge in badges {
            let badgeView = BadgeView(theme: theme)
            badgeView.delegate = self
            scrollView.addSubview(badgeView)
            badgesViews.append(badgeView)

            badgeView.configure(badge)
        }
    }
}

extension BadgesCell: BadgeViewDelegate {
    // MARK: - Interface

    func badgeView(_ badgeView: BadgeView, didTapWith item: BadgeItem) {
        guard let storedItem = storedItem else { return }
        storedDelegate?.badgesCell(self, didTap: storedItem, badge: item)
    }
}

extension BadgesCell: UIScrollViewDelegate {
    // MARK: - Interface

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let item = storedItem else { return }
        storedDelegate?.badgesCell(self, item: item, didChange: scrollView.contentOffset)
    }
}
