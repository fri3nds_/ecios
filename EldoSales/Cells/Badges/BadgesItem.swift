//
//  BadgesItem.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class BadgesItem: CollectionItem {
    // MARK: - System

    override var cellClass: AnyClass {
        BadgesCell.self
    }

    // MARK: - Properties

    let badges: [BadgeItem]
    let contentOffset: CGPoint

    // MARK: - Init

    init(identifier: String = UUID().uuidString,
         strategy: CollectionSizingStrategy,
         badges: [BadgeItem],
         contentOffset: CGPoint,
         model: Any?) {
        self.badges = badges
        self.contentOffset = contentOffset

        super.init(identifier: identifier, strategy: strategy, model: model)
    }
}
