//
//  ButtonCell.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class ButtonCell: CollectionCell {
    // MARK: - Properties

    private var storedItem: ButtonItem? {
        item as? ButtonItem
    }

    // MARK: - Views

    private weak var button: UIButton!

    // MARK: - Life cycle

    override func setup(with theme: Theme) {
        guard !isSetuped else { return }

        super.setup(with: theme)

        backgroundColor = .clear
        contentView.backgroundColor = theme.colors.background

        let button = UIButton()
        button.addTarget(self, action: #selector(buttonDidTap(_:)), for: .touchUpInside)
        contentView.addSubview(button)
        self.button = button
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        performLayout()
    }

    // MARK: - Interface

    override func fill(with item: CollectionItem, animated: Bool = false) {
        super.fill(with: item, animated: animated)

        guard let item = item as? ButtonItem else { return }

        button.setTitle(item.title, for: .normal)
        button.setTitleColor(item.titleColor ?? theme.colors.titleColor, for: .normal)
        button.titleLabel?.font = item.font ?? theme.fonts.font16Bold

        contentView.backgroundColor = item.backgroundColor ?? theme.colors.background

        setNeedsLayout()
    }

    // MARK: - Private. Layout

    private func performLayout() {
        button.pin
            .all()
            .marginLeft(pin.safeArea.left + 16)
            .marginRight(pin.safeArea.right + 16)
    }

    // MARK: - Private. Actions

    @objc
    private func buttonDidTap(_ sender: UIButton) {
        sender.bounce()

        guard let item = item else { return }
        delegate?.collectionCell(self, didTap: item)
    }
}
