//
//  ButtonItem.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class ButtonItem: CollectionItem {
    // MARK: - System

    override var cellClass: AnyClass {
        ButtonCell.self
    }

    // MARK: - Properties

    let title: String
    let titleColor: UIColor?
    let font: UIFont?
    let backgroundColor: UIColor?

    // MARK: - Init

    init(identifier: String = UUID().uuidString,
         strategy: CollectionSizingStrategy,
         title: String,
         titleColor: UIColor? = nil,
         font: UIFont? = nil,
         backgroundColor: UIColor? = nil,
         model: Any?) {
        self.title = title
        self.titleColor = titleColor
        self.font = font
        self.backgroundColor = backgroundColor

        super.init(identifier: identifier, strategy: strategy, model: model)
    }
}
