//
//  CollectionCell.swift
//  EldoSales
//
//  Created by Nikita Bondar on 19.06.2021.
//

import UIKit

protocol CollectionCellDelegate: AnyObject {
    // MARK: - Interface

    func collectionCell(_ collectionCell: CollectionCell, didTap item: CollectionItem)
}

class CollectionCell: UICollectionViewCell {
    // MARK: - Delegate

    weak var delegate: CollectionCellDelegate?

    // MARK: - Properties

    private(set) var item: CollectionItem?
    private(set) var isSetuped: Bool = false
    private(set) var theme: Theme!

    // MARK: - Life cycle

    func setup(with theme: Theme) {
        guard !isSetuped else { return }
        isSetuped.toggle()

        self.theme = theme
    }

    // MARK: - Interface

    func fill(with item: CollectionItem, animated: Bool = false) {
        self.item = item
    }
}
