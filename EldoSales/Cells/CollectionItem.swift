//
//  CollectionItem.swift
//  EldoSales
//
//  Created by Nikita Bondar on 19.06.2021.
//

import UIKit

class CollectionItem {
    // MARK: - Static

    var reuseIdentifier: String {
        String(describing: type(of: cellClass))
    }

    var cellClass: AnyClass {
        fatalError("not implemented")
    }

    // MARK: - Properties

    let identifier: String
    let strategy: CollectionSizingStrategy
    let model: Any?

    // MARK: - Init

    init(identifier: String = UUID().uuidString, strategy: CollectionSizingStrategy, model: Any?) {
        self.identifier = identifier
        self.strategy = strategy
        self.model = model
    }

    // MARK: - Interface

    func size(in parentSize: CGSize) -> CGSize {
        strategy.size(in: parentSize)
    }
}

extension CollectionItem: Equatable {
    // MARK: - Interface

    static func ==(lhs: CollectionItem, rhs: CollectionItem) -> Bool {
        lhs.identifier == rhs.identifier
    }
}
