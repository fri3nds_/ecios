//
//  DatePickerCell.swift
//  EldoSales
//
//  Created by Nikita Bondar on 21.06.2021.
//

import UIKit

protocol DatePickerCellDelegate: CollectionCellDelegate {
    // MARK: - Interface

    func datePickerCell(_ datePickerCell: DatePickerCell, didChange date: Date)
}

class DatePickerCell: CollectionCell {
    // MARK: - Delegate

    private weak var storedDelegate: DatePickerCellDelegate? {
        delegate as? DatePickerCellDelegate
    }

    // MARK: - Properties

    private var storedItem: DatePickerItem? {
        item as? DatePickerItem
    }

    // MARK: - Views

    private weak var datePicker: DatePicker!

    // MARK: - Life cycle

    override func setup(with theme: Theme) {
        guard !isSetuped else { return }

        super.setup(with: theme)

        backgroundColor = .clear
        contentView.backgroundColor = theme.colors.background

        let datePicker = DatePicker()
        datePicker.locale = Locale(identifier: "ru")
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(datePickerDateChanged(_:)), for: .valueChanged)
        contentView.addSubview(datePicker)
        self.datePicker = datePicker
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        performLayout()
    }

    // MARK: - Interface

    override func fill(with item: CollectionItem, animated: Bool = false) {
        super.fill(with: item, animated: animated)

        guard let item = item as? DatePickerItem else { return }

        datePicker.setDate(item.date, animated: true)

        setNeedsLayout()
    }

    // MARK: - Private. Layout

    private func performLayout() {
        datePicker.setNeedsToLayoutSubviews()
        datePicker.pin.all()
    }

    // MARK: - Private. Actions

    @objc
    private func datePickerDateChanged(_ sender: DatePicker) {
        guard let item = storedItem else { return }

        item.date = sender.date
        storedDelegate?.datePickerCell(self, didChange: sender.date)
    }
}
