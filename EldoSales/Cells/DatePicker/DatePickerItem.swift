//
//  DatePickerItem.swift
//  EldoSales
//
//  Created by Nikita Bondar on 21.06.2021.
//

import UIKit

class DatePickerItem: CollectionItem {
    // MARK: - System

    override var cellClass: AnyClass {
        DatePickerCell.self
    }

    // MARK: - Properties

    var date: Date

    // MARK: - Init

    init(identifier: String = UUID().uuidString,
         strategy: CollectionSizingStrategy,
         date: Date,
         model: Any?) {
        self.date = date

        super.init(identifier: identifier, strategy: strategy, model: model)
    }
}
