//
//  DatesCell.swift
//  EldoSales
//
//  Created by Nikita Bondar on 21.06.2021.
//

import UIKit

protocol DatesCellDelegate: CollectionCellDelegate {
    // MARK: - Interface

    func datesCell(_ datesCell: DatesCell, didTap item: DatesItem, dateItem: DateItem)
}

class DatesCell: CollectionCell {
    // MARK: - Delegate

    private weak var storedDelegate: DatesCellDelegate? {
        delegate as? DatesCellDelegate
    }

    // MARK: - Properties

    private var storedItem: DatesItem? {
        item as? DatesItem
    }

    // MARK: - Views

    private weak var leftDateButton: DateButton!
    private weak var rightDateButton: DateButton!

    // MARK: - Life cycle

    override func setup(with theme: Theme) {
        guard !isSetuped else { return }

        super.setup(with: theme)

        backgroundColor = .clear
        contentView.backgroundColor = theme.colors.background

        let leftDateButton = DateButton(theme: theme)
        leftDateButton.delegate = self
        contentView.addSubview(leftDateButton)
        self.leftDateButton = leftDateButton

        let rightDateButton = DateButton(theme: theme)
        rightDateButton.delegate = self
        contentView.addSubview(rightDateButton)
        self.rightDateButton = rightDateButton
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        performLayout()
    }

    // MARK: - Interface

    override func fill(with item: CollectionItem, animated: Bool = false) {
        super.fill(with: item, animated: animated)

        guard let item = item as? DatesItem else { return }

        leftDateButton.configure(item.leftDateItem)
        rightDateButton.configure(item.rightDateItem)

        setNeedsLayout()
    }

    // MARK: - Private. Layout

    private func performLayout() {
        let leftMargin = pin.safeArea.left + 16
        let rightMargin = pin.safeArea.right + 16
        let centerMargin: CGFloat = 8

        leftDateButton.pin
            .top()
            .left()
            .marginLeft(leftMargin)
            .size(CGSize(width: contentView.frame.width / 2 - leftMargin - centerMargin, height: contentView.frame.height))

        rightDateButton.pin
            .top()
            .right()
            .marginRight(rightMargin)
            .size(CGSize(width: contentView.frame.width / 2 - rightMargin - centerMargin, height: contentView.frame.height))
    }
}

extension DatesCell: DateButtonDelegate {
    // MARK: - Interface

    func dateButton(_ dateButton: DateButton, didTapWith item: DateItem) {
        guard let storedItem = storedItem else { return }
        storedDelegate?.datesCell(self, didTap: storedItem, dateItem: item)
    }
}
