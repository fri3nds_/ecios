//
//  DatesItem.swift
//  EldoSales
//
//  Created by Nikita Bondar on 21.06.2021.
//

import UIKit

class DatesItem: CollectionItem {
    // MARK: - System

    override var cellClass: AnyClass {
        DatesCell.self
    }

    // MARK: - Properties

    let leftDateItem: DateItem
    let rightDateItem: DateItem

    // MARK: - Init

    init(identifier: String = UUID().uuidString,
         strategy: CollectionSizingStrategy,
         leftDateItem: DateItem,
         rightDateItem: DateItem,
         model: Any?) {
        self.leftDateItem = leftDateItem
        self.rightDateItem = rightDateItem
        
        super.init(identifier: identifier, strategy: strategy, model: model)
    }
}
