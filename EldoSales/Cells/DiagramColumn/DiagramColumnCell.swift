//
//  DiagramColumnCell.swift
//  EldoSales
//
//  Created by Nikita Bondar on 19.06.2021.
//

import PinLayout
import UIKit

protocol DiagramColumnCellDelegate: CollectionCellDelegate {
    // MARK: - Interface

    func diagramColumnCellDottedFrame(_ diagramColumnCell: DiagramColumnCell) -> CGRect
}

class DiagramColumnCell: CollectionCell {
    // MARK: - Delegate

    private weak var diagramDelegate: DiagramColumnCellDelegate? {
        delegate as? DiagramColumnCellDelegate
    }

    // MARK: - State

    private enum State: String {
        case idle
        case waitingForAnimation
    }

    // MARK: - Properties

    private var diagramColumnItem: DiagramColumnItem? {
        item as? DiagramColumnItem
    }

    private var state: State = .idle

    // MARK: - Views

    private weak var backgroundEmptyView: UIView!
    private weak var backgroundColorView: UIView!
    private weak var topLabel: UILabel!
    private weak var bottomLabel: UILabel!

    private weak var button: UIButton!

    // MARK: - Life cycle

    override func setup(with theme: Theme) {
        guard !isSetuped else { return }
        
        super.setup(with: theme)

        backgroundColor = .clear
        contentView.backgroundColor = .clear

        let backgroundEmptyView = UIView()
        backgroundEmptyView.backgroundColor = theme.colors.elementLightGray
        backgroundEmptyView.layer.cornerRadius = 7
        contentView.addSubview(backgroundEmptyView)
        self.backgroundEmptyView = backgroundEmptyView

        let backgroundColorView = UIView()
        backgroundColorView.backgroundColor = theme.colors.elementGreen
        backgroundColorView.layer.cornerRadius = 7
        contentView.addSubview(backgroundColorView)
        self.backgroundColorView = backgroundColorView

        let topLabel = UILabel()
        topLabel.font = theme.fonts.font14Bold
        topLabel.textColor = theme.colors.textLightGray
        topLabel.textAlignment = .center
        contentView.addSubview(topLabel)
        self.topLabel = topLabel

        let bottomLabel = UILabel()
        bottomLabel.font = theme.fonts.font10Bold
        bottomLabel.textColor = theme.colors.textGray
        bottomLabel.textAlignment = .center
        contentView.addSubview(bottomLabel)
        self.bottomLabel = bottomLabel

        let button = UIButton()
        button.addTarget(self, action: #selector(buttonDidTap(_:)), for: .touchUpInside)
        contentView.addSubview(button)
        self.button = button
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        performLayout()
    }

    // MARK: - Interface

    override func fill(with item: CollectionItem, animated: Bool = false) {
        super.fill(with: item, animated: animated)

        guard let item = item as? DiagramColumnItem else { return }

        topLabel.text = item.topTitle
        topLabel.textColor = item.topTitleColor ?? theme.colors.textLightGray
        bottomLabel.text = item.bottomTitle
        bottomLabel.textColor = item.bottomTitleColor ?? theme.colors.textGray

        backgroundEmptyView.backgroundColor = item.fullPercentColor ?? theme.colors.elementLightGray
        backgroundColorView.backgroundColor = item.percentColor ?? theme.colors.elementGreen

        if animated {
            state = .waitingForAnimation
            performLayout()

            state = .idle
            UIView.animate(withDuration: 0.7, delay: 0, options: .curveEaseInOut) { [weak self] in
                self?.performLayout()
            }
        } else {
            performLayout()
        }
    }

    func animate() {
        state = .idle
        performLayout()
    }

    // MARK: - Private. Layout

    private func performLayout() {
        guard let item = diagramColumnItem else { return }

        let dottedFrame = diagramDelegate?.diagramColumnCellDottedFrame(self) ?? .zero
        let dottedHeight = dottedFrame.height + 10

        let backgroundHeight = state == .waitingForAnimation ? 0 : dottedHeight * CGFloat(min(1, item.fullPercent ?? 0))
        backgroundEmptyView.pin
            .horizontally()
            .bottom()
            .marginBottom(frame.height - dottedFrame.maxY)
            .height(backgroundHeight)
        backgroundEmptyView.layer.cornerRadius = neededCornerRadiusForColumn(backgroundHeight)

        let colorHeight = state == .waitingForAnimation ? 0 : dottedHeight * CGFloat(min(1, item.percent))
        backgroundColorView.pin
            .horizontally()
            .bottom()
            .marginBottom(frame.height - dottedFrame.maxY)
            .height(colorHeight)
        backgroundColorView.layer.cornerRadius = neededCornerRadiusForColumn(colorHeight)

        topLabel.pin
            .horizontally()
            .bottom(to: backgroundColorView.edge.bottom)
            .marginBottom(7)
            .sizeToFit(.width)

        bottomLabel.pin
            .horizontally()
            .top(to: backgroundColorView.edge.bottom)
            .marginTop(16)
            .sizeToFit(.width)

        button.pin.all()
    }

    // MARK: - Private. Actions

    @objc
    private func buttonDidTap(_ sender: UIButton) {
        guard let item = item else { return }
        delegate?.collectionCell(self, didTap: item)
    }

    // MARK: - Private. Help

    private func neededCornerRadiusForColumn(_ newHeight: CGFloat) -> CGFloat {
        let radius: CGFloat = 7
        let halfHeight: CGFloat = newHeight / 2
        return halfHeight > radius ? radius : halfHeight
    }
}
