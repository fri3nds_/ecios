//
//  DiagramColumnItem.swift
//  EldoSales
//
//  Created by Nikita Bondar on 19.06.2021.
//

import UIKit

class DiagramColumnItem: CollectionItem {
    // MARK: - System

    override var cellClass: AnyClass {
        DiagramColumnCell.self
    }

    // MARK: - Properties

    let topTitle: String?
    let topTitleColor: UIColor?
    let bottomTitle: String?
    let bottomTitleColor: UIColor?
    let percent: Double
    let percentColor: UIColor?
    let fullPercent: Double?
    let fullPercentColor: UIColor?

    // MARK: - Init

    init(identifier: String = UUID().uuidString,
         strategy: CollectionSizingStrategy,
         topTitle: String?,
         topTitleColor: UIColor?,
         bottomTitle: String?,
         bottomTitleColor: UIColor?,
         percent: Double,
         percentColor: UIColor? = nil,
         fullPercent: Double?,
         fullPercentColor: UIColor? = nil,
         model: Any?) {
        self.topTitle = topTitle
        self.topTitleColor = topTitleColor
        self.bottomTitle = bottomTitle
        self.bottomTitleColor = bottomTitleColor
        self.percent = percent
        self.percentColor = percentColor
        self.fullPercent = fullPercent
        self.fullPercentColor = fullPercentColor

        super.init(identifier: identifier, strategy: strategy, model: model)
    }
}
