//
//  DiagramTitlesCell.swift
//  EldoSales
//
//  Created by Nikita Bondar on 21.06.2021.
//

import UIKit

class DiagramTitlesCell: CollectionCell {
    // MARK: - Properties

    private var storedItem: DiagramTitlesItem? {
        item as? DiagramTitlesItem
    }

    // MARK: - Views

    private weak var bannerDiagramView: DiagramBannerView!

    // MARK: - Life cycle

    override func setup(with theme: Theme) {
        guard !isSetuped else { return }

        super.setup(with: theme)

        backgroundColor = .clear
        contentView.backgroundColor = theme.colors.background

        let bannerDiagramView = DiagramBannerView(theme: theme)
        contentView.addSubview(bannerDiagramView)
        self.bannerDiagramView = bannerDiagramView
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        performLayout()
    }

    // MARK: - Interface

    override func fill(with item: CollectionItem, animated: Bool = false) {
        super.fill(with: item, animated: animated)

        guard let item = item as? DiagramTitlesItem else { return }

        bannerDiagramView.reloadData(item.bannerItem, animated: animated)

        setNeedsLayout()
    }

    // MARK: - Private. Layout

    private func performLayout() {
        bannerDiagramView.pin.all()
    }
}
