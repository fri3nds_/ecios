//
//  DiagramTitlesItem.swift
//  EldoSales
//
//  Created by Nikita Bondar on 21.06.2021.
//

import UIKit

class DiagramTitlesItem: CollectionItem {
    // MARK: - System

    override var cellClass: AnyClass {
        DiagramTitlesCell.self
    }

    // MARK: - Properties

    let bannerItem: DiagramBannerItem

    // MARK: - Init

    init(identifier: String = UUID().uuidString,
         strategy: CollectionSizingStrategy,
         bannerItem: DiagramBannerItem,
         model: Any?) {
        self.bannerItem = bannerItem

        super.init(identifier: identifier, strategy: strategy, model: model)
    }
}
