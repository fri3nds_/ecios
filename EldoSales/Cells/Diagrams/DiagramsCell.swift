//
//  DiagramsCell.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class DiagramsCell: CollectionCell {
    // MARK: - Properties

    private var diagramsItem: DiagramsItem? {
        item as? DiagramsItem
    }

    // MARK: - Views

    private weak var circleDiagram: DiagramCircleView!
    private weak var columnDiagram: DiagramColumnView!

    // MARK: - Life cycle

    override func setup(with theme: Theme) {
        guard !isSetuped else { return }

        super.setup(with: theme)

        backgroundColor = .clear
        contentView.backgroundColor = theme.colors.background

        let circleDiagram = DiagramCircleView(theme: theme)
        contentView.addSubview(circleDiagram)
        self.circleDiagram = circleDiagram

        let columnDiagram = DiagramColumnView(theme: theme)
        contentView.addSubview(columnDiagram)
        self.columnDiagram = columnDiagram
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        performLayout()
    }

    // MARK: - Interface

    override func fill(with item: CollectionItem, animated: Bool = false) {
        super.fill(with: item, animated: animated)

        guard let item = item as? DiagramsItem else { return }

        switch item.mode {
        case .circle:
            columnDiagram.isHidden = true
            circleDiagram.isHidden = false

            circleDiagram.reloadData(title: item.circleTitle,
                                     subtitle: item.circleSubtitle,
                                     mainPercent: item.circleMainPercent,
                                     backgroundPercent: item.circleBackgroundPercent,
                                     animated: animated)

        case .column:
            circleDiagram.isHidden = true
            columnDiagram.isHidden = false

            columnDiagram.reloadData(item.columnItems,
                                     titles: item.columnTitles,
                                     lineColor: .clear,
                                     interItemInset: item.columnInterItemInset,
                                     itemsInsets: item.columnInsets,
                                     animated: animated)
        }

        setNeedsLayout()
    }

    // MARK: - Private. Layout

    private func performLayout() {
        circleDiagram.pin
            .all()
            .marginLeft(pin.safeArea.left)
            .marginRight(pin.safeArea.right)

        columnDiagram.pin
            .all()
            .marginLeft(pin.safeArea.left + 11)
            .marginRight(pin.safeArea.right)
    }
}
