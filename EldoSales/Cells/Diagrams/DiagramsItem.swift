//
//  DiagramsItem.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class DiagramsItem: CollectionItem {
    // MARK: - Static

    override var cellClass: AnyClass {
        DiagramsCell.self
    }

    // MARK: - Children

    enum DiagramMode: String {
        // MARK: - Cases

        case circle
        case column
    }

    // MARK: - Properties

    var mode: DiagramMode

    let circleTitle: String?
    let circleSubtitle: String?
    let circleMainPercent: Double
    let circleBackgroundPercent: Double

    let columnTitles: [String]
    let columnItems: [DiagramColumnItem]
    let columnInterItemInset: CGFloat
    let columnInsets: UIEdgeInsets

    // MARK: - Init

    init(identifier: String = UUID().uuidString,
         strategy: CollectionSizingStrategy,
         mode: DiagramMode,
         circleTitle: String?,
         circleSubtitle: String?,
         circleMainPercent: Double,
         circleBackgroundPercent: Double,
         columnTitles: [String],
         columnItems: [DiagramColumnItem],
         columnInterItemInset: CGFloat,
         columnInsets: UIEdgeInsets,
         model: Any?) {
        self.mode = mode
        
        self.circleTitle = circleTitle
        self.circleSubtitle = circleSubtitle
        self.circleMainPercent = circleMainPercent
        self.circleBackgroundPercent = circleBackgroundPercent

        self.columnTitles = columnTitles
        self.columnItems = columnItems
        self.columnInterItemInset = columnInterItemInset
        self.columnInsets = columnInsets

        super.init(identifier: identifier, strategy: strategy, model: model)
    }
}
