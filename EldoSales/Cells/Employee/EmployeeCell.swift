//
//  EmployeeCell.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class EmployeeCell: CollectionCell {
    // MARK: - Properties

    private var storedItem: EmployeeItem? {
        item as? EmployeeItem
    }

    // MARK: - Views

    private weak var numberLabel: UILabel!
    private weak var imageView: CircleImageView!
    private weak var titleLabel: UILabel!
    private weak var subtitleLabel: UILabel!
    private weak var accessoryTitleLabel: UILabel!
    private weak var accessorySubtitleLabel: UILabel!
    private weak var disclosureImageView: UIImageView!

    private weak var button: UIButton!

    // MARK: - Life cycle

    override func setup(with theme: Theme) {
        guard !isSetuped else { return }

        super.setup(with: theme)

        backgroundColor = .clear
        contentView.backgroundColor = theme.colors.background

        let numberLabel = UILabel()
        numberLabel.font = theme.fonts.font14Bold
        numberLabel.textColor = theme.colors.textBlack
        contentView.addSubview(numberLabel)
        self.numberLabel = numberLabel

        let imageView = CircleImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        contentView.addSubview(imageView)
        self.imageView = imageView

        let titleLabel = UILabel()
        titleLabel.font = theme.fonts.font16Bold
        titleLabel.textColor = theme.colors.textBlack
        contentView.addSubview(titleLabel)
        self.titleLabel = titleLabel

        let subtitleLabel = UILabel()
        subtitleLabel.font = theme.fonts.font14
        subtitleLabel.textColor = theme.colors.subtitleColor
        contentView.addSubview(subtitleLabel)
        self.subtitleLabel = subtitleLabel

        let accessoryTitleLabel = UILabel()
        accessoryTitleLabel.font = theme.fonts.font16Bold
        accessoryTitleLabel.textColor = theme.colors.textBlack
        accessoryTitleLabel.textAlignment = .right
        contentView.addSubview(accessoryTitleLabel)
        self.accessoryTitleLabel = accessoryTitleLabel

        let accessorySubtitleLabel = UILabel()
        accessorySubtitleLabel.font = theme.fonts.font14
        accessorySubtitleLabel.textColor = theme.colors.subtitleColor
        accessorySubtitleLabel.textAlignment = .right
        contentView.addSubview(accessorySubtitleLabel)
        self.accessorySubtitleLabel = accessorySubtitleLabel

        let disclosureImageView = UIImageView()
        disclosureImageView.image = theme.images.chevronRight
        disclosureImageView.contentMode = .scaleAspectFit
        contentView.addSubview(disclosureImageView)
        self.disclosureImageView = disclosureImageView

        let button = UIButton()
        button.addTarget(self, action: #selector(buttonDidTap(_:)), for: .touchUpInside)
        contentView.addSubview(button)
        self.button = button
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        performLayout()
    }

    // MARK: - Interface

    override func fill(with item: CollectionItem, animated: Bool = false) {
        super.fill(with: item, animated: animated)

        guard let item = item as? EmployeeItem else { return }

        numberLabel.text = item.number
        numberLabel.isHidden = item.number == nil

        if let imageURL = item.imageURL {
            Queue.global {
                ImageLoader(imageURL: imageURL).load(.profile).done { [weak self] image in
                    self?.imageView.image = image
                }.catch {
                    print("error image load: \($0.localizedDescription)")
                }
            }
        } else {
            imageView.image = nil
        }

        titleLabel.text = item.title
        titleLabel.textColor = item.titleColor ?? theme.colors.textBlack
        subtitleLabel.text = item.subtitle
        accessoryTitleLabel.text = item.accessoryTitle
        accessoryTitleLabel.textColor = item.accessoryTitleColor ?? theme.colors.textBlack

        accessorySubtitleLabel.text = item.accessorySubtitle
        accessorySubtitleLabel.isHidden = item.accessorySubtitle == nil

        disclosureImageView.isHidden = !item.showDisclosure

        setNeedsLayout()
    }

    // MARK: - Private. Layout

    private func performLayout() {
        numberLabel.pin
            .vCenter()
            .left()
            .marginLeft(pin.safeArea.left + 16)
            .sizeToFit()

        imageView.pin
            .vCenter()
            .left()
            .marginLeft(numberLabel.isHidden ? pin.safeArea.left + 16 : pin.safeArea.left + 50)
            .size(45)

        titleLabel.pin
            .top(to: imageView.edge.top)
            .left(to: imageView.edge.right)
            .marginTop(3)
            .marginLeft(12)
            .sizeToFit()
            .maxWidth(contentView.frame.width * 0.4)

        subtitleLabel.pin
            .top(to: titleLabel.edge.bottom)
            .left(to: imageView.edge.right)
            .marginTop(5)
            .marginLeft(12)
            .sizeToFit()
            .maxWidth(contentView.frame.width * 0.4)

        disclosureImageView.pin
            .vCenter()
            .right()
            .marginRight(pin.safeArea.right + 16)
            .size(15)

        if accessorySubtitleLabel.isHidden {
            accessoryTitleLabel.pin
                .vCenter()
                .left(to: titleLabel.edge.right)
                .right(to: disclosureImageView.isHidden ? contentView.edge.right : disclosureImageView.edge.left)
                .marginLeft(10)
                .marginRight(disclosureImageView.isHidden ? pin.safeArea.right + 16 : 10)
                .sizeToFit(.width)
        } else {
            accessoryTitleLabel.pin
                .top(to: titleLabel.edge.top)
                .left(to: titleLabel.edge.right)
                .right(to: disclosureImageView.isHidden ? contentView.edge.right : disclosureImageView.edge.left)
                .marginLeft(10)
                .marginRight(disclosureImageView.isHidden ? pin.safeArea.right + 16 : 10)
                .sizeToFit(.width)
        }

        accessorySubtitleLabel.pin
            .top(to: subtitleLabel.edge.top)
            .left(to: subtitleLabel.edge.right)
            .right(to: disclosureImageView.isHidden ? contentView.edge.right : disclosureImageView.edge.left)
            .marginLeft(10)
            .marginRight(disclosureImageView.isHidden ? pin.safeArea.right + 16 : 10)
            .sizeToFit(.width)

        button.pin.all()
    }

    // MARK: - Private. Actions

    @objc
    private func buttonDidTap(_ sender: UIButton) {
        guard let item = item else { return }
        delegate?.collectionCell(self, didTap: item)
    }
}
