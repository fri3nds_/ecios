//
//  EmployeeItem.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class EmployeeItem: CollectionItem {
    // MARK: - System

    override var cellClass: AnyClass {
        EmployeeCell.self
    }

    // MARK: - Properties

    let number: String?
    let imageURL: URL?
    let title: String?
    let titleColor: UIColor?
    let subtitle: String?
    let accessoryTitle: String?
    let accessoryTitleColor: UIColor?
    let accessorySubtitle: String?
    let showDisclosure: Bool

    // MARK: - Init

    init(identifier: String = UUID().uuidString,
         strategy: CollectionSizingStrategy,
         number: String?,
         imageURL: URL?,
         title: String?,
         titleColor: UIColor? = nil,
         subtitle: String?,
         accessoryTitle: String?,
         accessoryTitleColor: UIColor? = nil,
         accessorySubtitle: String?,
         showDisclosure: Bool,
         model: Any?) {
        self.number = number
        self.imageURL = imageURL
        self.title = title
        self.titleColor = titleColor
        self.subtitle = subtitle
        self.accessoryTitle = accessoryTitle
        self.accessoryTitleColor = accessoryTitleColor
        self.accessorySubtitle = accessorySubtitle
        self.showDisclosure = showDisclosure

        super.init(identifier: identifier, strategy: strategy, model: model)
    }
}
