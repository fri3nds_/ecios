//
//  EmptyCell.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class EmptyCell: CollectionCell {
    // MARK: - Properties

    private var storedItem: EmptyItem? {
        item as? EmptyItem
    }

    // MARK: - Views

    // MARK: - Life cycle

    override func setup(with theme: Theme) {
        guard !isSetuped else { return }

        super.setup(with: theme)

        backgroundColor = .clear
        contentView.backgroundColor = theme.colors.background
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        performLayout()
    }

    // MARK: - Interface

    override func fill(with item: CollectionItem, animated: Bool = false) {
        super.fill(with: item, animated: animated)

        guard let item = item as? EmptyItem else { return }

        contentView.backgroundColor = item.backgroundColor ?? theme.colors.background
    }

    // MARK: - Private. Layout

    private func performLayout() {

    }
}
