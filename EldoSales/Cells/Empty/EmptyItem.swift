//
//  EmptyItem.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class EmptyItem: CollectionItem {
    // MARK: - System

    override var cellClass: AnyClass {
        EmptyCell.self
    }

    // MARK: - Properties

    let backgroundColor: UIColor?

    // MARK: - Init

    init(identifier: String = UUID().uuidString,
         strategy: CollectionSizingStrategy,
         backgroundColor: UIColor? = nil,
         model: Any?) {
        self.backgroundColor = backgroundColor

        super.init(identifier: identifier, strategy: strategy, model: model)
    }
}
