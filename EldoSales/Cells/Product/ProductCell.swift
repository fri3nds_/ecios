//
//  ProductCell.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class ProductCell: CollectionCell {
    // MARK: - Properties

    private var storedItem: ProductItem? {
        item as? ProductItem
    }

    // MARK: - Views

    private weak var imageView: UIImageView!
    private weak var titleLabel: UILabel!
    private weak var subtitleLabel: UILabel!
    private weak var accessoryTitleLabel: UILabel!

    private weak var button: UIButton!

    // MARK: - Life cycle

    override func setup(with theme: Theme) {
        guard !isSetuped else { return }

        super.setup(with: theme)

        backgroundColor = .clear
        contentView.backgroundColor = theme.colors.background

        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        contentView.addSubview(imageView)
        self.imageView = imageView

        let titleLabel = UILabel()
        titleLabel.font = theme.fonts.font16Bold
        titleLabel.textColor = theme.colors.titleColor
        contentView.addSubview(titleLabel)
        self.titleLabel = titleLabel

        let subtitleLabel = UILabel()
        subtitleLabel.font = theme.fonts.font13
        subtitleLabel.textColor = theme.colors.titleColor
        subtitleLabel.numberOfLines = 2
        contentView.addSubview(subtitleLabel)
        self.subtitleLabel = subtitleLabel

        let accessoryTitleLabel = UILabel()
        accessoryTitleLabel.font = theme.fonts.font16Bold
        accessoryTitleLabel.textColor = theme.colors.titleColor
        contentView.addSubview(accessoryTitleLabel)
        self.accessoryTitleLabel = accessoryTitleLabel

        let button = UIButton()
        button.addTarget(self, action: #selector(buttonDidTap(_:)), for: .touchUpInside)
        contentView.addSubview(button)
        self.button = button
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        performLayout()
    }

    // MARK: - Interface

    override func fill(with item: CollectionItem, animated: Bool = false) {
        super.fill(with: item, animated: animated)

        guard let item = item as? ProductItem else { return }

        if let imageURL = item.imageURL {
            Queue.global {
                ImageLoader(imageURL: imageURL).load(.product).done { [weak self] image in
                    self?.imageView.image = image
                }.catch {
                    print("error image load: \($0.localizedDescription)")
                }
            }
        } else {
            imageView.image = nil
        }

        titleLabel.text = item.title
        subtitleLabel.text = item.subtitle
        accessoryTitleLabel.text = item.accessoryTitle

        setNeedsLayout()
    }

    // MARK: - Private. Layout

    private func performLayout() {
        imageView.pin
            .left()
            .vCenter()
            .marginLeft(16)
            .size(60)

        accessoryTitleLabel.pin
            .top(to: imageView.edge.top)
            .right()
            .marginRight(16)
            .sizeToFit()

        titleLabel.pin
            .top(to: imageView.edge.top)
            .left(to: imageView.edge.right)
            .right(to: accessoryTitleLabel.edge.left)
            .marginLeft(16)
            .marginRight(8)
            .sizeToFit(.width)

        subtitleLabel.pin
            .top(to: titleLabel.edge.bottom)
            .left(to: imageView.edge.right)
            .right(to: accessoryTitleLabel.edge.left)
            .marginTop(5)
            .marginLeft(16)
            .marginRight(8)
            .sizeToFit(.width)

        button.pin.all()
    }

    // MARK: - Private. Actions

    @objc
    private func buttonDidTap(_ sender: UIButton) {
        guard let item = item else { return }
        delegate?.collectionCell(self, didTap: item)
    }
}
