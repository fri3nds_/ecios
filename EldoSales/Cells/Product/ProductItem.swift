//
//  ProductItem.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class ProductItem: CollectionItem {
    // MARK: - System

    override var cellClass: AnyClass {
        ProductCell.self
    }

    // MARK: - Properties

    let title: String
    let subtitle: String
    let accessoryTitle: String?
    let imageURL: URL?

    // MARK: - Init

    init(identifier: String = UUID().uuidString,
         strategy: CollectionSizingStrategy,
         title: String,
         subtitle: String,
         accessoryTitle: String?,
         imageURL: URL?,
         model: Any?) {
        self.title = title
        self.subtitle = subtitle
        self.accessoryTitle = accessoryTitle
        self.imageURL = imageURL

        super.init(identifier: identifier, strategy: strategy, model: model)
    }
}
