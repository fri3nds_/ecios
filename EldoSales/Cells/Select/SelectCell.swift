//
//  SelectCell.swift
//  EldoSales
//
//  Created by Nikita Bondar on 21.06.2021.
//

import UIKit

class SelectCell: CollectionCell {
    // MARK: - Properties

    private var storedItem: SelectItem? {
        item as? SelectItem
    }

    // MARK: - Views

    private weak var titleLabel: UILabel!
    private weak var imageView: UIImageView!

    private weak var button: UIButton!

    // MARK: - Life cycle

    override func setup(with theme: Theme) {
        guard !isSetuped else { return }

        super.setup(with: theme)

        backgroundColor = .clear
        contentView.backgroundColor = theme.colors.background

        let titleLabel = UILabel()
        titleLabel.font = theme.fonts.font16
        titleLabel.textColor = theme.colors.titleColor
        contentView.addSubview(titleLabel)
        self.titleLabel = titleLabel

        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = theme.images.checkmarkIcon
        contentView.addSubview(imageView)
        self.imageView = imageView

        let button = UIButton()
        button.addTarget(self, action: #selector(buttonDidTap(_:)), for: .touchUpInside)
        contentView.addSubview(button)
        self.button = button
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        performLayout()
    }

    // MARK: - Interface

    override func fill(with item: CollectionItem, animated: Bool = false) {
        super.fill(with: item, animated: animated)

        guard let item = item as? SelectItem else { return }

        titleLabel.text = item.title
        imageView.isHidden = !item.isSelected

        setNeedsLayout()
    }

    // MARK: - Private. Layout

    private func performLayout() {
        imageView.pin
            .vCenter()
            .right()
            .marginRight(pin.safeArea.right + 16)
            .size(24)

        titleLabel.pin
            .vCenter()
            .left()
            .right(to: imageView.edge.left)
            .marginLeft(pin.safeArea.left + 16)
            .marginRight(10)
            .sizeToFit(.width)

        button.pin.all()
    }

    // MARK: - Private. Actions

    @objc
    private func buttonDidTap(_ sender: UIButton) {
        guard let item = item else { return }
        delegate?.collectionCell(self, didTap: item)
    }
}
