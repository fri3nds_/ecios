//
//  SelectItem.swift
//  EldoSales
//
//  Created by Nikita Bondar on 21.06.2021.
//

import UIKit

class SelectItem: CollectionItem {
    // MARK: - System

    override var cellClass: AnyClass {
        SelectCell.self
    }

    // MARK: - Properties

    let title: String
    let isSelected: Bool

    // MARK: - Init

    init(identifier: String = UUID().uuidString,
         strategy: CollectionSizingStrategy,
         title: String,
         isSelected: Bool,
         model: Any?) {
        self.title = title
        self.isSelected = isSelected

        super.init(identifier: identifier, strategy: strategy, model: model)
    }
}
