//
//  SeparatorCell.swift
//  EldoSales
//
//  Created by Nikita Bondar on 21.06.2021.
//

import UIKit

class SeparatorCell: CollectionCell {
    // MARK: - Properties

    private var storedItem: SeparatorItem? {
        item as? SeparatorItem
    }

    // MARK: - Views

    private weak var separatorView: UIView!

    // MARK: - Life cycle

    override func setup(with theme: Theme) {
        guard !isSetuped else { return }

        super.setup(with: theme)

        backgroundColor = .clear
        contentView.backgroundColor = theme.colors.background

        let separatorView = UIView()
        separatorView.backgroundColor = theme.colors.collectionBackground
        contentView.addSubview(separatorView)
        self.separatorView = separatorView
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        performLayout()
    }

    // MARK: - Interface

    override func fill(with item: CollectionItem, animated: Bool = false) {
        super.fill(with: item, animated: animated)

        guard let item = item as? SeparatorItem else { return }

        separatorView.backgroundColor = item.color ?? theme.colors.collectionBackground

        setNeedsLayout()
    }

    // MARK: - Private. Layout

    private func performLayout() {
        separatorView.pin
            .all()
            .marginLeft(pin.safeArea.left + 16)
            .marginRight(pin.safeArea.right + 16)
    }
}
