//
//  SeparatorItem.swift
//  EldoSales
//
//  Created by Nikita Bondar on 21.06.2021.
//

import UIKit

class SeparatorItem: CollectionItem {
    // MARK: - System

    override var cellClass: AnyClass {
        SeparatorCell.self
    }

    // MARK: - Properties

    let color: UIColor?

    // MARK: - Init

    init(identifier: String = UUID().uuidString,
         strategy: CollectionSizingStrategy,
         color: UIColor? = nil,
         model: Any?) {
        self.color = color

        super.init(identifier: identifier, strategy: strategy, model: model)
    }
}
