//
//  SwitchCell.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class SwitchCell: CollectionCell {
    // MARK: - Properties

    private var storedItem: SwitchItem? {
        item as? SwitchItem
    }

    // MARK: - Views

    private weak var titleLabel: UILabel!
    private weak var switchView: UISwitch!

    // MARK: - Life cycle

    override func setup(with theme: Theme) {
        guard !isSetuped else { return }

        super.setup(with: theme)

        backgroundColor = .clear
        contentView.backgroundColor = theme.colors.background

        let titleLabel = UILabel()
        titleLabel.font = theme.fonts.font16
        titleLabel.textColor = theme.colors.titleColor
        contentView.addSubview(titleLabel)
        self.titleLabel = titleLabel

        let switchView = UISwitch()
        switchView.addTarget(self, action: #selector(switchDidChange(_:)), for: .valueChanged)
        contentView.addSubview(switchView)
        self.switchView = switchView
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        performLayout()
    }

    // MARK: - Interface

    override func fill(with item: CollectionItem, animated: Bool = false) {
        super.fill(with: item, animated: animated)

        guard let item = item as? SwitchItem else { return }

        titleLabel.text = item.title
        switchView.isOn = item.on

        setNeedsLayout()
    }

    // MARK: - Private. Layout

    private func performLayout() {
        switchView.pin
            .vCenter()
            .right()
            .marginRight(pin.safeArea.right + 16)
            .sizeToFit()

        titleLabel.pin
            .vCenter()
            .left()
            .right(to: switchView.edge.left)
            .marginLeft(pin.safeArea.left + 16)
            .sizeToFit(.width)
    }

    // MARK: - Private. Actions

    @objc
    private func switchDidChange(_ sender: UISwitch) {
        storedItem?.on = sender.isOn
        print("switch changed \(sender.isOn)")
    }
}
