//
//  SwitchItem.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class SwitchItem: CollectionItem {
    // MARK: - System

    override var cellClass: AnyClass {
        SwitchCell.self
    }

    // MARK: - Properties

    var on: Bool
    let title: String

    // MARK: - Init

    init(identifier: String = UUID().uuidString,
         strategy: CollectionSizingStrategy,
         on: Bool,
         title: String,
         model: Any?) {
        self.on = on
        self.title = title

        super.init(identifier: identifier, strategy: strategy, model: model)
    }
}
