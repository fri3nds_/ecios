//
//  TextEditCell.swift
//  EldoSales
//
//  Created by Nikita Bondar on 22.06.2021.
//

import UIKit

protocol TextEditCellDelegate: CollectionCellDelegate {
    // MARK: - Interface

    func textEditCell(_ textEditCell: TextEditCell, item: TextEditItem, textDidChange text: String)
}

class TextEditCell: CollectionCell {
    // MARK: - Delegate

    private weak var storedDelegate: TextEditCellDelegate? {
        delegate as? TextEditCellDelegate
    }

    // MARK: - Properties

    private var storedItem: TextEditItem? {
        item as? TextEditItem
    }

    // MARK: - Views

    private weak var textField: UITextField!
    private weak var accessoryLabel: UILabel!
    private weak var separatorView: UIView!

    // MARK: - Life cycle

    override func setup(with theme: Theme) {
        guard !isSetuped else { return }

        super.setup(with: theme)

        backgroundColor = .clear
        contentView.backgroundColor = theme.colors.background

        let textField = UITextField()
        textField.textColor = theme.colors.textBlack
        textField.font = theme.fonts.font16Bold
        textField.returnKeyType = .done
        textField.keyboardType = .numberPad
        textField.autocorrectionType = .no
        textField.tintColor = theme.colors.textBlack
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        contentView.addSubview(textField)
        self.textField = textField

        let accessoryLabel = UILabel()
        accessoryLabel.font = theme.fonts.font16Bold
        accessoryLabel.textColor = theme.colors.textBlack
        accessoryLabel.textAlignment = .right
        contentView.addSubview(accessoryLabel)
        self.accessoryLabel = accessoryLabel

        let separatorView = UIView()
        separatorView.backgroundColor = theme.colors.elementGreen
        contentView.addSubview(separatorView)
        self.separatorView = separatorView
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        performLayout()
    }

    // MARK: - Interface

    override func fill(with item: CollectionItem, animated: Bool = false) {
        super.fill(with: item, animated: animated)

        guard let item = item as? TextEditItem else { return }

        if let formatter = item.numberFormatter, let text = item.text?.toNumber, let number = Int64(text) {
            textField.text = formatter.string(from: NSNumber(value: number))
        } else {
            textField.text = item.text
        }

        textField.placeholder = item.placeholder
        accessoryLabel.text = item.accessoryTitle

        textField.becomeFirstResponder()

        setNeedsLayout()
    }

    // MARK: - Private. Layout

    private func performLayout() {
        accessoryLabel.pin
            .vCenter()
            .right()
            .marginRight(pin.safeArea.right + 16)
            .sizeToFit()

        textField.pin
            .vertically()
            .left()
            .right(to: accessoryLabel.edge.left)
            .marginLeft(pin.safeArea.left + 16)
            .marginRight(10)

        separatorView.pin
            .horizontally()
            .bottom()
            .marginLeft(pin.safeArea.left + 16)
            .marginRight(pin.safeArea.right + 16)
            .height(1)
    }

    // MARK: - Private. Actions

    @objc
    private func textFieldDidChange(_ sender: TextField) {
        guard let item = storedItem else { return }

        if let formatter = item.numberFormatter, let text = sender.text?.toNumber, let number = Int64(text) {
            sender.text = formatter.string(from: NSNumber(value: number))
        }

        item.text = sender.text
        storedDelegate?.textEditCell(self, item: item, textDidChange: sender.text ?? "")
    }
}
