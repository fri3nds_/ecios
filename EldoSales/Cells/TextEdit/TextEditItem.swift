//
//  TextEditItem.swift
//  EldoSales
//
//  Created by Nikita Bondar on 22.06.2021.
//

import UIKit

class TextEditItem: CollectionItem {
    // MARK: - System

    override var cellClass: AnyClass {
        TextEditCell.self
    }

    // MARK: - Properties

    var text: String?
    let numberFormatter: NumberFormatter?
    let placeholder: String
    let accessoryTitle: String?

    // MARK: - Init

    init(identifier: String = UUID().uuidString,
         strategy: CollectionSizingStrategy,
         text: String?,
         numberFormatter: NumberFormatter?,
         placeholder: String,
         accessoryTitle: String?,
         model: Any?) {
        self.text = text
        self.numberFormatter = numberFormatter
        self.placeholder = placeholder
        self.accessoryTitle = accessoryTitle

        super.init(identifier: identifier, strategy: strategy, model: model)
    }
}
