//
//  TitleCell.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class TitleCell: CollectionCell {
    // MARK: - Properties

    private var storedItem: TitleItem? {
        item as? TitleItem
    }

    // MARK: - Views

    private weak var titleLabel: UILabel!

    // MARK: - Life cycle

    override func setup(with theme: Theme) {
        guard !isSetuped else { return }

        super.setup(with: theme)

        backgroundColor = .clear
        contentView.backgroundColor = theme.colors.background

        let titleLabel = UILabel()
        titleLabel.font = theme.fonts.font24Bold
        titleLabel.textColor = theme.colors.titleColor
        titleLabel.textAlignment = .left
        contentView.addSubview(titleLabel)
        self.titleLabel = titleLabel
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        performLayout()
    }

    // MARK: - Interface

    override func fill(with item: CollectionItem, animated: Bool = false) {
        super.fill(with: item, animated: animated)

        guard let item = item as? TitleItem else { return }

        titleLabel.text = item.title
        titleLabel.textColor = item.textColor ?? theme.colors.titleColor
        titleLabel.font = item.font ?? theme.fonts.font24Bold
        titleLabel.textAlignment = item.alignment

        contentView.backgroundColor = item.backgroundColor ?? theme.colors.background

        setNeedsLayout()
    }

    // MARK: - Private. Layout

    private func performLayout() {
        titleLabel.pin
            .all()
            .marginLeft(pin.safeArea.left + 16)
            .marginRight(pin.safeArea.right + 16)
    }
}
