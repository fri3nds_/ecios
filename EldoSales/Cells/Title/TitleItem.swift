//
//  TitleItem.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class TitleItem: CollectionItem {
    // MARK: - System

    override var cellClass: AnyClass {
        TitleCell.self
    }

    // MARK: - Properties

    let title: String
    let alignment: NSTextAlignment
    let textColor: UIColor?
    let font: UIFont?
    let backgroundColor: UIColor?

    // MARK: - Init

    init(identifier: String = UUID().uuidString,
         strategy: CollectionSizingStrategy,
         title: String,
         alignment: NSTextAlignment,
         textColor: UIColor? = nil,
         font: UIFont? = nil,
         backgroundColor: UIColor? = nil,
         model: Any?) {
        self.title = title
        self.alignment = alignment
        self.textColor = textColor
        self.font = font
        self.backgroundColor = backgroundColor

        super.init(identifier: identifier, strategy: strategy, model: model)
    }
}
