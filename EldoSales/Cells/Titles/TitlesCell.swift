//
//  TitlesCell.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class TitlesCell: CollectionCell {
    // MARK: - Properties

    private var storedItem: TitlesItem? {
        item as? TitlesItem
    }

    // MARK: - Views

    private weak var titleLabel: UILabel!
    private weak var subtitleLabel: UILabel!
    private weak var disclosureImageView: UIImageView!

    private weak var button: UIButton!

    // MARK: - Life cycle

    override func setup(with theme: Theme) {
        guard !isSetuped else { return }

        super.setup(with: theme)

        backgroundColor = .clear
        contentView.backgroundColor = theme.colors.background

        let titleLabel = UILabel()
        titleLabel.font = theme.fonts.font16Bold
        titleLabel.textColor = theme.colors.titleColor
        contentView.addSubview(titleLabel)
        self.titleLabel = titleLabel

        let subtitleLabel = UILabel()
        subtitleLabel.font = theme.fonts.font16
        subtitleLabel.textColor = theme.colors.subtitleColor
        subtitleLabel.textAlignment = .right
        contentView.addSubview(subtitleLabel)
        self.subtitleLabel = subtitleLabel

        let disclosureImageView = UIImageView()
        disclosureImageView.contentMode = .scaleAspectFit
        contentView.addSubview(disclosureImageView)
        self.disclosureImageView = disclosureImageView

        let button = UIButton()
        button.addTarget(self, action: #selector(buttonDidTap(_:)), for: .touchUpInside)
        contentView.addSubview(button)
        self.button = button
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        performLayout()
    }

    // MARK: - Interface

    override func fill(with item: CollectionItem, animated: Bool = false) {
        super.fill(with: item, animated: animated)

        guard let item = item as? TitlesItem else { return }

        titleLabel.text = item.title
        subtitleLabel.text = item.subtitle
        disclosureImageView.image = theme.images.chevronRight
        disclosureImageView.isHidden = !item.showDisclosure

        setNeedsLayout()
    }

    // MARK: - Private. Layout

    private func performLayout() {
        titleLabel.pin
            .left()
            .vCenter()
            .marginLeft(pin.safeArea.left + 16)
            .sizeToFit()
            .maxWidth(contentView.frame.width * 0.7)

        disclosureImageView.pin
            .vCenter()
            .right()
            .marginRight(pin.safeArea.right + 16)
            .size(15)

        subtitleLabel.pin
            .left(to: titleLabel.edge.right)
            .right(to: disclosureImageView.isHidden ? contentView.edge.right : disclosureImageView.edge.left)
            .vCenter()
            .marginLeft(5)
            .marginRight(disclosureImageView.isHidden ? pin.safeArea.right + 16 : 8)
            .sizeToFit(.width)

        button.pin.all()
    }

    // MARK: - Private. Actions

    @objc
    private func buttonDidTap(_ sender: UIButton) {
        guard let item = item else { return }
        delegate?.collectionCell(self, didTap: item)
    }
}
