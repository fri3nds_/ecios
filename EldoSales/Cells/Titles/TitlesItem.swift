//
//  TitlesItem.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class TitlesItem: CollectionItem {
    // MARK: - System

    override var cellClass: AnyClass {
        TitlesCell.self
    }

    // MARK: - Properties

    let title: String?
    let subtitle: String?
    let showDisclosure: Bool

    // MARK: - Init

    init(identifier: String = UUID().uuidString,
         strategy: CollectionSizingStrategy,
         title: String?,
         subtitle: String?,
         showDisclosure: Bool,
         model: Any?) {
        self.title = title
        self.subtitle = subtitle
        self.showDisclosure = showDisclosure

        super.init(identifier: identifier, strategy: strategy, model: model)
    }
}
