//
//  UserCell.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class UserCell: CollectionCell {
    // MARK: - Properties

    private var storedItem: UserItem? {
        item as? UserItem
    }

    // MARK: - Views

    private weak var imageView: CircleImageView!

    private weak var containerView: UIView!
    private weak var titleLabel: UILabel!
    private weak var subtitleLabel: UILabel!

    // MARK: - Life cycle

    override func setup(with theme: Theme) {
        guard !isSetuped else { return }

        super.setup(with: theme)

        backgroundColor = .clear
        contentView.backgroundColor = theme.colors.background

        let imageView = CircleImageView()
        imageView.contentMode = .scaleAspectFit
        contentView.addSubview(imageView)
        self.imageView = imageView

        let containerView = UIView()
        contentView.addSubview(containerView)
        self.containerView = containerView

        let titleLabel = UILabel()
        titleLabel.font = theme.fonts.font17Bold
        titleLabel.textColor = theme.colors.textBlack
        containerView.addSubview(titleLabel)
        self.titleLabel = titleLabel

        let subtitleLabel = UILabel()
        subtitleLabel.font = theme.fonts.font15
        subtitleLabel.textColor = theme.colors.textGray
        containerView.addSubview(subtitleLabel)
        self.subtitleLabel = subtitleLabel
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        performLayout()
    }

    // MARK: - Interface

    override func fill(with item: CollectionItem, animated: Bool = false) {
        super.fill(with: item, animated: animated)

        guard let item = item as? UserItem else { return }

        if let imageURL = item.imageURL {
            Queue.global {
                ImageLoader(imageURL: imageURL).load(.product).done { [weak self] image in
                    self?.imageView.image = image
                }.catch {
                    print("error image load: \($0.localizedDescription)")
                }
            }
        } else {
            imageView.image = nil
        }

        titleLabel.text = item.title
        subtitleLabel.text = item.subtitle

        setNeedsLayout()
    }

    // MARK: - Private. Layout

    private func performLayout() {
        imageView.pin
            .vCenter()
            .left()
            .marginLeft(pin.safeArea.left + 16)
            .size(45)

        containerView.pin
            .vCenter()
            .left(to: imageView.edge.right)
            .right()
            .marginLeft(16)
            .marginRight(pin.safeArea.right + 16)

        titleLabel.pin
            .top()
            .horizontally()
            .sizeToFit(.width)

        subtitleLabel.pin
            .top(to: titleLabel.edge.bottom)
            .horizontally()
            .sizeToFit(.width)

        containerView.pin
            .wrapContent(.vertically)
            .vCenter()
    }
}
