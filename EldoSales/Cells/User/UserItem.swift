//
//  UserItem.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class UserItem: CollectionItem {
    // MARK: - System

    override var cellClass: AnyClass {
        UserCell.self
    }

    // MARK: - Properties

    let imageURL: URL?
    let title: String?
    let subtitle: String?

    // MARK: - Init

    init(identifier: String = UUID().uuidString,
         strategy: CollectionSizingStrategy,
         imageURL: URL?,
         title: String?,
         subtitle: String?,
         model: Any?) {
        self.imageURL = imageURL
        self.title = title
        self.subtitle = subtitle

        super.init(identifier: identifier, strategy: strategy, model: model)
    }
}
