//
//  Category.swift
//  EldoSales
//
//  Created by Nikita Bondar on 22.06.2021.
//

import HandyJSON

class Category: HandyJSON {
    // MARK: - Properties

    var id: String?
    var name: String?

    // MARK: - Init

    required init() {}
}
