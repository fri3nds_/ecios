//
//  Plan.swift
//  EldoSales
//
//  Created by Nikita Bondar on 22.06.2021.
//

import HandyJSON

enum PlanType: String, HandyJSONEnum {
    // MARK: - Cases

    case common
    case individual
}

extension PlanType: CaseIterable {}

class Plan: HandyJSON {
    // MARK: - Properties

    var id: String?
    var ownerId: String?
    var shopId: String?
    var type: PlanType?
    var date: Date?
    var categoriesIds: [String]?

    var owner: User?
    var shop: Shop?
    var categories: [PlanCategory]?
    var sales: [Sale]?

    var amount: Int64 {
        var sum: Int64 = 0
        categories?.forEach { sum += $0.amount ?? 0 }
        return sum
    }

    // MARK: - Init

    required init() {}

}
