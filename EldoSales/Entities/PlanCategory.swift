//
//  PlanCategory.swift
//  EldoSales
//
//  Created by Nikita Bondar on 22.06.2021.
//

import HandyJSON

class PlanCategory: HandyJSON {
    // MARK: - Properties

    var id: String?
    var categoryId: String?
    var amount: Int64?

    var category: Category?

    // MARK: - Init

    required init() {}
}
