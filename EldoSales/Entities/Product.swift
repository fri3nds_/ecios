//
//  Product.swift
//  EldoSales
//
//  Created by Nikita Bondar on 22.06.2021.
//

import HandyJSON

enum ProductType: String, HandyJSONEnum {
    // MARK: - Cases

    case accessory
    case service
    case goods
}

class Product: HandyJSON {
    // MARK: - Properties

    var id: String?
    var name: String?
    var description: String?
    var image: URL?
    var price: Int64?
    var categoryId: String?
    var category: Category?

    // MARK: - Init

    required init() {}
}
