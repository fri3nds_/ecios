//
//  Region.swift
//  EldoSales
//
//  Created by Nikita Bondar on 22.06.2021.
//

import HandyJSON

class Region: HandyJSON {
    // MARK: - Properties

    var id: String?
    var name: String?
    var image: URL?

    var shops: [Shop]?

    // MARK: - Init

    required init() {}
}
