//
//  Sale.swift
//  EldoSales
//
//  Created by Nikita Bondar on 22.06.2021.
//

import HandyJSON

class Sale: HandyJSON {
    // MARK: - Properties

    var id: String?
    var productId: String?
    var userId: String?
    var shopId: String?
    var date: Date?
    var count: Int?

    var product: Product?
    var user: User?
    var shop: Shop?

    // MARK: - Init

    required init() {}
}
