//
//  Shop.swift
//  EldoSales
//
//  Created by Nikita Bondar on 22.06.2021.
//

import HandyJSON

class Shop: HandyJSON {
    // MARK: - Properties

    var id: String?
    var regionId: String?
    var name: String?
    var image: URL?
    var address: String?

    // MARK: - Init

    required init() {}
}
