//
//  User.swift
//  EldoSales
//
//  Created by Nikita Bondar on 22.06.2021.
//

import HandyJSON

enum UserPosition: String, HandyJSONEnum {
    // MARK: - Cases

    case director
    case seller

    // MARK: - Properties

    var title: String {
        switch self {
        case .seller:   return "Сотрудник"
        case .director: return "Директор"
        }
    }
}

class User: HandyJSON {
    // MARK: - Properties

    var id: String?
    var name: String?
    var personnelNumber: String?
    var phoneNumber: String?
    var address: String?
    var position: UserPosition?
    var image: URL?
    var shopId: String?

    var shop: Shop?

    private var _salesAmount: [String: Int64] = [:]

    // MARK: - Init

    required init() {}

    // MARK: - Interface

    func salesAmount(_ plan: Plan, categoryId: String?, force: Bool = false) -> Int64 {
        if !force, let salesAmount = _salesAmount[categoryId ?? "all"] {
            return salesAmount
        }

        let userSales = plan.sales?.filter { $0.userId == id } ?? []
        let categorySales = categoryId == nil ? userSales : userSales.filter { $0.product?.category?.id == categoryId }

        var salesAmount: Int64 = 0
        categorySales.forEach { salesAmount += $0.product?.price ?? 0 }

        _salesAmount[categoryId ?? "all"] = salesAmount
        return salesAmount
    }
}
