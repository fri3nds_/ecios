//
//  Errors.swift
//  EldoSales
//
//  Created by Nikita Bondar on 22.06.2021.
//

import Foundation

enum Errors: Error {
    // MARK: - Cases

    case requestCancelled
    case notFound
    case unauthorized
    case unknownError
}
