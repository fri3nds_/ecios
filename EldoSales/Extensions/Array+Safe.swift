//
//  Array+Safe.swift
//  EldoSales
//
//  Created by Nikita Bondar on 19.06.2021.
//

import Foundation

extension Array {
    // MARK: - Interface

    func object(at index: Int) -> Element? {
        guard index >= 0, index < count else { return nil }
        return self[index]
    }
}
