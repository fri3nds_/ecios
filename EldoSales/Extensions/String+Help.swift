//
//  String+Help.swift
//  EldoSales
//
//  Created by Nikita Bondar on 23.06.2021.
//

import Foundation

extension String {
    // MARK: - Properties

    var toNumber: String {
        var number: String = ""
        for c in self {
            guard c.asciiValue != nil else { continue }
            number.append(c)
        }
        return number
    }
}
