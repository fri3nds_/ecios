//
//  UIGestureRecognizer+Cancel.swift
//  EldoSales
//
//  Created by Nikita Bondar on 21.06.2021.
//

import UIKit

extension UIGestureRecognizer {
    // MARK: - Interface

    func cancel() {
        isEnabled.toggle()
        isEnabled.toggle()
    }
}
