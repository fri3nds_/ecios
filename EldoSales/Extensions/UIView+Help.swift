//
//  UIView+Help.swift
//  EldoSales
//
//  Created by Nikita Bondar on 18.06.2021.
//

import UIKit

extension UIView {
    // MARK: - Interface

    func bounce(scale: CGFloat = 0.99) {
        UIView.animate(withDuration: 0.1, animations: { [weak self] in
            self?.transform = CGAffineTransform(scaleX: scale, y: scale)
        }, completion: { _ in
            UIView.animate(withDuration: 0.1, animations: { [weak self] in
                self?.transform = .identity
            })
        })
    }

    func startBouncingAnimation() {
        let bouncing = CABasicAnimation(keyPath: "transform")
        bouncing.duration = 0.5
        bouncing.fromValue = CATransform3DMakeScale(1, 1, 1)
        bouncing.toValue = CATransform3DMakeScale(1.5, 1.5, 1)
        bouncing.repeatCount = .infinity
        bouncing.autoreverses = true

        layer.add(bouncing, forKey: "bouncing")
    }

    func stopBouncingAnimation() {
        layer.removeAnimation(forKey: "bouncing")
        layer.removeAnimation(forKey: "rotation")
    }

    func setHidden(_ hidden: Bool, animated: Bool) {
        if animated {
            UIView.animate(withDuration: 0.3) { [weak self] in
                self?.alpha = hidden ? 0 : 1
            }
        } else {
            alpha = hidden ? 0 : 1
        }
    }

    func apply(shadow: Shadow) {
        layer.shadowColor = shadow.color.cgColor
        layer.shadowOffset = shadow.offset
        layer.shadowRadius = shadow.radius
        layer.shadowOpacity = shadow.opacity
    }

    func roundCorners(_ corners: UIRectCorner, radius: CGFloat = 10) {
        clipsToBounds = true

        layer.cornerRadius = radius

        var cornerMask: CACornerMask = []
        if corners.contains(.topLeft) {
            cornerMask.insert(.layerMinXMinYCorner)
        }
        if corners.contains(.topRight) {
            cornerMask.insert(.layerMaxXMinYCorner)
        }
        if corners.contains(.bottomLeft) {
            cornerMask.insert(.layerMinXMaxYCorner)
        }
        if corners.contains(.bottomRight) {
            cornerMask.insert(.layerMaxXMaxYCorner)
        }
        if corners.contains(.allCorners) {
            cornerMask = [
                .layerMinXMinYCorner,
                .layerMaxXMinYCorner,
                .layerMinXMaxYCorner,
                .layerMaxXMaxYCorner
            ]
        }

        layer.maskedCorners = cornerMask
    }
}
