//
//  UIViewController+Help.swift
//  EldoSales
//
//  Created by Nikita Bondar on 19.06.2021.
//

import UIKit

extension UIViewController {
    // MARK: - Properties

    var navigationTitle: String {
        get {
            if let tabBarController = tabBarController {
                return tabBarController.navigationItem.title ?? ""
            } else {
                return navigationItem.title ?? ""
            }
        }
        set {
            if let tabBarController = tabBarController {
                tabBarController.navigationItem.title = newValue
            } else {
                navigationItem.title = newValue
            }
        }
    }

    var topNavigationController: UINavigationController? {
        if let tabBarController = tabBarController {
            return tabBarController.navigationController
        } else {
            return navigationController
        }
    }

    // MARK: - Interface

    func extractCollectionViewController() -> CollectionViewController? {
        children.first(where: { $0 is CollectionViewController }) as? CollectionViewController
    }
}
