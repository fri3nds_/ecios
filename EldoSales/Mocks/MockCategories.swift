//
//  MockCategories.swift
//  EldoSales
//
//  Created by Nikita Bondar on 22.06.2021.
//

import Foundation

class MockCategories {
    // MARK: - Static

    static let shared = MockCategories()

    // MARK: - Properties

    let categories: [Category]

    // MARK: - Init

    init() {
        let categoryNames = ["Аксессуары", "Услуги", "Товары"]
        var categories: [Category] = []

        for name in categoryNames {
            let category = Category()
            category.id = UUID().uuidString
            category.name = name

            categories.append(category)
        }
        self.categories = categories
    }
}
