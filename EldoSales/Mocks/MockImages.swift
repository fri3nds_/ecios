//
//  MockImages.swift
//  EldoSales
//
//  Created by Nikita Bondar on 22.06.2021.
//

import UIKit

class MockImages {
    // MARK: - Static

    static let shared = MockImages()

    // MARK: - Properties

    let profileImages: [UIImage]
    let productImages: [UIImage]
    let randomImages: [UIImage]

    // MARK: - Init

    init() {
        var profileImages: [UIImage] = []
        for i in 0 ..< 5 {
            profileImages.append(UIImage(named: "mock_profile_\(i + 1)")!)
        }
        self.profileImages = profileImages

        var productImages: [UIImage] = []
        for i in 0 ..< 3 {
            productImages.append(UIImage(named: "mock_product_\(i + 1)")!)
        }
        self.productImages = productImages

        var randomImages: [UIImage] = []
        for i in 0 ..< 9 {
            randomImages.append(UIImage(named: "mock_random_\(i + 1)")!)
        }
        self.randomImages = randomImages
    }
}
