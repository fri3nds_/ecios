//
//  MockPlans.swift
//  EldoSales
//
//  Created by Nikita Bondar on 22.06.2021.
//

import Foundation

class MockPlans {
    // MARK: - Statis

    static let shared = MockPlans()

    // MARK: - Properties

    let plans: [Plan]

    // MARK: - Init

    init() {
        let categories = MockCategories.shared.categories

        var planCategories: [PlanCategory] = []
        for i in 0 ..< 3 {
            let planCategory = PlanCategory()
            planCategory.id = UUID().uuidString
            planCategory.categoryId = categories[i].id
            planCategory.amount = Int64.random(in: 200000 ..< 2000000)
            planCategory.category = categories[i]

            planCategories.append(planCategory)
        }

        let oneDay: TimeInterval = 60 * 60 * 24
        var plans: [Plan] = []
        for i in 0 ..< 30 {
            let director = Plan()
            director.id = UUID().uuidString
            director.type = .common
            director.date = Date().addingTimeInterval(oneDay * Double(i - 14))
            director.owner = MockUsers.shared.director
            director.ownerId = director.owner?.id
            director.shop = MockShops.shared.current
            director.shopId = director.shop?.id
            director.categoriesIds = categories.compactMap { $0.id }
            director.categories = planCategories
            director.sales = MockSales.shared.sales.filter { $0.shopId == director.shop?.id }

            plans.append(director)
        }

        for shop in MockShops.shared.shops {
            guard shop.id != MockShops.shared.current.id else { continue }
            
            var planCategories: [PlanCategory] = []
            for i in 0 ..< 3 {
                let planCategory = PlanCategory()
                planCategory.id = UUID().uuidString
                planCategory.categoryId = categories[i].id
                planCategory.amount = Int64.random(in: 200000 ..< 2000000)
                planCategory.category = categories[i]

                planCategories.append(planCategory)
            }

            let oneDay: TimeInterval = 60 * 60 * 24
            for i in 0 ..< 30 {
                let director = Plan()
                director.id = UUID().uuidString
                director.type = .common
                director.date = Date().addingTimeInterval(oneDay * Double(i - 14))
                director.owner = MockUsers.shared.director
                director.ownerId = director.owner?.id
                director.shop = shop
                director.shopId = shop.id
                director.categoriesIds = categories.compactMap { $0.id }
                director.categories = planCategories
                director.sales = MockSales.shared.sales.filter { $0.shopId == shop.id }

                plans.append(director)
            }
        }

        self.plans = plans
    }
}
