//
//  MockProducts.swift
//  EldoSales
//
//  Created by Nikita Bondar on 22.06.2021.
//

import Foundation

class MockProducts {
    // MARK: - Static

    static let shared = MockProducts()

    // MARK: - Properties

    let products: [Product]

    // MARK: - Init

    init() {
        let info: [(name: String, description: String, price: Int64)] = [
            (name: "Утюг", description: "Хороший стильный утюг Тефаль", price: 2990),
            (name: "Cковорода", description: "Колонка премиум класса, нового поколения, Американский флагман", price: 9990),
            (name: "Страховка", description: "Застрахуем все, можем даже Эйфелеву башню", price: 7990)
        ]

        var products: [Product] = []
        for _ in 0 ..< 300 {
            let productInfo = info.randomElement()

            let product = Product()
            product.id = UUID().uuidString
            product.name = productInfo?.name
            product.description = productInfo?.description
            product.image = URL(string: "https://_product_image")
            product.price = productInfo?.price
            product.category = MockCategories.shared.categories.randomElement()
            product.categoryId = product.category?.id

            products.append(product)
        }
        self.products = products
    }
}
