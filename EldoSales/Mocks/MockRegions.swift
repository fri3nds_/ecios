//
//  MockRegions.swift
//  EldoSales
//
//  Created by Nikita Bondar on 22.06.2021.
//

import Foundation

class MockRegions {
    // MARK: - Static

    static let shared = MockRegions()

    // MARK: - Properties

    let current: Region
    let regions: [Region]

    // MARK: - Init

    init() {
        let names: [String] = [
            "Москва",
            "Московская область",
            "Воронеж",
            "Воронежская область",
            "Краснодар",
            "Крым",
            "Камчатка",
            "Сургут",
            "Магадан"
        ]

        func randomShop(in region: Region) -> Shop? {
            guard let shop = MockShops.shared.shops.first(where: { $0.regionId == nil }) else { return nil }
            shop.regionId = current.id
            return shop
        }

        let current = Region()
        current.id = UUID().uuidString
        current.name = names.first
        current.image = URL(string: "https://_current_region_")
        current.shops = [randomShop(in: current), randomShop(in: current), randomShop(in: current)].compactMap { $0 }
        self.current = current

        var regions: [Region] = [current]
        for i in 1 ..< names.count {
            let region = Region()
            region.id = UUID().uuidString
            region.name = names[i]
            region.image = URL(string: "https://_region_")
            region.shops = [randomShop(in: region), randomShop(in: region), randomShop(in: region)].compactMap { $0 }

            regions.append(region)
        }
        self.regions = regions
    }
}
