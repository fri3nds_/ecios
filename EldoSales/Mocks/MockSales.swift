//
//  MockSales.swift
//  EldoSales
//
//  Created by Nikita Bondar on 22.06.2021.
//

import Foundation

class MockSales {
    // MARK: - Static

    static let shared = MockSales()

    // MARK: - Properties

    let sales: [Sale]

    // MARK: - Init

    init() {
        var sales: [Sale] = []
        for user in MockUsers.shared.users {
            for _ in 0 ..< 10 {
                let sale = Sale()
                sale.id = UUID().uuidString
                sale.product = MockProducts.shared.products.randomElement()
                sale.productId = sale.product?.id
                sale.user = user
                sale.userId = user.id
                sale.shop = user.shop
                sale.shopId = sale.shop?.id
                sale.date = Date().addingTimeInterval(60 * 60 * Double(Int.random(in: 0 ..< 120)))
                sale.count = 1

                sales.append(sale)
            }
        }
        self.sales = sales
    }

    // MARK: - Interface

    func fetch(for userId: String) -> [Sale] {
        sales.filter { $0.userId == userId }
    }
}
