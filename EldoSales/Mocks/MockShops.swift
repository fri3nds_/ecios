//
//  MockShops.swift
//  EldoSales
//
//  Created by Nikita Bondar on 22.06.2021.
//

import Foundation

class MockShops {
    // MARK: - Static

    static let shared = MockShops()

    // MARK: - Properties

    let current: Shop
    let shops: [Shop]

    // MARK: - Init

    init() {
        let info: [(name: String, address: String)] = [
            (name: "Эльдо Волгоградка", address: "Москва, Волгоградское ш., д. 6"),
            (name: "Эльдо Ленинский", address: "Москва, Ленинский проспект, д. 5"),
            (name: "Эльдо Одинцово", address: "Одинцово, ул. Маршала Жукова, д. 29"),
            (name: "Эльдо Воронеж", address: "Воронеж, ул. Главная, д. 12"),
            (name: "Эльдо Самара", address: "Самара, ул. Танкистов, д. 8"),
            (name: "Эльдо Калининград", address: "Калининград, ул. Северная, д. 19"),
            (name:"Эльдо Алушта", address: "Алушта, ул. Спасения, д. 1"),
            (name:"Эльдо Тверь", address: "Тверь, ул. Ленина, д. 5"),
            (name:"Эльдо Коломна", address: "Коломна, ул. Верная, д. 117"),
            (name:"Эльдо Сергиев-Посад", address: "Сергиев-Посад, ул. Дружбы, д. 98"),
            (name:"Эльдо Ростов", address: "Ростов, ул. Курортная, д. 21"),
            (name:"Эльдо Ялта", address: "Ялта, ул. Массандра, д. 56")
        ]

        let currentInfo = info.randomElement()

        let current = Shop()
        current.id = UUID().uuidString
        current.name = currentInfo?.name
        current.image = URL(string: "https://_current_shop_image")
        current.address = currentInfo?.address
        self.current = current

        var shops: [Shop] = [current]
        for _ in 0 ..< 5 {
            let shopInfo = info.randomElement()

            let shop = Shop()
            shop.id = UUID().uuidString
            shop.name = shopInfo?.name
            shop.image = URL(string: "https://_shop_image")
            shop.address = shopInfo?.address
            shops.append(shop)
        }
        self.shops = shops
    }
}
