//
//  MockUsers.swift
//  EldoSales
//
//  Created by Nikita Bondar on 22.06.2021.
//

import Foundation

struct MockUsers {
    // MARK: - Statis

    static let shared = MockUsers()

    // MARK: - Properties

    let director: User
    let seller: User

    let users: [User]

    // MARK: - Init

    init() {
        let director = User()
        director.id = UUID().uuidString
        director.name = "Иван Владимирович"
        director.personnelNumber = "1"
        director.phoneNumber = "+7 999 888 77 66"
        director.address = "Москва"
        director.position = .director
        director.image = URL(string: "https://_current_user_image_")
        director.shopId = MockShops.shared.current.id
        director.shop = MockShops.shared.current
        self.director = director

        let seller = User()
        seller.id = UUID().uuidString
        seller.name = "Артем Евгеньевич"
        seller.personnelNumber = "2"
        seller.phoneNumber = "+7 999 888 77 66"
        seller.address = "Москва"
        seller.position = .seller
        seller.image = URL(string: "https://_current_user_image_")
        seller.shopId = MockShops.shared.current.id
        seller.shop = MockShops.shared.current
        self.seller = seller

        var users: [User] = [seller]
        let names = ["Артем Евгеньевич", "Иван Сидоров", "Владимир Невский", "Ирина Давыдова", "Илья Казантипов"]

        for i in 0 ..< 29 {
            let user = User()
            user.id = UUID().uuidString
            user.name = names.randomElement()
            user.personnelNumber = "\(i + 3)"
            user.phoneNumber = "+7 999 888 77 66"
            user.address = ""
            user.position = .seller
            user.image = URL(string: "https://_user_image_")
            user.shop = MockShops.shared.shops.randomElement()
            user.shopId = user.shop?.id

            users.append(user)
        }
        self.users = users
    }
}
