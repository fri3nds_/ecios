//
//  MainModuleFactory.swift
//  EldoSales
//
//  Created by Nikita Bondar on 14.06.2021.
//

import UIKit

class MainModuleFactory: ModuleFactory {
    // MARK: - Properties

    let services: Services
    let theme: Theme

    private let storage: UserDefaults

    // MARK: - Init

    init(services: Services, storage: UserDefaults, theme: Theme) {
        self.services = services
        self.storage = storage
        self.theme = theme
    }

    // MARK: - Interface

    func auth() -> UIViewController {
        AuthViewController(services: services, storage: storage, moduleFactory: self, theme: theme)
    }

    func plan(_ user: User) -> UIViewController {
        PlanViewController(moduleFactory: self, services: services, theme: theme, user: user)
    }

    func profile(_ user: User) -> UIViewController {
        ProfileViewController(moduleFactory: self, services: services, theme: theme, user: user)
    }

    func statistics(_ user: User) -> UIViewController {
        StatisticsViewController(moduleFactory: self, services: services, theme: theme, user: user)
    }

    func rating(_ user: User) -> UIViewController {
        RatingViewController(moduleFactory: self, services: services, theme: theme, user: user)
    }

    func tabBar() -> UIViewController {
        let tabBarController = TabBarController(theme: theme)

        print("user position \(services.userService.current?.position?.rawValue ?? "-")")

        let viewControllers: [UIViewController]
        switch services.userService.current?.position {
        case .director:
            viewControllers = [
                statistics(services.userService.current!),
                rating(services.userService.current!),
                plan(services.userService.current!),
                profile(services.userService.current!)
            ]

        case .seller:
            viewControllers = [
                statistics(services.userService.current!),
                rating(services.userService.current!),
                profile(services.userService.current!)
            ]

        case .none:
            fatalError("can't parse user position")
        }

        tabBarController.setViewControllers(viewControllers, animated: false)
        return tabBarController
    }

    // MARK: - Interface. Support

    func containerMovableCollection(_ items: [CollectionItem], heightMode: CollectionViewController.HeightMode, cellDelegate: CollectionCellDelegate?) -> UIViewController {
        let collectionViewController = CollectionViewController(cellDelegate: cellDelegate, mode: heightMode, theme: theme)
        collectionViewController.reloadItems(items)

        let containerMovableViewController = ContainerMovableViewController(theme: theme)
        containerMovableViewController.isAlphaEnabled = true
        containerMovableViewController.cornerRadius = 10
        containerMovableViewController.allowedPositions = [.top, .hidden]
        containerMovableViewController.startPosition = .top
        containerMovableViewController.set(sourceViewController: collectionViewController)

        return containerMovableViewController
    }
}
