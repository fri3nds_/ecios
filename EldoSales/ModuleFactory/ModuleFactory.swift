//
//  ModuleFactory.swift
//  EldoSales
//
//  Created by Nikita Bondar on 14.06.2021.
//

import UIKit

protocol ModuleFactory: AnyObject {
    // MARK: - Properties

    var services: Services { get }
    var theme: Theme { get }

    // MARK: - Interface

    func auth() -> UIViewController
    func plan(_ user: User) -> UIViewController
    func profile(_ user: User) -> UIViewController
    func statistics(_ user: User) -> UIViewController
    func rating(_ user: User) -> UIViewController
    func tabBar() -> UIViewController

    // MARK: - Interface. Support

    func containerMovableCollection(_ items: [CollectionItem], heightMode: CollectionViewController.HeightMode, cellDelegate: CollectionCellDelegate?) -> UIViewController
}
