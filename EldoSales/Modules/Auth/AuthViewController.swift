//
//  AuthViewController.swift
//  EldoSales
//
//  Created by Nikita Bondar on 14.06.2021.
//

import PinLayout
import PromiseKit
import UIKit

class AuthViewController: UIViewController {
    // MARK: - State

    private enum State: Equatable {
        // MARK: - Cases

        case loading
        case login
        case loggingIn
        case loggedIn(User?)

        // MARK: - Interface

        static func ==(lhs: State, rhs: State) -> Bool {
            if case .loading = lhs, case .loading = rhs { return true }
            if case .login = lhs, case .login = rhs { return true }
            if case .loggingIn = lhs, case .loggingIn = rhs { return true }
            if case .loggedIn = lhs, case .loggedIn = rhs { return true }
            return false
        }
    }

    // MARK: - Properties

    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        [.portrait]
    }

    private let services: Services
    private let storage: UserDefaults
    private let moduleFactory: ModuleFactory
    private let theme: Theme

    private var state: State = .loading

    // MARK: - Views

    private let imageView = UIImageView()

    private let authTextField = TextField()
    private let passwordTextField = TextField()
    private let authButton = UIButton()

    private let activityIndicatorView = UIActivityIndicatorView(style: .gray)

    // MARK: - Init

    init(services: Services,
         storage: UserDefaults,
         moduleFactory: ModuleFactory,
         theme: Theme) {
        self.services = services
        self.storage = storage
        self.moduleFactory = moduleFactory
        self.theme = theme

        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable, message: "Use init(storage:, moduleFactory:, theme:) instead")
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        performState(state)

        Queue.main(after: 1) { [weak self] in
            guard let self = self else { return }

//            let isLoggedIn = self.storage.bool(forKey: StorageKeys.isLoggedIn)
//            self.performState(isLoggedIn ? .loggedIn : .login)
            self.performState(.login)
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        performLayout()
    }

    // MARK: - Private. Setup

    private func setupViews() {
        navigationController?.setNavigationBarHidden(true, animated: false)
        view.backgroundColor = theme.colors.elementBlue

        view.addSubview(imageView)

        view.addSubview(authTextField)
        view.addSubview(passwordTextField)
        view.addSubview(authButton)

        view.addSubview(activityIndicatorView)

        imageView.image = theme.images.circleAppIcon
        imageView.contentMode = .scaleAspectFit

        authTextField.alpha = 0
        authTextField.textAlignment = .left
        authTextField.tintColor = theme.colors.textWhite
        authTextField.textColor = theme.colors.textWhite
        authTextField.font = theme.fonts.font17
        authTextField.attributedPlaceholder = NSAttributedString(string: Strings.authPlaceholder, attributes: [.foregroundColor: theme.colors.textLightGray])
        authTextField.backgroundColor = theme.colors.elementLightBlue
        authTextField.layer.cornerRadius = 8
        authTextField.textContentType = .username
        authTextField.autocapitalizationType = .none
        authTextField.returnKeyType = .next
        authTextField.padding = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        authTextField.delegate = self

        passwordTextField.alpha = 0
        passwordTextField.textAlignment = .left
        passwordTextField.tintColor = theme.colors.textWhite
        passwordTextField.textColor = theme.colors.textWhite
        passwordTextField.font = theme.fonts.font17
        passwordTextField.attributedPlaceholder = NSAttributedString(string: Strings.passwordPlaceholder, attributes: [.foregroundColor: theme.colors.textLightGray])
        passwordTextField.backgroundColor = theme.colors.elementLightBlue
        passwordTextField.layer.cornerRadius = 8
        passwordTextField.textContentType = .password
        passwordTextField.isSecureTextEntry = true
        passwordTextField.autocapitalizationType = .none
        passwordTextField.padding = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        passwordTextField.returnKeyType = .done
        passwordTextField.delegate = self

        authButton.alpha = 0
        authButton.backgroundColor = theme.colors.elementGreen
        authButton.layer.cornerRadius = 8
        authButton.titleLabel?.font = theme.fonts.font15Bold
        authButton.setTitle(Strings.authButtonTitle, for: .normal)
        authButton.setTitleColor(theme.colors.textWhite, for: .normal)
        authButton.addTarget(self, action: #selector(authButtonAction(_:)), for: .touchUpInside)

        activityIndicatorView.color = theme.colors.activityIndicator
        activityIndicatorView.hidesWhenStopped = true

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapGestureRecognizerAction(_:)))
        view.addGestureRecognizer(tapGestureRecognizer)
    }

    // MARK: - Private. Layout

    private func performLayout() {
        guard state != .loggedIn(nil) else { return }

        switch state {
        case .loading:
            imageView.pin
                .center()
                .size(CGSize(width: 100, height: 100))

        case .login, .loggingIn:
            imageView.pin
                .top()
                .size(CGSize(width: 100, height: 100))
                .marginTop(view.pin.safeArea.top + 20)

        default:
            break
        }

        authTextField.pin
            .top(to: imageView.edge.bottom)
            .horizontally()
            .marginTop(view.frame.height < 650 ? 10 : 30)
            .marginLeft(view.pin.safeArea.left + 32)
            .marginRight(view.pin.safeArea.right + 32)
            .height(40)

        passwordTextField.pin
            .top(to: authTextField.edge.bottom)
            .horizontally()
            .marginTop(10)
            .marginLeft(view.pin.safeArea.left + 32)
            .marginRight(view.pin.safeArea.right + 32)
            .height(40)

        authButton.pin
            .top(to: passwordTextField.edge.bottom)
            .horizontally()
            .marginTop(30)
            .marginLeft(view.pin.safeArea.left + 32)
            .marginRight(view.pin.safeArea.right + 32)
            .height(44)

        activityIndicatorView.pin
            .vCenter(to: authButton.edge.vCenter)
            .hCenter(to: authButton.edge.hCenter)
    }

    // MARK: - Private. State

    private func performState(_ state: State) {
        self.state = state

        switch state {
        case .loading:
            imageView.startBouncingAnimation()
            activityIndicatorView.stopAnimating()

            authTextField.setHidden(true, animated: true)
            passwordTextField.setHidden(true, animated: true)
            authButton.setHidden(true, animated: true)

            UIView.animate(withDuration: 0.3) { [weak self] in
                self?.performLayout()
            }

        case .login:
            imageView.stopBouncingAnimation()
            activityIndicatorView.stopAnimating()

            authTextField.setHidden(false, animated: true)
            passwordTextField.setHidden(false, animated: true)
            authButton.setHidden(false, animated: true)

            authTextField.becomeFirstResponder()

            UIView.animate(withDuration: 0.3) { [weak self] in
                self?.performLayout()
            }

        case .loggingIn:
            imageView.stopBouncingAnimation()

            authTextField.setHidden(false, animated: true)
            passwordTextField.setHidden(false, animated: true)
            authButton.setHidden(true, animated: true)

            activityIndicatorView.startAnimating()

            UIView.animate(withDuration: 0.3) { [weak self] in
                self?.performLayout()
            }

        case let .loggedIn(user):
            imageView.stopBouncingAnimation()

            guard user != nil else { fatalError("User can't be nil") }

            let nextController = moduleFactory.tabBar()
            navigationController?.setNavigationBarHidden(false, animated: true)
            navigationController?.setViewControllers([nextController], animated: true)
        }
    }

    // MARK: - Private. Actions

    @objc
    private func authButtonAction(_ sender: UIButton) {
        sender.bounce()
        performLogin()
        view.endEditing(false)
    }

    @objc
    private func tapGestureRecognizerAction(_ sender: UITapGestureRecognizer) {
        view.endEditing(false)
    }

    // MARK: - Private. Help

    private func performLogin() {
        performState(.loggingIn)

        let number = authTextField.text ?? ""

        Queue.global {
            firstly {
                self.services.userService.auth(with: number)
            }.done { [weak self] user in
                self?.performState(.loggedIn(user))
            }.catch { [weak self] _ in
                self?.performState(.login)
            }
        }
    }
}

extension AuthViewController: UITextFieldDelegate {
    // MARK: - Interface

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case authTextField:
            passwordTextField.becomeFirstResponder()

        case passwordTextField:
            performLogin()
            view.endEditing(false)

        default:
            break
        }

        return true
    }
}
