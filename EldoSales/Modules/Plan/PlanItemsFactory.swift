//
//  PlanItemsFactory.swift
//  EldoSales
//
//  Created by Nikita Bondar on 21.06.2021.
//

import UIKit

enum PlanTopBadgeIdentifier: String {
    // MARK: - Cases

    case period
}

enum PlanItemIdentifier {
    // MARK: - Cases

    case allPlan
    case some(String)

    // MARK: - Properties

    var value: String {
        switch self {
        case .allPlan:
            return "allPlan"

        case let .some(identifier):
            return "some_\(identifier)"
        }
    }

    // MARK: - Init

    init?(identifier: String) {
        if identifier == "allPlan" {
            self = .allPlan
        } else if identifier.starts(with: "some_") {
            var identifier = identifier
            identifier.removeFirst(5)
            self = .some(identifier)
        } else {
            return nil
        }
    }
}

enum PlanEditIdentifier: String {
    // MARK: - Cases

    case textField
    case done
}

class PlanItemsFactory {
    // MARK: - Children

    enum ItemIdentifier: String {
        case topBadges
        case bottomBadges
    }

    // MARK: - Properties

    private let dateFormatter = DateFormatter()
    private let numberFormatter = NumberFormatter()
    private let theme: Theme

    // MARK: - Init

    init(theme: Theme) {
        self.theme = theme

        numberFormatter.numberStyle = .decimal
        numberFormatter.maximumFractionDigits = 0
    }

    // MARK: - Interface

    func items(_ users: [User],
               plan: Plan,
               badgePeriodDates: (Date, Date),
               badgeCategoryId: String?,
               badgeContentOffset: [String: CGPoint]) -> [CollectionItem] {
        var items: [CollectionItem] = []

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 26), model: nil))

        let topBadgesIconItem = BadgesItem(
            identifier: ItemIdentifier.topBadges.rawValue,
            strategy: CustomHeightSizingStrategy(height: 35),
            badges: topBadgeItems(badgePeriodDates),
            contentOffset: badgeContentOffset[ItemIdentifier.topBadges.rawValue] ?? .zero,
            model: nil
        )
        items.append(topBadgesIconItem)

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 10), model: nil))

        let bottomBadgesIconItem = BadgesItem(
            identifier: ItemIdentifier.bottomBadges.rawValue,
            strategy: CustomHeightSizingStrategy(height: 35),
            badges: bottomBadgesItems(plan.categories?.compactMap { $0.category } ?? [], selectedCategoryId: badgeCategoryId),
            contentOffset: badgeContentOffset[ItemIdentifier.bottomBadges.rawValue] ?? .zero,
            model: nil
        )
        items.append(bottomBadgesIconItem)

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 20), model: nil))

        let titleItem = TitleItem(strategy: CustomHeightSizingStrategy(height: 44),
                                  title: "План",
                                  alignment: .left,
                                  font: theme.fonts.font20Bold,
                                  model: nil)
        items.append(titleItem)

        let amount = badgeCategoryId == nil ? plan.amount : plan.categories?.first(where: { $0.categoryId == badgeCategoryId })?.amount ?? 0
        let allPlanItem = EmployeeItem(identifier: PlanItemIdentifier.allPlan.value,
                                       strategy: CustomHeightSizingStrategy(height: 60),
                                       number: nil,
                                       imageURL: plan.shop?.image,
                                       title: plan.shop?.name ?? "-",
                                       subtitle: plan.shop?.address,
                                       accessoryTitle: "\(numberFormatter.string(from: NSNumber(value: amount)) ?? "-") ₽",
                                       accessorySubtitle: nil,
                                       showDisclosure: true,
                                       model: nil)
        items.append(allPlanItem)

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 25), model: nil))

        let employeeTitleItem = TitleItem(strategy: CustomHeightSizingStrategy(height: 44),
                                          title: "Индивидуальный план",
                                          alignment: .left,
                                          font: theme.fonts.font20Bold,
                                          model: nil)
        items.append(employeeTitleItem)

        let strategy = CustomHeightSizingStrategy(height: 60)
        for user in users {
            let userAmount = Double(amount) / Double(users.count)
            let item = EmployeeItem(identifier: PlanItemIdentifier.some(user.id ?? "-").value,
                                    strategy: strategy,
                                    number: nil,
                                    imageURL: user.image,
                                    title: user.name ?? "-",
                                    subtitle: user.position?.title ?? "-",
                                    accessoryTitle: "\(numberFormatter.string(from: NSNumber(value: userAmount)) ?? "-") ₽",
                                    accessorySubtitle: nil,
                                    showDisclosure: false,
                                    model: nil)
            items.append(item)
        }

        return items
    }

    func editPlanItems(_ plan: Plan, badgeCategoryId: String?) -> [CollectionItem] {
        var items: [CollectionItem] = []

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 10), model: nil))

        let titleItem = TitleItem(strategy: CustomHeightSizingStrategy(height: 44),
                                  title: "План",
                                  alignment: .left,
                                  font: theme.fonts.font17Bold,
                                  model: nil)
        items.append(titleItem)

        let amount = badgeCategoryId == nil ? plan.amount : plan.categories?.first(where: { $0.category?.id == badgeCategoryId })?.amount ?? 0
        let textEditItem = TextEditItem(identifier: PlanEditIdentifier.textField.rawValue,
                                        strategy: CustomHeightSizingStrategy(height: 44),
                                        text: "\(amount)",
                                        numberFormatter: numberFormatter,
                                        placeholder: "Сумма",
                                        accessoryTitle: "₽",
                                        model: nil)
        items.append(textEditItem)

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 10), model: nil))

        let saveButton = ButtonItem(identifier: PlanEditIdentifier.done.rawValue,
                                    strategy: CustomHeightSizingStrategy(height: 44),
                                    title: "Готово",
                                    titleColor: theme.colors.textGreen,
                                    font: theme.fonts.font16Bold,
                                    model: nil)
        items.append(saveButton)

        return items
    }

    func dateItems(_ periodDates: (Date, Date)) -> [CollectionItem] {
        dateFormatter.dateFormat = "dd.MM.YYYY"

        var items: [CollectionItem] = []

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 10), model: nil))

        let titleItem = TitleItem(strategy: CustomHeightSizingStrategy(height: 44),
                                  title: "Период",
                                  alignment: .left,
                                  font: theme.fonts.font17Bold,
                                  model: nil)
        items.append(titleItem)

        let datesItem = DatesItem(identifier: CollectionPeriodIdentifier.dates.rawValue,
                                  strategy: CustomHeightSizingStrategy(height: 70),
                                  leftDateItem: DateItem(title: "Начало", subtitle: dateFormatter.string(from: periodDates.0), date: periodDates.0, isSelected: true),
                                  rightDateItem: DateItem(title: "Конец", subtitle: dateFormatter.string(from: periodDates.1), date: periodDates.1, isSelected: false),
                                  model: nil)
        items.append(datesItem)

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 10), model: nil))

        let datePickerItem = DatePickerItem(identifier: CollectionPeriodIdentifier.picker.rawValue,
                                            strategy: CustomHeightSizingStrategy(height: 216),
                                            date: periodDates.0,
                                            model: nil)
        items.append(datePickerItem)

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 10), model: nil))

        let saveButton = ButtonItem(identifier: CollectionPeriodIdentifier.save.rawValue,
                                    strategy: CustomHeightSizingStrategy(height: 44),
                                    title: "Сохранить",
                                    titleColor: theme.colors.textGreen,
                                    font: theme.fonts.font16Bold,
                                    model: nil)
        items.append(saveButton)

        return items
    }

    // MARK: - Private. Help

    private func topBadgeItems(_ period: (Date, Date)) -> [BadgeItem] {
        dateFormatter.dateFormat = "dd.MM"
        let periodTitle: String

        let calendar: Calendar = .current
        if calendar.component(.month, from: period.0) == calendar.component(.month, from: period.1)
            && calendar.component(.day, from: period.0) == calendar.component(.day, from: period.1) {
            periodTitle = dateFormatter.string(from: period.0)
        } else {
            periodTitle = "\(dateFormatter.string(from: period.0)) - \(dateFormatter.string(from: period.1))"
        }

        return [
            BadgeItem(identifier: PlanTopBadgeIdentifier.period.rawValue,
                      title: periodTitle,
                      image: theme.images.chevronDown,
                      isSelected: true,
                      model: period)
        ]
    }
    
    private func bottomBadgesItems(_ categories: [Category], selectedCategoryId: String?) -> [BadgeItem] {
        var items: [BadgeItem] = []
        items.append(BadgeItem(identifier: PlanItemIdentifier.allPlan.value, title: "Общий оборот", image: nil, isSelected: selectedCategoryId == nil, model: nil))

        for category in categories {
            items.append(BadgeItem(identifier: PlanItemIdentifier.some(category.id ?? "-").value,
                                   title: category.name ?? "",
                                   image: nil,
                                   isSelected: selectedCategoryId == category.id,
                                   model: category))
        }

        return items
    }
}
