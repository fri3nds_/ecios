//
//  PlanViewController.swift
//  EldoSales
//
//  Created by Nikita Bondar on 21.06.2021.
//

import PromiseKit
import UIKit

class PlanViewController: UIViewController {
    // MARK: - Properties

    private let cellRegistrator: CellRegistrator
    private let moduleFactory: ModuleFactory
    private let theme: Theme

    private let user: User
    private let services: Services
    private let dateFormatter = DateFormatter()

    private var plan: Plan?
    private var users: [User] = []

    private let itemsFactory: PlanItemsFactory
    private var items: [CollectionItem] = []

    private var badgePeriodDates: (Date, Date) = (Date(), Date())
    private var badgeCategoryId: String?

    private var badgeContentOffset: [String: CGPoint] = [:]

    // MARK: - Views

    private let collectionView: UICollectionView

    private weak var collectionViewController: CollectionViewController?

    // MARK: - Init

    init(moduleFactory: ModuleFactory,
         services: Services,
         theme: Theme,
         user: User) {
        self.moduleFactory = moduleFactory
        self.theme = theme

        self.user = user
        self.services = services
        self.itemsFactory = PlanItemsFactory(theme: theme)

        collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        cellRegistrator = CellRegistrator(collectionView: collectionView)

        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable, message: "Use init(moduleFactory:, theme:) instead")
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
        refetchData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationTitle = Strings.planTitle
        reloadItems()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        performLayout()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        coordinator.animate { [weak self] context in
            self?.collectionView.collectionViewLayout.invalidateLayout()
        }
    }

    // MARK: - Private. Setup

    private func setupViews() {
        view.backgroundColor = theme.colors.background

        tabBarItem.title = Strings.planTitle
        tabBarItem.image = theme.images.planIcon

        view.addSubview(collectionView)

        if let flowCollectionLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowCollectionLayout.minimumInteritemSpacing = 0
            flowCollectionLayout.minimumLineSpacing = 0
            flowCollectionLayout.sectionInset = .zero
            flowCollectionLayout.scrollDirection = .vertical
        }

        collectionView.backgroundColor = theme.colors.background
        collectionView.alwaysBounceVertical = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.delegate = self
        collectionView.dataSource = self

        dateFormatter.dateFormat = "dd.MM.YYYY"
    }

    // MARK: - Private. Layout

    private func performLayout() {
        collectionView.pin
            .all()
            .marginTop(view.pin.safeArea.top)
    }

    // MARK: - Private. Fetch

    private func refetchData() {
        Queue.global {
            firstly {
                when(fulfilled: self.services.planService.fetchPlan(self.user), self.services.shopService.fetchUsers(self.user.shop!))
            }.done { plan, users in
                self.plan = plan
                self.users = users

                self.reloadItems()
            }.catch {
                print("error: \($0.localizedDescription)")
            }
        }
    }

    // MARK: - Private. Items

    private func reloadItems() {
        guard let plan = plan else { return }

        items = itemsFactory.items(users,
                                   plan: plan,
                                   badgePeriodDates: badgePeriodDates,
                                   badgeCategoryId: badgeCategoryId,
                                   badgeContentOffset: badgeContentOffset)
        collectionView.reloadData()
    }
}

extension PlanViewController: UICollectionViewDelegate {

}

extension PlanViewController: UICollectionViewDelegateFlowLayout {
    // MARK: - Interface

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        items[indexPath.row].size(in: collectionView.frame.size)
    }
}

extension PlanViewController: UICollectionViewDataSource {
    // MARK: - Interface

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        items.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = items[indexPath.row]
        cellRegistrator.registerIfNeeded(item.cellClass, for: item.reuseIdentifier)

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: item.reuseIdentifier, for: indexPath) as? CollectionCell
        cell?.delegate = self
        cell?.setup(with: theme)
        cell?.fill(with: item, animated: true)

        return cell ?? UICollectionViewCell()
    }
}

extension PlanViewController: CollectionCellDelegate {
    // MARK: - Interface

    func collectionCell(_ collectionCell: CollectionCell, didTap item: CollectionItem) {
        if let planIdentifier = PlanItemIdentifier(identifier: item.identifier) {
            switch planIdentifier {
            case .allPlan:
                guard let plan = plan else { return }

                let containerMovableViewController = moduleFactory.containerMovableCollection(
                    itemsFactory.editPlanItems(plan, badgeCategoryId: badgeCategoryId),
                    heightMode: .default,
                    cellDelegate: self
                )
                topNavigationController?.present(containerMovableViewController, animated: false, completion: nil)

                collectionViewController = containerMovableViewController.extractCollectionViewController()

            default:
                break
            }
        }

        if let identifier = CollectionPeriodIdentifier(rawValue: item.identifier), identifier == .save {
            guard let datesItem = collectionViewController?.items.first(where: { $0.identifier == CollectionPeriodIdentifier.dates.rawValue }) as? DatesItem else { return }

            badgePeriodDates = (datesItem.leftDateItem.date, datesItem.rightDateItem.date)
            reloadItems()

            collectionViewController?.hide()
        }

        if let identifier = PlanEditIdentifier(rawValue: item.identifier), identifier == .done {
            guard let textEditItem = collectionViewController?.items.first(where: { $0.identifier == PlanEditIdentifier.textField.rawValue }) as? TextEditItem else { return }
            guard let amount = Int64(textEditItem.text?.toNumber ?? "") else { return }

            if let badgeCategoryId = badgeCategoryId {
                plan?.categories?.first(where: { $0.category?.id == badgeCategoryId })?.amount = amount
            } else {
                let categoriesCount = Int64(plan?.categories?.count ?? 0)
                let categoryAmount = amount / max(1, categoriesCount)
                plan?.categories?.forEach { $0.amount = categoryAmount }
            }

            refetchData()
            collectionViewController?.hide()
        }
    }
}

extension PlanViewController: BadgesCellDelegate {
    // MARK: - Interface

    func badgesCell(_ badgesCell: BadgesCell, didTap item: BadgesItem, badge: BadgeItem) {
        if let identifier = PlanTopBadgeIdentifier(rawValue: badge.identifier) {
            switch identifier {
            case .period:
                let containerMovableViewController = moduleFactory.containerMovableCollection(
                    itemsFactory.dateItems(badgePeriodDates),
                    heightMode: .byContent,
                    cellDelegate: self
                )
                topNavigationController?.present(containerMovableViewController, animated: false, completion: nil)

                collectionViewController = containerMovableViewController.extractCollectionViewController()
            }
        }

        if let identifier = PlanItemIdentifier(identifier: badge.identifier) {
            switch identifier {
            case .allPlan:
                badgeCategoryId = nil

            case let .some(identifier):
                badgeCategoryId = identifier
            }

            refetchData()
        }
    }

    func badgesCell(_ badgesCell: BadgesCell, item: BadgesItem, didChange contentOffset: CGPoint) {
        badgeContentOffset[item.identifier] = contentOffset
    }
}

// MARK: - Collection controller cells

extension PlanViewController: DatesCellDelegate {
    // MARK: - Interface

    func datesCell(_ datesCell: DatesCell, didTap item: DatesItem, dateItem: DateItem) {
        guard let datePickerItem = collectionViewController?.items.first(where: { $0.identifier == CollectionPeriodIdentifier.picker.rawValue }) as? DatePickerItem else { return }

        item.leftDateItem.isSelected = item.leftDateItem == dateItem
        item.rightDateItem.isSelected = item.rightDateItem == dateItem

        datePickerItem.date = item.leftDateItem.isSelected ? item.leftDateItem.date : item.rightDateItem.date

        collectionViewController?.refillItems([item, datePickerItem])
    }
}

extension PlanViewController: DatePickerCellDelegate {
    // MARK: - Interface

    func datePickerCell(_ datePickerCell: DatePickerCell, didChange date: Date) {
        guard let collectionViewController = collectionViewController else { return }
        guard let datesItem = collectionViewController.items.first(where: { $0.identifier == CollectionPeriodIdentifier.dates.rawValue }) as? DatesItem else { return }

        if datesItem.leftDateItem.isSelected {
            datesItem.leftDateItem.subtitle = dateFormatter.string(from: date)
            datesItem.leftDateItem.date = date
        } else {
            datesItem.rightDateItem.subtitle = dateFormatter.string(from: date)
            datesItem.rightDateItem.date = date
        }

        collectionViewController.refillItems([datesItem])
    }
}

extension PlanViewController: TextEditCellDelegate {
    // MARK: - Interface

    func textEditCell(_ textEditCell: TextEditCell, item: TextEditItem, textDidChange text: String) {
        
    }
}
