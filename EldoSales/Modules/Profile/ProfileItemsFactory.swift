//
//  ProfileItemsFactory.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class ProfileItemsFactory {
    // MARK: - Children

    enum ItemIdentifier: String {
        case logout
    }

    // MARK: - Properties

    private let theme: Theme

    // MARK: - Init

    init(theme: Theme) {
        self.theme = theme
    }

    // MARK: - Interface

    func items(_ user: User) -> [CollectionItem] {
        var items: [CollectionItem] = []

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 16),
                               backgroundColor: theme.colors.collectionBackground,
                               model: nil))

        let userItem = UserItem(
            identifier: "userItem",
            strategy: CustomHeightSizingStrategy(height: 75),
            imageURL: user.image,
            title: user.name,
            subtitle: user.position?.title ?? "-",
            model: user
        )
        items.append(userItem)

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 30),
                               backgroundColor: theme.colors.collectionBackground,
                               model: nil))

        let infoTitleItem = TitleItem(identifier: "info",
                                      strategy: CustomHeightSizingStrategy(height: 34),
                                      title: "ОСНОВНОЕ",
                                      alignment: .left,
                                      textColor: theme.colors.subtitleColor,
                                      font: theme.fonts.font13,
                                      backgroundColor: theme.colors.collectionBackground,
                                      model: nil)
        items.append(infoTitleItem)

        let titles: [[String]] = [["Табельный номер", user.personnelNumber ?? "-"], ["Адрес филиала", user.shop?.address ?? "-"], ["Моб. телефон", user.phoneNumber ?? "-"]]
        for title in titles {
            let titlesItem = TitlesItem(strategy: CustomHeightSizingStrategy(height: 44),
                                        title: title[0],
                                        subtitle: title[1],
                                        showDisclosure: false,
                                        model: nil)
            items.append(titlesItem)
        }

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 16), backgroundColor: theme.colors.collectionBackground, model: nil))

        let techInfoItem = TitleItem(identifier: "techInfo",
                                     strategy: CustomHeightSizingStrategy(height: 34),
                                     title: "ТЕХНИЧЕСКАЯ ИНФОРМАЦИЯ",
                                     alignment: .left,
                                     textColor: theme.colors.subtitleColor,
                                     font: theme.fonts.font13,
                                     backgroundColor: theme.colors.collectionBackground,
                                     model: nil)
        items.append(techInfoItem)

        let techTitles: [[String]] = [["Оформление", "Светлое"]]
        for title in techTitles {
            let titlesItem = TitlesItem(strategy: CustomHeightSizingStrategy(height: 44),
                                        title: title[0],
                                        subtitle: title[1],
                                        showDisclosure: true,
                                        model: nil)
            items.append(titlesItem)
        }

        let pushItem = SwitchItem(identifier: "push",
                                  strategy: CustomHeightSizingStrategy(height: 44),
                                  on: true,
                                  title: "Push-уведомления",
                                  model: nil)
        items.append(pushItem)

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 16), backgroundColor: theme.colors.collectionBackground, model: nil))

        let appInfoItem = TitleItem(identifier: "appInfo",
                                    strategy: CustomHeightSizingStrategy(height: 34),
                                    title: "О ПРИЛОЖЕНИИ",
                                    alignment: .left,
                                    textColor: theme.colors.subtitleColor,
                                    font: theme.fonts.font13,
                                    backgroundColor: theme.colors.collectionBackground,
                                    model: nil)
        items.append(appInfoItem)

        let versionItem = TitlesItem(identifier: "version",
                                     strategy: CustomHeightSizingStrategy(height: 44),
                                     title: "Версия",
                                     subtitle: (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String) ?? "",
                                     showDisclosure: false,
                                     model: nil)
        items.append(versionItem)

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 6), backgroundColor: theme.colors.collectionBackground, model: nil))

        let logoutItem = ButtonItem(identifier: ItemIdentifier.logout.rawValue,
                                    strategy: CustomHeightSizingStrategy(height: 44),
                                    title: "Выйти",
                                    titleColor: theme.colors.red,
                                    font: theme.fonts.font16Bold,
                                    backgroundColor: theme.colors.collectionBackground,
                                    model: nil)
        items.append(logoutItem)

        return items
    }
}
