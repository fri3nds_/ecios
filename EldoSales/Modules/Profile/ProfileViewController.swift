//
//  ProfileViewController.swift
//  EldoSales
//
//  Created by Nikita Bondar on 19.06.2021.
//

import UIKit

class ProfileViewController: UIViewController {
    // MARK: - Properties

    private let cellRegistrator: CellRegistrator
    private let moduleFactory: ModuleFactory
    private let theme: Theme

    private let user: User
    private let services: Services

    private let itemsFactory: ProfileItemsFactory
    private var items: [CollectionItem] = []

    // MARK: - Views

    private let collectionView: UICollectionView

    // MARK: - Init

    init(moduleFactory: ModuleFactory, services: Services, theme: Theme, user: User) {
        self.moduleFactory = moduleFactory
        self.theme = theme

        self.user = user
        self.services = services
        self.itemsFactory = ProfileItemsFactory(theme: theme)

        collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        cellRegistrator = CellRegistrator(collectionView: collectionView)

        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable, message: "Use init(moduleFactory:, theme:) instead")
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationTitle = Strings.profileTitle

        items = itemsFactory.items(user)
        collectionView.reloadData()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        performLayout()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        coordinator.animate { [weak self] context in
            self?.collectionView.collectionViewLayout.invalidateLayout()
        }
    }

    // MARK: - Private. Setup

    private func setupViews() {
        view.backgroundColor = theme.colors.background

        tabBarItem.title = Strings.profileTitle
        tabBarItem.image = theme.images.profileIcon
        
        view.addSubview(collectionView)

        if let flowCollectionLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowCollectionLayout.minimumInteritemSpacing = 0
            flowCollectionLayout.minimumLineSpacing = 0
            flowCollectionLayout.sectionInset = .zero
            flowCollectionLayout.scrollDirection = .vertical
        }

        collectionView.backgroundColor = theme.colors.collectionBackground
        collectionView.alwaysBounceVertical = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.delegate = self
        collectionView.dataSource = self
    }

    // MARK: - Private. Layout

    private func performLayout() {
        collectionView.pin
            .all()
            .marginTop(view.pin.safeArea.top)
    }
}

extension ProfileViewController: UICollectionViewDelegate {

}

extension ProfileViewController: UICollectionViewDelegateFlowLayout {
    // MARK: - Interface

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        items[indexPath.row].size(in: collectionView.frame.size)
    }
}

extension ProfileViewController: UICollectionViewDataSource {
    // MARK: - Interface

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        items.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = items[indexPath.row]
        cellRegistrator.registerIfNeeded(item.cellClass, for: item.reuseIdentifier)

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: item.reuseIdentifier, for: indexPath) as? CollectionCell
        cell?.delegate = self
        cell?.setup(with: theme)
        cell?.fill(with: item, animated: true)

        return cell ?? UICollectionViewCell()
    }
}

extension ProfileViewController: CollectionCellDelegate {
    // MARK: - Interface

    func collectionCell(_ collectionCell: CollectionCell, didTap item: CollectionItem) {
        guard let identifier = ProfileItemsFactory.ItemIdentifier(rawValue: item.identifier) else { return }

        switch identifier {
        case .logout:
            services.userService.logout().done {
                self.topNavigationController?.setViewControllers([self.moduleFactory.auth()], animated: true)
            }.catch {
                print($0.localizedDescription)
            }
        }
    }
}
