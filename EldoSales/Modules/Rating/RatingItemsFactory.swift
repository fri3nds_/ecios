//
//  RatingItemsFactory.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

enum RatingTopBadgeIdentifier: String {
    // MARK: - Cases

    case entities
    case period
}

class RatingItemsFactory {
    // MARK: - Children

    enum ItemIdentifier: String {
        case topBadges
        case bottomBadges
    }

    // MARK: - Properties

    private let dateFormatter = DateFormatter()
    private let numberFormatter = NumberFormatter()
    private let theme: Theme

    // MARK: - Init

    init(theme: Theme) {
        self.theme = theme

        numberFormatter.numberStyle = .decimal
        numberFormatter.maximumFractionDigits = 0
    }

    // MARK: - Interface

    func items(_ users: [User],
               plan: Plan,
               currentUser: User,
               badgePeriodDates: (Date, Date),
               badgeCategoryId: String?,
               badgeContentOffset: [String: CGPoint]) -> [CollectionItem] {
        var items: [CollectionItem] = []

        items.append(contentsOf: topItems(badgePeriodDates: badgePeriodDates, badgeContentOffset: badgeContentOffset, sortParameter: .employees))

        var columnAmounts: [Int64] = []
        let topAmount = users.first?.salesAmount(plan, categoryId: badgeCategoryId) ?? 0

        if topAmount > 0 {
            columnAmounts.append(max(0, topAmount))
            columnAmounts.append(max(0, topAmount * 3 / 4))
            columnAmounts.append(max(0, topAmount * 2 / 4))
            columnAmounts.append(max(0, topAmount * 1 / 4))
            columnAmounts.append(0)
        }

        let diagramsItem = DiagramsItem(
            identifier: "diagrams",
            strategy: DiagramsItemStrategy(),
            mode: .column,
            circleTitle: "",
            circleSubtitle: "",
            circleMainPercent: 0,
            circleBackgroundPercent: 0,
            columnTitles: columnAmounts.map { "\(numberFormatter.string(from: NSNumber(value: $0)) ?? "-") ₽" },
            columnItems: diagramItems(users, plan: plan, currentUser: currentUser, selectedCategoryId: badgeCategoryId, topAmount: topAmount),
            columnInterItemInset: 16,
            columnInsets: UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20),
            model: nil
        )
        items.append(diagramsItem)

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 10), model: nil))
        items.append(SeparatorItem(strategy: CustomHeightSizingStrategy(height: 1), model: nil))

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 18), model: nil))

        let badgesItem = BadgesItem(identifier: ItemIdentifier.bottomBadges.rawValue,
                                    strategy: CustomHeightSizingStrategy(height: 35),
                                    badges: bottomBadgesItems(plan.categories?.compactMap { $0.category } ?? [], selectedCategoryId: badgeCategoryId),
                                    contentOffset: badgeContentOffset[ItemIdentifier.bottomBadges.rawValue] ?? .zero,
                                    model: nil)
        items.append(badgesItem)

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 8), model: nil))

        items.append(contentsOf: employeeItems(users, plan: plan, currentUser: currentUser, selectedCategoryId: badgeCategoryId))

        return items
    }

    func items(_ plans: [Plan],
               currentShop: Shop,
               badgePeriodDates: (Date, Date),
               badgeCategoryId: String?,
               badgeContentOffset: [String: CGPoint]) -> [CollectionItem] {
        var items: [CollectionItem] = []

        items.append(contentsOf: topItems(badgePeriodDates: badgePeriodDates, badgeContentOffset: badgeContentOffset, sortParameter: .shops))

        var columnAmounts: [Int64] = []
        let sales = badgeCategoryId == nil ? plans.first?.sales ?? [] : plans.first?.sales?.filter { $0.product?.category?.id == badgeCategoryId } ?? []
        var topAmount: Int64 = 0
        sales.forEach { topAmount += $0.product?.price ?? 0 }

        if topAmount > 0 {
            columnAmounts.append(max(0, topAmount))
            columnAmounts.append(max(0, topAmount * 3 / 4))
            columnAmounts.append(max(0, topAmount * 2 / 4))
            columnAmounts.append(max(0, topAmount * 1 / 4))
            columnAmounts.append(0)
        }

        let diagramsItem = DiagramsItem(
            identifier: "diagrams",
            strategy: DiagramsItemStrategy(),
            mode: .column,
            circleTitle: "",
            circleSubtitle: "",
            circleMainPercent: 0,
            circleBackgroundPercent: 0,
            columnTitles: columnAmounts.map { "\(numberFormatter.string(from: NSNumber(value: $0)) ?? "-") ₽" },
            columnItems: diagramItems(plans, currentShop: currentShop, selectedCategoryId: badgeCategoryId, topAmount: topAmount),
            columnInterItemInset: 16,
            columnInsets: UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20),
            model: nil
        )
        items.append(diagramsItem)

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 10), model: nil))
        items.append(SeparatorItem(strategy: CustomHeightSizingStrategy(height: 1), model: nil))

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 18), model: nil))

        let badgesItem = BadgesItem(identifier: ItemIdentifier.bottomBadges.rawValue,
                                    strategy: CustomHeightSizingStrategy(height: 35),
                                    badges: bottomBadgesItems(plans.first?.categories?.compactMap { $0.category } ?? [], selectedCategoryId: badgeCategoryId),
                                    contentOffset: badgeContentOffset[ItemIdentifier.bottomBadges.rawValue] ?? .zero,
                                    model: nil)
        items.append(badgesItem)

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 8), model: nil))

        items.append(contentsOf: shopItems(plans, currentShop: currentShop, selectedCategoryId: badgeCategoryId))

        return items
    }

    func entitiesBadgeItems(_ selectedEntityIdentifier: RatingSortParameter) -> [CollectionItem] {
        var items: [CollectionItem] = []

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 10), model: nil))

        let titleItem = TitleItem(strategy: CustomHeightSizingStrategy(height: 44),
                                  title: "План",
                                  alignment: .left,
                                  font: theme.fonts.font17Bold,
                                  model: nil)
        items.append(titleItem)

        for parameter in RatingSortParameter.allCases {
            items.append(SelectItem(identifier: parameter.rawValue,
                                    strategy: CustomHeightSizingStrategy(height: 44),
                                    title: parameter.title,
                                    isSelected: parameter == selectedEntityIdentifier,
                                    model: parameter))
        }

        return items
    }

    func dateItems(_ periodDates: (Date, Date)) -> [CollectionItem] {
        dateFormatter.dateFormat = "dd.MM.YYYY"

        var items: [CollectionItem] = []

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 10), model: nil))

        let titleItem = TitleItem(strategy: CustomHeightSizingStrategy(height: 44),
                                  title: "Период",
                                  alignment: .left,
                                  font: theme.fonts.font17Bold,
                                  model: nil)
        items.append(titleItem)

        let datesItem = DatesItem(identifier: CollectionPeriodIdentifier.dates.rawValue,
                                  strategy: CustomHeightSizingStrategy(height: 70),
                                  leftDateItem: DateItem(title: "Начало", subtitle: dateFormatter.string(from: periodDates.0), date: periodDates.0, isSelected: true),
                                  rightDateItem: DateItem(title: "Конец", subtitle: dateFormatter.string(from: periodDates.1), date: periodDates.1, isSelected: false),
                                  model: nil)
        items.append(datesItem)

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 10), model: nil))

        let datePickerItem = DatePickerItem(identifier: CollectionPeriodIdentifier.picker.rawValue,
                                            strategy: CustomHeightSizingStrategy(height: 216),
                                            date: periodDates.0,
                                            model: nil)
        items.append(datePickerItem)

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 10), model: nil))

        let saveButton = ButtonItem(identifier: CollectionPeriodIdentifier.save.rawValue,
                                    strategy: CustomHeightSizingStrategy(height: 44),
                                    title: "Сохранить",
                                    titleColor: theme.colors.textGreen,
                                    font: theme.fonts.font16Bold,
                                    model: nil)
        items.append(saveButton)

        return items
    }

    // MARK: - Private. Items Help

    private func topItems(badgePeriodDates: (Date, Date), badgeContentOffset: [String: CGPoint], sortParameter: RatingSortParameter) -> [CollectionItem] {
        var items: [CollectionItem] = []

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 16), model: nil))

        let badgesIconItem = BadgesItem(
            identifier: ItemIdentifier.topBadges.rawValue,
            strategy: CustomHeightSizingStrategy(height: 35),
            badges: topBadgeItems(sortParameter, period: badgePeriodDates),
            contentOffset: badgeContentOffset[ItemIdentifier.topBadges.rawValue] ?? .zero,
            model: nil
        )
        items.append(badgesIconItem)

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 10), model: nil))

        let updateItem = TitleItem(strategy: CustomHeightSizingStrategy(height: 29),
                                   title: "Обновлено в 9:00",
                                   alignment: .left,
                                   textColor: theme.colors.subtitleColor,
                                   font: theme.fonts.font14,
                                   model: nil)
        items.append(updateItem)

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 10), model: nil))

        return items
    }

    // MARK: - Private. Help

    private func topBadgeItems(_ sortParameter: RatingSortParameter, period: (Date, Date)) -> [BadgeItem] {
        dateFormatter.dateFormat = "dd.MM"
        let periodTitle: String

        let calendar: Calendar = .current
        if calendar.component(.month, from: period.0) == calendar.component(.month, from: period.1)
            && calendar.component(.day, from: period.0) == calendar.component(.day, from: period.1) {
            periodTitle = dateFormatter.string(from: period.0)
        } else {
            periodTitle = "\(dateFormatter.string(from: period.0)) - \(dateFormatter.string(from: period.1))"
        }

        return [
            BadgeItem(identifier: RatingTopBadgeIdentifier.entities.rawValue,
                      title: sortParameter.title,
                      image: theme.images.chevronDown,
                      isSelected: true,
                      model: sortParameter),
            BadgeItem(identifier: RatingTopBadgeIdentifier.period.rawValue,
                      title: periodTitle,
                      image: theme.images.chevronDown,
                      isSelected: true,
                      model: period)
        ]
    }

    private func diagramItems(_ users: [User], plan: Plan, currentUser: User, selectedCategoryId: String?, topAmount: Int64) -> [DiagramColumnItem] {
        var items: [DiagramColumnItem] = []

        let strategy = CustomWidthSizingStrategy(width: 32)
        for (index, user) in users.enumerated() {
            let item = DiagramColumnItem(
                identifier: user.id ?? "-",
                strategy: strategy,
                topTitle: nil,
                topTitleColor: nil,
                bottomTitle: "\(index + 1)",
                bottomTitleColor: currentUser.id == user.id ? theme.colors.textGreen : theme.colors.textBlue,
                percent: Double(user.salesAmount(plan, categoryId: selectedCategoryId)) / Double(topAmount),
                percentColor: currentUser.id == user.id ? theme.colors.elementGreen : theme.colors.elementBlue,
                fullPercent: nil,
                model: user
            )
            items.append(item)
        }

        return items
    }

    private func diagramItems(_ plans: [Plan], currentShop: Shop, selectedCategoryId: String?, topAmount: Int64) -> [DiagramColumnItem] {
        var items: [DiagramColumnItem] = []

        let strategy = CustomWidthSizingStrategy(width: 32)
        for (index, plan) in plans.enumerated() {
            let sales = selectedCategoryId == nil ? plan.sales ?? [] : plan.sales?.filter { $0.product?.category?.id == selectedCategoryId } ?? []
            var salesAmount: Int64 = 0
            sales.forEach { salesAmount += $0.product?.price ?? 0 }

            let item = DiagramColumnItem(
                identifier: plan.shop?.id ?? "-",
                strategy: strategy,
                topTitle: nil,
                topTitleColor: nil,
                bottomTitle: "\(index + 1)",
                bottomTitleColor: plan.shop?.id == currentShop.id ? theme.colors.textGreen : theme.colors.textBlue,
                percent: Double(salesAmount) / Double(topAmount),
                percentColor: currentShop.id == plan.shop?.id ? theme.colors.elementGreen : theme.colors.elementBlue,
                fullPercent: nil,
                model: plan.shop
            )
            items.append(item)
        }

        return items
    }

    private func bottomBadgesItems(_ categories: [Category], selectedCategoryId: String?) -> [BadgeItem] {
        var items: [BadgeItem] = []
        items.append(BadgeItem(identifier: PlanItemIdentifier.allPlan.value, title: "Общий оборот", image: nil, isSelected: selectedCategoryId == nil, model: nil))

        for category in categories {
            items.append(BadgeItem(identifier: PlanItemIdentifier.some(category.id ?? "-").value,
                                   title: category.name ?? "",
                                   image: nil,
                                   isSelected: selectedCategoryId == category.id,
                                   model: category))
        }

        return items
    }

    private func employeeItems(_ users: [User], plan: Plan, currentUser: User, selectedCategoryId: String?) -> [CollectionItem] {
        var items: [CollectionItem] = []

        let strategy = CustomHeightSizingStrategy(height: 60)
        for (index, user) in users.enumerated() {
            let item = EmployeeItem(identifier: user.id ?? "-",
                                    strategy: strategy,
                                    number: "\(index + 1).",
                                    imageURL: user.image,
                                    title: user.name ?? "-",
                                    titleColor: user.id == currentUser.id ? theme.colors.textGreen : theme.colors.textBlack,
                                    subtitle: user.position?.title ?? "-",
                                    accessoryTitle: "\(numberFormatter.string(from: NSNumber(value: user.salesAmount(plan, categoryId: selectedCategoryId))) ?? "-") ₽",
                                    accessoryTitleColor: user.id == currentUser.id ? theme.colors.textGreen : theme.colors.textBlack,
                                    accessorySubtitle: "из \(numberFormatter.string(from: NSNumber(value: plan.amount)) ?? "-") ₽",
                                    showDisclosure: true,
                                    model: user)
            items.append(item)
        }

        return items
    }

    private func shopItems(_ plans: [Plan], currentShop: Shop, selectedCategoryId: String?) -> [CollectionItem] {
        var items: [CollectionItem] = []

        let strategy = CustomHeightSizingStrategy(height: 60)
        for (index, plan) in plans.enumerated() {
            let selectedCategory = plan.categories?.first(where: { $0.category?.id == selectedCategoryId })
            let amount = selectedCategoryId == nil ? plan.amount : (selectedCategory?.amount ?? 0)

            let sales = selectedCategoryId == nil ? plan.sales ?? [] : plan.sales?.filter { $0.product?.category?.id == selectedCategoryId } ?? []
            var salesAmount: Int64 = 0
            sales.forEach { salesAmount += $0.product?.price ?? 0 }

            let item = EmployeeItem(identifier: plan.shop?.id ?? "-",
                                    strategy: strategy,
                                    number: "\(index + 1).",
                                    imageURL: plan.shop?.image,
                                    title: plan.shop?.name ?? "-",
                                    titleColor: plan.shop?.id == currentShop.id ? theme.colors.textGreen : theme.colors.textBlack,
                                    subtitle: plan.shop?.address ?? "-",
                                    accessoryTitle: "\(numberFormatter.string(from: NSNumber(value: salesAmount)) ?? "-") ₽",
                                    accessoryTitleColor: plan.shop?.id == currentShop.id ? theme.colors.textGreen : theme.colors.textBlack,
                                    accessorySubtitle: "из \(numberFormatter.string(from: NSNumber(value: amount)) ?? "-") ₽",
                                    showDisclosure: true,
                                    model: plan.shop)
            items.append(item)
        }

        return items
    }
}
