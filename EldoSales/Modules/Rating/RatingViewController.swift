//
//  RatingViewController.swift
//  EldoSales
//
//  Created by Nikita Bondar on 19.06.2021.
//

import PromiseKit
import UIKit

enum RatingSortParameter: String {
    // MARK: - Case

    case employees
    case shops
    case regions

    // MARK: - Properties

    var title: String {
        switch self {
        case .employees:    return "Сотрудников"
        case .shops:        return "Магазинов"
        case .regions:      return "Регионов"
        }
    }
}

extension RatingSortParameter: CaseIterable {}

class RatingViewController: UIViewController {
    // MARK: - Properties

    private let cellRegistrator: CellRegistrator
    private let moduleFactory: ModuleFactory
    private let theme: Theme

    private let user: User
    private let services: Services

    private let itemsFactory: RatingItemsFactory
    private var items: [CollectionItem] = []

    private let dateFormatter = DateFormatter()

    // for employees mode
    private var plan: Plan?
    private var users: [User] = []

    // for shops mode
    private var plans: [Plan] = []
    private var shops: [Shop] = []

    private var badgeSelectedParameter: RatingSortParameter = .employees
    private var badgePeriodDates: (Date, Date) = (Date(), Date())
    private var badgeCategoryId: String?

    private var badgeContentOffset: [String: CGPoint] = [:]

    // MARK: - Views

    private let collectionView: UICollectionView

    private weak var collectionViewController: CollectionViewController?

    // MARK: - Init

    init(moduleFactory: ModuleFactory,
         services: Services,
         theme: Theme,
         user: User) {
        self.moduleFactory = moduleFactory
        self.theme = theme

        self.user = user
        self.services = services
        self.itemsFactory = RatingItemsFactory(theme: theme)

        collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        cellRegistrator = CellRegistrator(collectionView: collectionView)

        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable, message: "Use init(moduleFactory:, theme:) instead")
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
        fetchEmployees()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationTitle = Strings.ratingTitle
        reloadItems()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        performLayout()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        coordinator.animate { [weak self] context in
            self?.collectionView.collectionViewLayout.invalidateLayout()
        }
    }

    // MARK: - Private. Setup

    private func setupViews() {
        view.backgroundColor = theme.colors.background

        tabBarItem.title = Strings.ratingTitle
        tabBarItem.image = theme.images.ratingIcon

        view.addSubview(collectionView)
        
        if let flowCollectionLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowCollectionLayout.minimumInteritemSpacing = 0
            flowCollectionLayout.minimumLineSpacing = 0
            flowCollectionLayout.sectionInset = .zero
            flowCollectionLayout.scrollDirection = .vertical
        }

        collectionView.backgroundColor = .clear
        collectionView.alwaysBounceVertical = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.delegate = self
        collectionView.dataSource = self

        dateFormatter.dateFormat = "dd.MM.YYYY"
    }

    // MARK: - Private. Layout

    private func performLayout() {
        collectionView.pin
            .all()
            .marginTop(view.pin.safeArea.top)
            .marginLeft(view.pin.safeArea.left)
            .marginRight(view.pin.safeArea.right)
    }

    // MARK: - Private. Fetch

    private func refetchData() {
        switch badgeSelectedParameter {
        case .employees:
            fetchEmployees()

        case .shops:
            fetchShops()

        default:
            break
        }
    }

    private func fetchEmployees() {
        Queue.global {
            firstly {
                when(fulfilled: self.services.planService.fetchPlan(self.user), self.services.shopService.fetchUsers(self.user.shop!))
            }.then { plan, users in
                self.services.userService.sorted(users, plan: plan, categoryId: self.badgeCategoryId)
            }.done { result in
                self.plan = result.plan
                self.users = result.users

                self.reloadItems()
            }.catch {
                print("error: \($0.localizedDescription)")
            }
        }
    }

    private func fetchShops() {
        Queue.global {
            firstly {
                self.services.shopService.fetchShops()
            }.then {
                self.services.planService.fetchPlans($0)
            }.then {
                self.services.planService.sortedPlans($0.plans, transitShops: $0.shops, categoryId: self.badgeCategoryId)
            }.done { result in
                self.shops = result.shops
                self.plans = result.plans

                self.reloadItems()
            }.catch {
                print("error: \($0.localizedDescription)")
            }
        }
    }

    // MARK: - Private. Items

    private func reloadItems() {
        guard let plan = plan else { return }

        switch badgeSelectedParameter {
        case .employees:
            items = itemsFactory.items(users,
                                       plan: plan,
                                       currentUser: user,
                                       badgePeriodDates: badgePeriodDates,
                                       badgeCategoryId: badgeCategoryId,
                                       badgeContentOffset: badgeContentOffset)

        case .shops:
            items = itemsFactory.items(plans,
                                       currentShop: user.shop!,
                                       badgePeriodDates: badgePeriodDates,
                                       badgeCategoryId: badgeCategoryId,
                                       badgeContentOffset: badgeContentOffset)

        default:
            break
        }
        collectionView.reloadData()
    }
}

extension RatingViewController: UICollectionViewDelegate {

}

extension RatingViewController: UICollectionViewDelegateFlowLayout {
    // MARK: - Interface

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        items[indexPath.row].size(in: collectionView.frame.size)
    }
}

extension RatingViewController: UICollectionViewDataSource {
    // MARK: - Interface

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        items.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = items[indexPath.row]
        cellRegistrator.registerIfNeeded(item.cellClass, for: item.reuseIdentifier)

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: item.reuseIdentifier, for: indexPath) as? CollectionCell
        cell?.delegate = self
        cell?.setup(with: theme)
        cell?.fill(with: item, animated: true)

        return cell ?? UICollectionViewCell()
    }
}

extension RatingViewController: CollectionCellDelegate {
    // MARK: - Interface

    func collectionCell(_ collectionCell: CollectionCell, didTap item: CollectionItem) {
        if let identifier = RatingSortParameter(rawValue: item.identifier) {
            badgeSelectedParameter = identifier

            switch identifier {
            case .employees:
                fetchEmployees()

            case .shops:
                fetchShops()

            default:
                break
            }

            collectionViewController?.hide()
        }

        if let identifier = CollectionPeriodIdentifier(rawValue: item.identifier), identifier == .save {
            guard let datesItem = collectionViewController?.items.first(where: { $0.identifier == CollectionPeriodIdentifier.dates.rawValue }) as? DatesItem else { return }

            badgePeriodDates = (datesItem.leftDateItem.date, datesItem.rightDateItem.date)
            refetchData()

            collectionViewController?.hide()
        }

        if let user = item.model as? User {
            topNavigationController?.pushViewController(moduleFactory.statistics(user), animated: true)
        }
    }
}

extension RatingViewController: BadgesCellDelegate {
    // MARK: - Interface

    func badgesCell(_ badgesCell: BadgesCell, didTap item: BadgesItem, badge: BadgeItem) {
        if let identifier = RatingTopBadgeIdentifier(rawValue: badge.identifier) {
            switch identifier {
            case .entities:
                let containerMovableViewController = moduleFactory.containerMovableCollection(
                    itemsFactory.entitiesBadgeItems(badgeSelectedParameter),
                    heightMode: .byContent,
                    cellDelegate: self
                )
                topNavigationController?.present(containerMovableViewController, animated: false, completion: nil)

                collectionViewController = containerMovableViewController.extractCollectionViewController()

            case .period:
                let containerMovableViewController = moduleFactory.containerMovableCollection(
                    itemsFactory.dateItems(badgePeriodDates),
                    heightMode: .byContent,
                    cellDelegate: self
                )
                topNavigationController?.present(containerMovableViewController, animated: false, completion: nil)

                collectionViewController = containerMovableViewController.extractCollectionViewController()
            }
        }

        if let identifier = PlanItemIdentifier(identifier: badge.identifier) {
            switch identifier {
            case .allPlan:
                badgeCategoryId = nil

            case let .some(identifier):
                badgeCategoryId = identifier
            }

            refetchData()
        }
    }

    func badgesCell(_ badgesCell: BadgesCell, item: BadgesItem, didChange contentOffset: CGPoint) {
        badgeContentOffset[item.identifier] = contentOffset
    }
}

// MARK: - Collection controller cells

extension RatingViewController: DatesCellDelegate {
    // MARK: - Interface

    func datesCell(_ datesCell: DatesCell, didTap item: DatesItem, dateItem: DateItem) {
        guard let datePickerItem = collectionViewController?.items.first(where: { $0.identifier == CollectionPeriodIdentifier.picker.rawValue }) as? DatePickerItem else { return }

        item.leftDateItem.isSelected = item.leftDateItem == dateItem
        item.rightDateItem.isSelected = item.rightDateItem == dateItem

        datePickerItem.date = item.leftDateItem.isSelected ? item.leftDateItem.date : item.rightDateItem.date

        collectionViewController?.refillItems([item, datePickerItem])
    }
}

extension RatingViewController: DatePickerCellDelegate {
    // MARK: - Interface

    func datePickerCell(_ datePickerCell: DatePickerCell, didChange date: Date) {
        guard let collectionViewController = collectionViewController else { return }
        guard let datesItem = collectionViewController.items.first(where: { $0.identifier == CollectionPeriodIdentifier.dates.rawValue }) as? DatesItem else { return }

        if datesItem.leftDateItem.isSelected {
            datesItem.leftDateItem.subtitle = dateFormatter.string(from: date)
            datesItem.leftDateItem.date = date
        } else {
            datesItem.rightDateItem.subtitle = dateFormatter.string(from: date)
            datesItem.rightDateItem.date = date
        }

        collectionViewController.refillItems([datesItem])
    }
}
