//
//  StatisticsItemsFactory.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

enum StatisticsTopBadgeIdentifier: String {
    // MARK: - Cases

    case plan
    case period
}

enum CollectionPeriodIdentifier: String {
    // MARK: - Cases

    case dates
    case picker
    case save
}

enum StatisticsPlanIdentifier: String {
    // MARK: - Cases

    case shop
    case individual
}

class StatisticsItemsFactory {
    // MARK: - Child

    enum ItemIdentifier: String {
        case topBadges
        case bottomBadges
    }

    // MARK: - Properties

    private let dateFormatter = DateFormatter()
    private let numberFormatter = NumberFormatter()
    private let theme: Theme

    // MARK: - Init

    init(theme: Theme) {
        self.theme = theme

        numberFormatter.numberStyle = .decimal
        numberFormatter.maximumFractionDigits = 0
    }

    // MARK: - Interface

    func items(_ user: User,
               isCurrentUser: Bool,
               plan: Plan,
               planUsersCount: Int,
               badgePlan: PlanType,
               badgePeriodDates: (Date, Date),
               badgeCategoryId: String?,
               badgeContentOffset: [String: CGPoint]) -> [CollectionItem] {
        var items: [CollectionItem] = []

        if isCurrentUser {
            items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 16), model: nil))

            let firstComponent = String(user.name?.split(separator: " ").first ?? "")
            let helloItem = TitleItem(strategy: CustomHeightSizingStrategy(height: 29),
                                      title: "Привет, \(firstComponent)!",
                                      alignment: .left,
                                      model: nil)
            items.append(helloItem)
        }

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 16), model: nil))

        let badgesIconItem = BadgesItem(
            identifier: ItemIdentifier.topBadges.rawValue,
            strategy: CustomHeightSizingStrategy(height: 35),
            badges: badgeItems(badgePlan, period: badgePeriodDates),
            contentOffset: badgeContentOffset[ItemIdentifier.topBadges.rawValue] ?? .zero,
            model: nil
        )
        items.append(badgesIconItem)

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 10), model: nil))

        let badgeCategory = plan.categories?.first(where: { $0.category?.id == badgeCategoryId })

        let amount = badgeCategoryId == nil ? plan.amount : (badgeCategory?.amount ?? 0)
        let allAmount = badgePlan == .common ? amount : amount / Int64(planUsersCount)

        let countTitleItem = TitleItem(strategy: CustomHeightSizingStrategy(height: 29),
                                       title: "Общий оборот — \(numberFormatter.string(from: NSNumber(value: allAmount)) ?? "-") ₽",
                                       alignment: .left,
                                       textColor: theme.colors.textBlack,
                                       font: theme.fonts.font20Bold,
                                       model: nil)
        items.append(countTitleItem)

        let updateItem = TitleItem(strategy: CustomHeightSizingStrategy(height: 29),
                                   title: "Обновлено в 9:00",
                                   alignment: .left,
                                   textColor: theme.colors.subtitleColor,
                                   font: theme.fonts.font14,
                                   model: nil)
        items.append(updateItem)

        let sales = badgePlan == .common ? plan.sales ?? [] : plan.sales?.filter { $0.userId == user.id } ?? []
        let categorySales = badgeCategoryId == nil ? sales : sales.filter { $0.product?.category?.id == badgeCategoryId }

        var salesAmount: Int64 = 0
        categorySales.forEach { salesAmount += $0.product?.price ?? 0 }

        let salesNumber = NSNumber(value: salesAmount)
        let restNumber = NSNumber(value: allAmount - salesAmount)

        let diagramBannerItem = DiagramBannerItem(mainTitleItem: DiagramBannerTitleItem(title: "Выполнено",
                                                                                        subtitle: "\(numberFormatter.string(from: salesNumber) ?? "-") ₽",
                                                                                        isMain: true),
                                                  mainPercent: Double(salesAmount) / Double(max(1, allAmount)),
                                                  backgroundTitleItem: DiagramBannerTitleItem(title: "Осталось",
                                                                                              subtitle: "\(numberFormatter.string(from: restNumber) ?? "-") ₽",
                                                                                              isMain: false),
                                                  backgroundPercent: 1)
        let diagramTitlesItem = DiagramTitlesItem(identifier: "diagram",
                                                  strategy: CustomHeightSizingStrategy(height: 118),
                                                  bannerItem: diagramBannerItem,
                                                  model: nil)
        items.append(diagramTitlesItem)

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 10), model: nil))
        items.append(SeparatorItem(strategy: CustomHeightSizingStrategy(height: 1), model: nil))

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 18), model: nil))

        let bottomBadges = bottomBadgesItems(plan.categories?.compactMap { $0.category } ?? [], selectedCategoryId: badgeCategoryId)
        let badgesItem = BadgesItem(identifier: ItemIdentifier.bottomBadges.rawValue,
                                    strategy: CustomHeightSizingStrategy(height: 35),
                                    badges: bottomBadges,
                                    contentOffset: badgeContentOffset[ItemIdentifier.bottomBadges.rawValue] ?? .zero,
                                    model: nil)
        items.append(badgesItem)

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 4), model: nil))

        items.append(contentsOf: productItems(categorySales))

        return items
    }

    func planBadgeItems(_ badgePlan: PlanType) -> [CollectionItem] {
        var items: [CollectionItem] = []

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 10), model: nil))

        let titleItem = TitleItem(strategy: CustomHeightSizingStrategy(height: 44),
                                  title: "План",
                                  alignment: .left,
                                  font: theme.fonts.font17Bold,
                                  model: nil)
        items.append(titleItem)

        for planType in PlanType.allCases {
            let selectItem = SelectItem(identifier: planType.rawValue,
                                        strategy: CustomHeightSizingStrategy(height: 44),
                                        title: planType == .common ? "Магазина" : "Личный",
                                        isSelected: badgePlan == planType,
                                        model: planType)
            items.append(selectItem)
        }

        return items
    }

    func dateItems(_ periodDates: (Date, Date)) -> [CollectionItem] {
        dateFormatter.dateFormat = "dd.MM.YYYY"

        var items: [CollectionItem] = []

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 10), model: nil))

        let titleItem = TitleItem(strategy: CustomHeightSizingStrategy(height: 44),
                                  title: "Период",
                                  alignment: .left,
                                  font: theme.fonts.font17Bold,
                                  model: nil)
        items.append(titleItem)

        let datesItem = DatesItem(identifier: CollectionPeriodIdentifier.dates.rawValue,
                                  strategy: CustomHeightSizingStrategy(height: 70),
                                  leftDateItem: DateItem(title: "Начало", subtitle: dateFormatter.string(from: periodDates.0), date: periodDates.0, isSelected: true),
                                  rightDateItem: DateItem(title: "Конец", subtitle: dateFormatter.string(from: periodDates.1), date: periodDates.1, isSelected: false),
                                  model: nil)
        items.append(datesItem)

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 10), model: nil))

        let datePickerItem = DatePickerItem(identifier: CollectionPeriodIdentifier.picker.rawValue,
                                            strategy: CustomHeightSizingStrategy(height: 216),
                                            date: periodDates.0,
                                            model: nil)
        items.append(datePickerItem)

        items.append(EmptyItem(strategy: CustomHeightSizingStrategy(height: 10), model: nil))

        let saveButton = ButtonItem(identifier: CollectionPeriodIdentifier.save.rawValue,
                                    strategy: CustomHeightSizingStrategy(height: 44),
                                    title: "Сохранить",
                                    titleColor: theme.colors.textGreen,
                                    font: theme.fonts.font16Bold,
                                    model: nil)
        items.append(saveButton)

        return items
    }

    // MARK: - Private. Help

    private func badgeItems(_ plan: PlanType, period: (Date, Date)) -> [BadgeItem] {
        dateFormatter.dateFormat = "dd.MM"
        let periodTitle: String

        let calendar: Calendar = .current
        if calendar.component(.month, from: period.0) == calendar.component(.month, from: period.1)
            && calendar.component(.day, from: period.0) == calendar.component(.day, from: period.1) {
            periodTitle = dateFormatter.string(from: period.0)
        } else {
            periodTitle = "\(dateFormatter.string(from: period.0)) - \(dateFormatter.string(from: period.1))"
        }

        return [
            BadgeItem(identifier: StatisticsTopBadgeIdentifier.plan.rawValue,
                      title: plan == .common ? "План магазина" : "Личный план",
                      image: theme.images.chevronDown,
                      isSelected: true,
                      model: period),
            BadgeItem(identifier: StatisticsTopBadgeIdentifier.period.rawValue,
                      title: periodTitle,
                      image: theme.images.chevronDown,
                      isSelected: true,
                      model: plan)
        ]
    }

    private func bottomBadgesItems(_ categories: [Category], selectedCategoryId: String?) -> [BadgeItem] {
        var items: [BadgeItem] = []
        items.append(BadgeItem(identifier: PlanItemIdentifier.allPlan.value, title: "Общий оборот", image: nil, isSelected: selectedCategoryId == nil, model: nil))

        for category in categories {
            items.append(BadgeItem(identifier: PlanItemIdentifier.some(category.id ?? "-").value,
                                   title: category.name ?? "",
                                   image: nil,
                                   isSelected: selectedCategoryId == category.id,
                                   model: category))
        }

        return items
    }

    private func productItems(_ sales: [Sale]) -> [CollectionItem] {
        let sizingStrategy = CustomHeightSizingStrategy(height: 80)

        var items: [CollectionItem] = []
        for sale in sales {
            let item = ProductItem(identifier: sale.id ?? "-",
                                   strategy: sizingStrategy,
                                   title: "\(numberFormatter.string(from: NSNumber(value: sale.product?.price ?? 0)) ?? "-") ₽",
                                   subtitle: sale.product?.description ?? "-",
                                   accessoryTitle: "\(sale.count ?? 0) шт.",
                                   imageURL: sale.product?.image,
                                   model: sale)
            items.append(item)
        }
        return items
    }
}
