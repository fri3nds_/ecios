//
//  ViewController.swift
//  EldoSales
//
//  Created by Nikita Bondar on 14.06.2021.
//

import UIKit

class TabBarController: UITabBarController {
    // MARK: - Properties

    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }

    private let theme: Theme

    // MARK: - Init

    init(theme: Theme) {
        self.theme = theme
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable, message: "Use init(theme:) instead")
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }

    override func setViewControllers(_ viewControllers: [UIViewController]?, animated: Bool) {
        super.setViewControllers(viewControllers, animated: animated)
        viewControllers?.forEach { $0.loadViewIfNeeded() }
    }

    // MARK: - Private. Setup

    private func setup() {
        view.backgroundColor = theme.colors.background
        tabBar.tintColor = theme.colors.textGreen
    }
}

