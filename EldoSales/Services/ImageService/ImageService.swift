//
//  ImageService.swift
//  EldoSales
//
//  Created by Nikita Bondar on 22.06.2021.
//

import PromiseKit

enum ImageCategory: String {
    // MARK: - Cases

    case random
    case profile
    case product
}

class ImageService {
    // MARK: - Interface

    func fetchImage(_ category: ImageCategory) -> Promise<UIImage> {
        Promise<UIImage> {
            switch category {
            case .random:
                $0.fulfill(MockImages.shared.randomImages.randomElement()!)

            case .profile:
                $0.fulfill(MockImages.shared.profileImages.randomElement()!)

            case .product:
                $0.fulfill(MockImages.shared.productImages.randomElement()!)
            }
        }
    }
}
