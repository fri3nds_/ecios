//
//  PlanService.swift
//  EldoSales
//
//  Created by Nikita Bondar on 22.06.2021.
//

import PromiseKit

class PlanService {
    // MARK: - Children

    struct FetchPlansResult {
        let shops: [Shop]
        let plans: [Plan]
    }

    struct SortedPlansResult {
        let shops: [Shop]
        let plans: [Plan]
    }

    // MARK: - Interface

    func fetchPlan(_ user: User) -> Promise<Plan> {
        Promise<Plan> {
            if let plan = MockPlans.shared.plans.first(where: { $0.shopId == user.shopId }) {
                $0.fulfill(plan)
            } else {
                $0.reject(Errors.notFound)
            }
        }
    }

    func fetchPlans(_ shops: [Shop]) -> Promise<FetchPlansResult> {
        Promise {
            var plans: [Plan] = []
            shops.forEach { shop in
                plans.append(MockPlans.shared.plans.first(where: { $0.shopId == shop.id })!)
            }
            $0.fulfill(.init(shops: shops, plans: plans))
        }
    }

    func sortedPlans(_ plans: [Plan], transitShops: [Shop], categoryId: String?) -> Promise<SortedPlansResult> {
        Promise {
            let sorted = plans.sorted { f, s in
                let fSales = categoryId == nil ? f.sales ?? [] : f.sales?.filter { $0.product?.category?.id == categoryId } ?? []
                var fAmount: Int64 = 0
                fSales.forEach { fAmount += $0.product?.price ?? 0 }

                let sSales = categoryId == nil ? s.sales ?? [] : s.sales?.filter { $0.product?.category?.id == categoryId } ?? []
                var sAmount: Int64 = 0
                sSales.forEach { sAmount += $0.product?.price ?? 0 }

                return fAmount > sAmount
            }
            $0.fulfill(.init(shops: transitShops, plans: sorted))
        }
    }
}
