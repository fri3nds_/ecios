//
//  Services.swift
//  EldoSales
//
//  Created by Nikita Bondar on 22.06.2021.
//

import Foundation

class Services {
    // MARK: - Properties

    let imageService: ImageService
    let planService: PlanService
    let shopService: ShopService
    let userService: UserService

    // MARK: - Init

    init() {
        imageService = ImageService()
        planService = PlanService()
        shopService = ShopService()
        userService = UserService()
    }
}
