//
//  ShopService.swift
//  EldoSales
//
//  Created by Nikita Bondar on 22.06.2021.
//

import PromiseKit

class ShopService {
    // MARK: - Interface

    func fetchShops() -> Promise<[Shop]> {
        Promise {
            $0.fulfill(MockShops.shared.shops)
        }
    }

    func fetchUsers(_ shop: Shop) -> Promise<[User]> {
        Promise<[User]> {
            let users = MockUsers.shared.users.filter { $0.shopId == shop.id }
            $0.fulfill(users)
        }
    }
}
