//
//  UserService.swift
//  EldoSales
//
//  Created by Nikita Bondar on 22.06.2021.
//

import PromiseKit

class UserService {
    // MARK: - Children

    struct SortedResult {
        let plan: Plan
        let users: [User]
    }

    // MARK: - Properties

    var current: User?

    // MARK: - Interface

    func auth(with personnelNumber: String) -> Promise<User> {
        Promise<User> { resolver in
            if MockUsers.shared.director.personnelNumber == personnelNumber {
                current = MockUsers.shared.director
                resolver.fulfill(MockUsers.shared.director)
            } else if MockUsers.shared.seller.personnelNumber == personnelNumber {
                current = MockUsers.shared.seller
                resolver.fulfill(MockUsers.shared.seller)
            } else {
                resolver.reject(Errors.unauthorized)
            }
        }
    }

    func sorted(_ users: [User], plan: Plan, categoryId: String?) -> Promise<SortedResult> {
        Promise {
            let sorted = users.sorted { $0.salesAmount(plan, categoryId: categoryId) > $1.salesAmount(plan, categoryId: categoryId) }
            $0.fulfill(.init(plan: plan, users: sorted))
        }
    }

    func logout() -> Promise<Void> {
        Promise {
            current = nil
            $0.fulfill(())
        }
    }
}
