//
//  CollectionSizingStrategy.swift
//  EldoSales
//
//  Created by Nikita Bondar on 19.06.2021.
//

import UIKit

protocol CollectionSizingStrategy: AnyObject {
    // MARK: - Interface

    func size(in parentSize: CGSize) -> CGSize
}
