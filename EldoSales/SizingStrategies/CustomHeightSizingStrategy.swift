//
//  CustomSizingStrategy.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class CustomHeightSizingStrategy: CollectionSizingStrategy {
    // MARK: - Properties

    private let height: CGFloat

    // MARK: - Init

    init(height: CGFloat) {
        self.height = height
    }

    // MARK: - Interface

    func size(in parentSize: CGSize) -> CGSize {
        CGSize(width: parentSize.width, height: height)
    }
}
