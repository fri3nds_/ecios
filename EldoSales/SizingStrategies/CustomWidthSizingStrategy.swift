//
//  CustomWidthSizingStrategy.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class CustomWidthSizingStrategy: CollectionSizingStrategy {
    // MARK: - Properties

    let width: CGFloat

    // MARK: - Init

    init(width: CGFloat) {
        self.width = width
    }

    // MARK: - Interface

    func size(in parentSize: CGSize) -> CGSize {
        CGSize(width: width, height: parentSize.height)
    }
}
