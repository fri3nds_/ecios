//
//  StatisticsDiagramItemStrategy.swift
//  EldoSales
//
//  Created by Nikita Bondar on 19.06.2021.
//

import UIKit

class DiagramItemStrategy: CollectionSizingStrategy {
    // MARK: - Interface

    func size(in parentSize: CGSize) -> CGSize {
        CGSize(width: 40, height: parentSize.height)
    }
}
