//
//  DiagramsItemStrategy.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class DiagramsItemStrategy: CollectionSizingStrategy {
    // MARK: - Interface

    func size(in parentSize: CGSize) -> CGSize {
        CGSize(width: parentSize.width, height: 270)
    }
}
