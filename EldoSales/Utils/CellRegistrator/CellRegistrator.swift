//
//  CellRegistrator.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class CellRegistrator {
    // MARK: - Properties

    private var registered: [String: AnyClass] = [:]

    // MARK: - Views

    private weak var collectionView: UICollectionView!

    // MARK: - Init

    init(collectionView: UICollectionView) {
        self.collectionView = collectionView
    }

    // MARK: - Interface

    func registerIfNeeded(_ cellClass: AnyClass, for reuseIdentifier: String) {
        guard registered[reuseIdentifier] == nil else { return }

        registered[reuseIdentifier] = cellClass
        collectionView.register(cellClass, forCellWithReuseIdentifier: reuseIdentifier)
    }
}
