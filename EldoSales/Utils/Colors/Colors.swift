//
//  Colors.swift
//  EldoSales
//
//  Created by Nikita Bondar on 14.06.2021.
//

import UIKit

protocol Colors: AnyObject {
    // MARK: - Properties. Custom

    var main: UIColor { get }
    var background: UIColor { get }
    var backgroundDimming: UIColor { get }
    var red: UIColor { get }

    var navigation: UIColor { get }

    var activityIndicator: UIColor { get }
    var textFieldBackground: UIColor { get }

    var badgeSelectedColor: UIColor { get }
    var badgeColor: UIColor { get }
    var badgeTitleSelectedColor: UIColor { get }
    var badgeTitleColor: UIColor { get }
    var badgeIconColor: UIColor { get }

    var diagramLineColor: UIColor { get }
    var diagramItemBackgroundColor: UIColor { get }
    var diagramItemEmptyColor: UIColor { get }

    var titleColor: UIColor { get }
    var mainTitleColor: UIColor { get }
    var subtitleColor: UIColor { get }

    var collectionBackground: UIColor { get }

    // MARK: - Properties. Design

    var elementGreen: UIColor { get }
    var elementBlue: UIColor { get }
    var elementLightBlue: UIColor { get }
    var elementLightGray: UIColor { get }

    var textBlue: UIColor { get }
    var textGreen: UIColor { get }
    var textBlack: UIColor { get }
    var textWhite: UIColor { get }
    var textGray: UIColor { get }
    var textLightGray: UIColor { get }
}
