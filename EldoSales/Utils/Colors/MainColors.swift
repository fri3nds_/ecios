//
//  MainColors.swift
//  EldoSales
//
//  Created by Nikita Bondar on 14.06.2021.
//

import UIKit

class MainColors: Colors {
    // MARK: - Properties. Custom

    var main: UIColor { UIColor(red: 98 / 255, green: 88 / 255, blue: 204 / 255, alpha: 1) }
    var background: UIColor { .white }
    var backgroundDimming: UIColor { UIColor(red: 0, green: 0, blue: 0, alpha: 0.4) }
    var red: UIColor { UIColor(red: 198 / 255, green: 19 / 255, blue: 52 / 255, alpha: 1) }

    var navigation: UIColor { UIColor(red: 28 / 255, green: 37 / 255, blue: 48 / 255, alpha: 1) }

    var activityIndicator: UIColor { .white }
    var textFieldBackground: UIColor { .white }

    var badgeSelectedColor: UIColor { UIColor(red: 245 / 255, green: 245 / 255, blue: 245 / 255, alpha: 1) }
    var badgeColor: UIColor { .white }
    var badgeTitleSelectedColor: UIColor { .black }
    var badgeTitleColor: UIColor { UIColor(red: 117 / 255, green: 123 / 255, blue: 122 / 255, alpha: 1) }
    var badgeIconColor: UIColor { UIColor(red: 211 / 255, green: 211 / 255, blue: 211 / 255, alpha: 1) }

    var diagramLineColor: UIColor { UIColor(red: 0, green: 0, blue: 0, alpha: 0.1) }
    var diagramItemBackgroundColor: UIColor { UIColor(red: 93 / 255, green: 176 / 255, blue: 117 / 255, alpha: 1) }
    var diagramItemEmptyColor: UIColor { UIColor(red: 232 / 255, green: 232 / 255, blue: 232 / 255, alpha: 1) }

    var titleColor: UIColor { .black }
    var mainTitleColor: UIColor { UIColor(red: 93 / 255, green: 176 / 255, blue: 117 / 255, alpha: 1) }
    var subtitleColor: UIColor { UIColor(red: 60 / 255, green: 60 / 255, blue: 67 / 255, alpha: 0.6) }

    var collectionBackground: UIColor { UIColor(red: 242 / 255, green: 242 / 255, blue: 242 / 255, alpha: 1) }

    // MARK: - Properties. Design

    var elementBlue: UIColor { UIColor(red: 28 / 255, green: 37 / 255, blue: 48 / 255, alpha: 1) }
    var elementLightBlue: UIColor { UIColor(red: 51 / 255, green: 59 / 255, blue: 69 / 255, alpha: 1) }
    var elementGreen: UIColor { UIColor(red: 47 / 255, green: 185 / 255, blue: 77 / 255, alpha: 1) }
    var elementLightGray: UIColor { UIColor(red: 245 / 255, green: 245 / 255, blue: 245 / 255, alpha: 1) }

    var textBlue: UIColor { UIColor(red: 28 / 255, green: 37 / 255, blue: 48 / 255, alpha: 1) }
    var textGreen: UIColor { UIColor(red: 47 / 255, green: 185 / 255, blue: 77 / 255, alpha: 1) }
    var textBlack: UIColor { UIColor(red: 0, green: 0, blue: 0, alpha: 1) }
    var textWhite: UIColor { UIColor(red: 1, green: 1, blue: 1, alpha: 1) }
    var textGray: UIColor { UIColor(red: 0, green: 0, blue: 0, alpha: 0.5) }
    var textLightGray: UIColor { UIColor(red: 205 / 255, green: 205 / 255, blue: 205 / 255, alpha: 0.8) }
}
