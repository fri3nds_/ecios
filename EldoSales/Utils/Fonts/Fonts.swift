//
//  Fonts.swift
//  EldoSales
//
//  Created by Nikita Bondar on 19.06.2021.
//

import UIKit

protocol Fonts: AnyObject {
    // MARK: - Properties

    var font10: UIFont { get }
    var font10Bold: UIFont { get }

    var font12: UIFont { get }
    var font12Bold: UIFont { get }

    var font13: UIFont { get }
    var font13Bold: UIFont { get }

    var font14: UIFont { get }
    var font14Bold: UIFont { get }

    var font15: UIFont { get }
    var font15Bold: UIFont { get }

    var font16: UIFont { get }
    var font16Bold: UIFont { get }

    var font17: UIFont { get }
    var font17Bold: UIFont { get }

    var font20: UIFont { get }
    var font20Bold: UIFont { get }

    var font24: UIFont { get }
    var font24Bold: UIFont { get }
}
