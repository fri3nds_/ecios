//
//  MainFonts.swift
//  EldoSales
//
//  Created by Nikita Bondar on 19.06.2021.
//

import UIKit

class MainFonts: Fonts {
    // MARK: - Properties

    var font10: UIFont { .systemFont(ofSize: 10) }
    var font10Bold: UIFont { .systemFont(ofSize: 10, weight: .semibold) }

    var font12: UIFont { .systemFont(ofSize: 12) }
    var font12Bold: UIFont { .systemFont(ofSize: 12, weight: .semibold) }

    var font13: UIFont { .systemFont(ofSize: 13) }
    var font13Bold: UIFont { .systemFont(ofSize: 13, weight: .semibold) }

    var font14: UIFont { .systemFont(ofSize: 14) }
    var font14Bold: UIFont { .systemFont(ofSize: 14, weight: .semibold) }

    var font15: UIFont { .systemFont(ofSize: 15) }
    var font15Bold: UIFont { .systemFont(ofSize: 15, weight: .semibold) }

    var font16: UIFont { .systemFont(ofSize: 16) }
    var font16Bold: UIFont { .systemFont(ofSize: 16, weight: .semibold) }

    var font17: UIFont { .systemFont(ofSize: 17) }
    var font17Bold: UIFont { .systemFont(ofSize: 17, weight: .semibold) }

    var font20: UIFont { .systemFont(ofSize: 20) }
    var font20Bold: UIFont { .systemFont(ofSize: 20, weight: .semibold) }

    var font24: UIFont { .systemFont(ofSize: 24) }
    var font24Bold: UIFont { .systemFont(ofSize: 24, weight: .semibold) }
}
