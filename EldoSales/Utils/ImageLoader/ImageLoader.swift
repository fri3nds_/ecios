//
//  ImageLoader.swift
//  EldoSales
//
//  Created by Nikita Bondar on 23.06.2021.
//

import PromiseKit

class ImageLoader {
    // MARK: - Properties

    let imageURL: URL

    // MARK: Init

    init(imageURL: URL) {
        self.imageURL = imageURL
    }

    // MARK: - Interface

    func load(_ category: ImageCategory) -> Promise<UIImage> {
        Promise {
            switch category {
            case .random:
                $0.fulfill(MockImages.shared.randomImages.randomElement()!)

            case .profile:
                $0.fulfill(MockImages.shared.profileImages.randomElement()!)

            case .product:
                $0.fulfill(MockImages.shared.productImages.randomElement()!)
            }
        }
    }
}
