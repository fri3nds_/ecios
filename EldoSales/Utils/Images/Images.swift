//
//  Images.swift
//  EldoSales
//
//  Created by Nikita Bondar on 18.06.2021.
//

import UIKit

protocol Images: AnyObject {
    // MARK: - Properties

    var chevronDown: UIImage { get }
    var chevronRight: UIImage { get }
    var circleAppIcon: UIImage { get }

    var circleIcon: UIImage { get }
    var columnIcon: UIImage { get }

    var checkmarkIcon: UIImage { get }

    var statisticsIcon: UIImage { get }
    var ratingIcon: UIImage { get }
    var planIcon: UIImage { get }
    var employeesIcon: UIImage { get }
    var profileIcon: UIImage { get }
}
