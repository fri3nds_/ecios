//
//  MainImages.swift
//  EldoSales
//
//  Created by Nikita Bondar on 19.06.2021.
//

import UIKit

class MainImages: Images {
    // MARK: - Properties

    var chevronDown: UIImage { UIImage(named: "chevronDown")! }
    var chevronRight: UIImage { UIImage(named: "chevronRight")! }
    var circleAppIcon: UIImage { UIImage(named: "iconEldoSales")! }

    var circleIcon: UIImage { UIImage(named: "circleIcon")! }
    var columnIcon: UIImage { UIImage(named: "columnIcon")! }

    var checkmarkIcon: UIImage { UIImage(named: "checkmark")! }

    var statisticsIcon: UIImage { UIImage(named: "statistics")! }
    var ratingIcon: UIImage { UIImage(named: "rating")! }
    var planIcon: UIImage { UIImage(named: "plan")! }
    var employeesIcon: UIImage { UIImage(named: "employees")! }
    var profileIcon: UIImage { UIImage(named: "profile")! }
}
