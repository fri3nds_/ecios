//
//  Queue.swift
//  EldoSales
//
//  Created by Nikita Bondar on 19.06.2021.
//

import Foundation

class Queue {
    // MARK: - Interface

    static func main(_ block: @escaping () -> Void) {
        DispatchQueue.main.async(execute: block)
    }

    static func main(after seconds: TimeInterval, block: @escaping () -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds, execute: block)
    }

    static func global(_ block: @escaping () -> Void) {
        DispatchQueue.global().async(execute: block)
    }
}
