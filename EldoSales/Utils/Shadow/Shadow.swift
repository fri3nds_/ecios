//
//  Shadow.swift
//  EldoSales
//
//  Created by Nikita Bondar on 21.06.2021.
//

import UIKit

struct Shadow {
    // MARK: - Properties

    let color: UIColor
    let radius: CGFloat
    let offset: CGSize
    let opacity: Float

    // MARK: - Init

    init(color: UIColor = .clear,
         radius: CGFloat = 0,
         offset: CGSize = .zero,
         opacity: Float = 0) {
        self.color = color
        self.radius = radius
        self.offset = offset
        self.opacity = opacity
    }
}
