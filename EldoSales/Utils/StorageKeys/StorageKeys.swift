//
//  StorageKeys.swift
//  EldoSales
//
//  Created by Nikita Bondar on 19.06.2021.
//

import Foundation

class StorageKeys {
    // MARK: - Properties

    static var isLoggedIn: String { "isLoggedIn" }
}
