//
//  Strings.swift
//  EldoSales
//
//  Created by Nikita Bondar on 19.06.2021.
//

import Foundation

class Strings {
    // MARK: - Properties

    static var authPlaceholder: String { "Логин" }
    static var passwordPlaceholder: String { "Пароль" }
    static var authButtonTitle: String { "Войти" }

    static var statisticsTitle: String { "Статистика" }
    static var ratingTitle: String { "Рейтинг" }
    static var planTitle: String { "План" }
    static var employeesTitle: String { "Сотрудники" }
    static var profileTitle: String { "Профиль" }
}
