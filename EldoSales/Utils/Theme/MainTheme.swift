//
//  MainTheme.swift
//  EldoSales
//
//  Created by Nikita Bondar on 19.06.2021.
//

import Foundation

class MainTheme: Theme {
    // MARK: - Properties

    let colors: Colors
    let images: Images
    let fonts: Fonts

    // MARK: - Init

    init(colors: Colors, images: Images, fonts: Fonts) {
        self.colors = colors
        self.images = images
        self.fonts = fonts
    }
}
