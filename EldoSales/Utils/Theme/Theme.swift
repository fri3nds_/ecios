//
//  Theme.swift
//  EldoSales
//
//  Created by Nikita Bondar on 19.06.2021.
//

import Foundation

protocol Theme: AnyObject {
    // MARK: - Properties

    var colors: Colors { get }
    var images: Images { get }
    var fonts: Fonts { get }
}
