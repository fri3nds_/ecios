//
//  CollectionViewController.swift
//  EldoSales
//
//  Created by Nikita Bondar on 21.06.2021.
//

import UIKit

protocol CollectionViewControllerDelegate: AnyObject {
    // MARK: - Interface

    func collectionViewController(_ collectionViewController: CollectionViewController, didTap item: CollectionItem)
}

class CollectionViewController: UIViewController, ContainerMovableSourceViewController {
    // MARK: - Children

    enum HeightMode: String {
        case `default`
        case byContent
    }

    // MARK: - Properties

    private(set) var items: [CollectionItem] = []

    private weak var cellDelegate: CollectionCellDelegate?
    private let cellRegistrator: CellRegistrator
    private let mode: HeightMode
    private let theme: Theme

    // MARK: - Views

    weak var containerMovableViewController: ContainerMovableViewController?
    weak var scrollView: UIScrollView? { collectionView }

    private let collectionView: UICollectionView

    // MARK: - Init

    init(cellDelegate: CollectionCellDelegate?, mode: HeightMode, theme: Theme) {
        self.cellDelegate = cellDelegate
        self.mode = mode
        self.theme = theme

        collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        cellRegistrator = CellRegistrator(collectionView: collectionView)

        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable, message: "Use init(moduleFactory:, theme:) instead")
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life cycle

    override func didMove(toParent parent: UIViewController?) {
        super.didMove(toParent: parent)

        containerMovableViewController?.delegate = self

        if mode == .byContent {
            containerMovableViewController?.dataSource = self
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        navigationTitle = Strings.employeesTitle
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        performLayout()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        coordinator.animate { [weak self] context in
            self?.collectionView.collectionViewLayout.invalidateLayout()
        }
    }

    // MARK: - Interface

    func reloadItems(_ items: [CollectionItem]) {
        self.items = items
        collectionView.reloadData()

        containerMovableViewController?.move(to: .top, animated: true)
    }

    func refillItems(_ items: [CollectionItem]) {
        for item in items {
            guard let index = self.items.firstIndex(of: item) else { continue }

            if let cell = collectionView.cellForItem(at: IndexPath(row: index, section: 0)) as? CollectionCell {
                cell.fill(with: item, animated: true)
            }
        }
    }

    func hide() {
        containerMovableViewController?.hide(animated: true)
    }

    // MARK: - Private. Setup

    private func setupViews() {
        view.backgroundColor = theme.colors.background

        tabBarItem.title = Strings.employeesTitle
        tabBarItem.image = theme.images.employeesIcon

        view.addSubview(collectionView)

        if let flowCollectionLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowCollectionLayout.minimumInteritemSpacing = 0
            flowCollectionLayout.minimumLineSpacing = 0
            flowCollectionLayout.sectionInset = .zero
            flowCollectionLayout.scrollDirection = .vertical
        }

        collectionView.backgroundColor = theme.colors.background
        collectionView.alwaysBounceVertical = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.delegate = self
        collectionView.dataSource = self
    }

    // MARK: - Private. Layout

    private func performLayout() {
        collectionView.pin.all()
    }
}

extension CollectionViewController: UICollectionViewDelegate {

}

extension CollectionViewController: UICollectionViewDelegateFlowLayout {
    // MARK: - Interface

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        items[indexPath.row].size(in: collectionView.frame.size)
    }
}

extension CollectionViewController: UICollectionViewDataSource {
    // MARK: - Interface

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        items.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = items[indexPath.row]
        cellRegistrator.registerIfNeeded(item.cellClass, for: item.reuseIdentifier)

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: item.reuseIdentifier, for: indexPath) as? CollectionCell
        cell?.delegate = cellDelegate
        cell?.setup(with: theme)
        cell?.fill(with: item, animated: true)

        return cell ?? UICollectionViewCell()
    }
}

extension CollectionViewController: ContainerMovableViewControllerDelegate {
    // MARK: - Interface

    func containerMovableViewController(_ containerMovableViewController: ContainerMovableViewController,
                                        willStartAnimationTo position: ContainerMoverPosition, with coordinator: IContainerMoverCoordinator) {
        if position == .hidden {
            view.endEditing(false)
        }
    }

    func containerMovableViewController(_ containerMovableViewController: ContainerMovableViewController, didEndAnimationTo position: ContainerMoverPosition) {

    }

    func containerMovableViewController(_ containerMovableViewController: ContainerMovableViewController, didInteractiveAnimateTo height: CGFloat) {
        
    }
}

extension CollectionViewController: ContainerMovableViewControllerDataSource {
    // MARK: - Interface

    func containerMovableViewController(_ containerMovableViewController: ContainerMovableViewController, heightFor position: ContainerMoverPosition) -> CGFloat {
        switch position {
        case .top:
            return max(0, min(collectionView.contentSize.height + containerMovableViewController.view.pin.safeArea.bottom,
                              containerMovableViewController.view.frame.height - containerMovableViewController.view.pin.safeArea.top - 10))

        default:
            return 0
        }
    }
}
