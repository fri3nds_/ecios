//
//  ContainerMovableViewController.swift
//  EldoSales
//
//  Created by Bondar Nikita on 21.06.2021.
//

import UIKit

/**
 *  Протокол для внутреннего контроллера, у которого есть свой контент
 */
protocol ContainerMovableSourceViewController: ContainerMovableContentViewController {
    // MARK: - Properties

    /**
     *  Слабая ссылка на двигательный контроллер
     */
    var containerMovableViewController: ContainerMovableViewController? { get set }
}

protocol ContainerMovableViewControllerDelegate: AnyObject {
    // MARK: - Interface

    /**
     *  Метод для оповещения делагата о начале анимации контейнера
     *
     *  - Parameters:
     *      - containerMovableViewController: Контроллер, который отвечает за передвижение контейнера
     *      - position: Конечная позиция контейнера после анимации
     *      - coordinator: Объект для применения кастомной анимации вместе с передвижением контейнера
     *
     *  Пример использования coordinator.
     *  Задача: изменить button.alpha при движении контейнера к .hidden
     *  ~~~
     *  // внутри делегата
     *  coordinator.animate(alongside: { context in
     *      self.button.alpha = position == .hidden ? 0 : 1
     *  }) { context, finished in
     *      self.fetchSomeDataAfterAnimation()
     *  }
     *  ~~~
     */
    func containerMovableViewController(_ containerMovableViewController: ContainerMovableViewController,
                                        willStartAnimationTo position: ContainerMoverPosition,
                                        with coordinator: IContainerMoverCoordinator)

    /**
     *  Метод для оповещения делегата о конце анимации контейнера к позиции
     *
     *  - Parameters:
     *      - containerMovableViewController: Контроллер, который отвечает за передвижение контейнера
     *      - position: Конечная позиция контейнера после анимации
     */
    func containerMovableViewController(_ containerMovableViewController: ContainerMovableViewController,
                                        didEndAnimationTo position: ContainerMoverPosition)

    /**
     *  Метод для оповещения делегата о интерактивном перемещении контейнера на высоту
     *
     *  - Parameters:
     *      - containerMovableViewController: Контроллер, который отвечает за передвижение контейнера
     *      - height: Текущая высота контейнера
     *
     *  Пример использования интерактивной анимации
     *  Задача: менять button.alpha интерактивно при движении контейнера от .medium к .hidden
     *  ~~~
     *  // внутри делегата
     *  let mediumPositionHeight = containerMovableView.height(for: .medium)
     *  let hiddenPositionHeight = containerMovableView.height(for: .hidden)
     *
     *  if mediumPositionHeight >= currentHeight {
     *      let fullDistance = mediumPositionHeight - hiddenPositionHeight
     *      let currentDistance = currentHeight - hiddenPositionHeight
     *      button.alpha = min(1, max(0, currentDistance / fullDistance))
     *  } else {
     *      button.alpha = 1
     *  }
     *  ~~~
     */
    func containerMovableViewController(_ containerMovableViewController: ContainerMovableViewController,
                                        didInteractiveAnimateTo height: CGFloat)
}

protocol ContainerMovableViewControllerDataSource: AnyObject {
    // MARK: - Interface

    /**
     *  Данные для кастомного определения высот заданным позициям
     *
     *  - Parameters:
     *      - containerMovableViewController: Контроллер, который отвечает за передвижение контейнера
     *      - position: Позиция контейнера
     *
     *  - Returns: Высота соответствующая position
     */
    func containerMovableViewController(_ containerMovableViewController: ContainerMovableViewController,
                                        heightFor position: ContainerMoverPosition) -> CGFloat
}

class ContainerMovableViewController: UIViewController {
    // MARK: - Delegate

    weak var delegate: ContainerMovableViewControllerDelegate?

    // MARK: - Data source

    weak var dataSource: ContainerMovableViewControllerDataSource? {
        didSet {
            containerView.dataSource = dataSource == nil ? nil : self
        }
    }

    // MARK: - Properties

    var allowedPositions: [ContainerMoverPosition] {
        get { containerView.allowedPositions }
        set { containerView.allowedPositions = newValue }
    }

    var currentPosition: ContainerMoverPosition { containerView.currentPosition }
    var startPosition: ContainerMoverPosition = .medium

    var cornerRadius: CGFloat {
        get { containerView.cornerRadius }
        set { containerView.cornerRadius = newValue }
    }

    var corners: UIRectCorner {
        get { containerView.corners }
        set { containerView.corners = newValue }
    }

    var dismissOnTap: Bool = true
    var isAlphaEnabled: Bool = false

    var movableBackgroundColor: UIColor? {
        get { containerView.movableBackgroundColor }
        set { containerView.movableBackgroundColor = newValue }
    }

    var shadow: Shadow {
        get { containerView.shadow }
        set { containerView.shadow = newValue }
    }

    private var isInitialAnimation: Bool = true

    private let theme: Theme

    // MARK: - Views

    private weak var containerView: ContainerMovableView!
    private weak var tapGestureRecognizer: UITapGestureRecognizer!

    private weak var sourceViewController: ContainerMovableSourceViewController?

    // MARK: - Init

    init(theme: Theme) {
        self.theme = theme
        super.init(nibName: nil, bundle: nil)

        modalPresentationStyle = .overFullScreen
        loadViewIfNeeded()
    }

    @available(*, unavailable, message: "Uset init(theme:) instead")
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if isInitialAnimation {
            isInitialAnimation.toggle()
            animateBackgroundColorIfNeeded(true)
        }
        containerView.move(to: startPosition, animated: true)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        performLayout()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        coordinator.animate { [weak self] _ in
            self?.containerView.willTransition()
        }
    }

    // MARK: - Interface

    func set(sourceViewController: ContainerMovableSourceViewController) {
        if let oldSourceViewController = self.sourceViewController {
            oldSourceViewController.containerMovableViewController = nil

            oldSourceViewController.willMove(toParent: nil)
            oldSourceViewController.view.removeFromSuperview()
            oldSourceViewController.removeFromParent()
        }

        containerView.setContentViewController(sourceViewController)
        sourceViewController.containerMovableViewController = self

        addChild(sourceViewController)
        sourceViewController.didMove(toParent: self)

        self.sourceViewController = sourceViewController

        view.setNeedsLayout()
    }

    func move(to position: ContainerMoverPosition, animated: Bool, completion: (() -> Void)? = nil) {
        switch position {
        case .hidden:
            hide(animated: animated, completion: completion)

        default:
            containerView.move(to: position, animated: animated, completion: completion)
        }
    }

    func hide(animated: Bool, completion: (() -> Void)? = nil) {
        if !allowedPositions.contains(.hidden) {
            allowedPositions = allowedPositions + [.hidden]
        }

        containerView.move(to: .hidden, animated: animated, completion: completion)

        if !animated {
            dismiss(animated: false, completion: nil)
        }
    }

    func height(for position: ContainerMoverPosition) -> CGFloat {
        containerView.height(for: position)
    }

    // MARK: - Private. Setup

    private func setup() {
        view.backgroundColor = .clear

        let containerView = ContainerMovableView(theme: theme)
        containerView.delegate = self
        view.addSubview(containerView)
        self.containerView = containerView

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapGestureRecognizerAction(_:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        tapGestureRecognizer.numberOfTouchesRequired = 1
        view.addGestureRecognizer(tapGestureRecognizer)
        self.tapGestureRecognizer = tapGestureRecognizer
    }

    // MARK: - Private. Layout

    private func performLayout() {
        containerView.pin.all()
    }

    // MARK: - Private. Actions

    @objc
    private func tapGestureRecognizerAction(_ sender: UITapGestureRecognizer) {
        guard dismissOnTap else { return }
        guard containerView.hitTest(sender.location(in: view), with: nil) == nil else { return }

        hide(animated: true)
    }

    // MARK: - Private. Help

    private func animateBackgroundColorIfNeeded(_ show: Bool) {
        guard isAlphaEnabled else { return }

        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let self = self else { return }
            self.view.backgroundColor = show ? self.theme.colors.backgroundDimming : .clear
        }
    }
}

extension ContainerMovableViewController: ContainerMovableViewDelegate {
    func containerMovableView(_ containerMovableView: ContainerMovableView,
                                     willStartAnimationTo position: ContainerMoverPosition,
                                     with coordinator: IContainerMoverCoordinator) {
        delegate?.containerMovableViewController(self, willStartAnimationTo: position, with: coordinator)

        if position == .hidden {
            animateBackgroundColorIfNeeded(false)
        }
    }

    func containerMovableView(_ containerMovableView: ContainerMovableView, didEndAnimationTo position: ContainerMoverPosition) {
        delegate?.containerMovableViewController(self, didEndAnimationTo: position)

        if position == .hidden {
            dismiss(animated: false, completion: nil)
        }
    }

    func containerMovableView(_ containerMovableView: ContainerMovableView, didInteractiveAnimateTo height: CGFloat) {
        delegate?.containerMovableViewController(self, didInteractiveAnimateTo: height)
    }
}

extension ContainerMovableViewController: ContainerMovableViewDataSource {
    func containerMovableView(_ containerMovableView: ContainerMovableView,
                                     heightFor position: ContainerMoverPosition) -> CGFloat {
        guard let dataSource = dataSource else { assertionFailure("data source is required"); return .zero }
        return dataSource.containerMovableViewController(self, heightFor: position)
    }
}
