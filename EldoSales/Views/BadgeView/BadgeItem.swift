//
//  BadgeItem.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

class BadgeItem {
    // MARK: - Properties

    let identifier: String
    let title: String
    let image: UIImage?
    var isSelected: Bool

    let model: Any?

    // MARK: - Init

    init(identifier: String,
         title: String,
         image: UIImage?,
         isSelected: Bool,
         model: Any?) {
        self.identifier = identifier
        self.title = title
        self.image = image
        self.isSelected = isSelected
        self.model = model
    }
}
