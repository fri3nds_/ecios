//
//  BadgeView.swift
//  EldoSales
//
//  Created by Nikita Bondar on 20.06.2021.
//

import UIKit

protocol BadgeViewDelegate: AnyObject {
    // MARK: - Interface

    func badgeView(_ badgeView: BadgeView, didTapWith item: BadgeItem)
}

class BadgeView: UIView {
    // MARK: - Delegate

    weak var delegate: BadgeViewDelegate?

    // MARK: - Properties

    private let theme: Theme

    private var item: BadgeItem?

    // MARK: - Views

    private let titleLabel = UILabel()
    private let imageView = UIImageView()
    private let button = UIButton()

    // MARK: - Init

    init(theme: Theme) {
        self.theme = theme

        super.init(frame: .zero)

        setup()
    }

    @available(*, unavailable, message: "Use init(theme:) instead")
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life cycle

    override func layoutSubviews() {
        super.layoutSubviews()

        performLayout()
    }

    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let imageSpace = imageView.isHidden ? 0 : .imageMargins.left + CGSize.imageSize.width + .imageMargins.right
        return CGSize(width: .titleMargins.left + titleLabel.intrinsicContentSize.width + .titleMargins.right + imageSpace, height: 35)
    }

    // MARK: - Interface

    func configure(_ item: BadgeItem) {
        self.item = item

        titleLabel.text = item.title
        titleLabel.textColor = item.isSelected ? theme.colors.badgeTitleSelectedColor : theme.colors.badgeTitleColor

        imageView.image = item.image?.withRenderingMode(.alwaysTemplate)
        imageView.isHidden = item.image == nil

        backgroundColor = item.isSelected ? theme.colors.badgeSelectedColor : theme.colors.badgeColor

        setNeedsLayout()
    }

    // MARK: - Private. Setup

    private func setup() {
        backgroundColor = theme.colors.badgeColor
        clipsToBounds = true
        layer.cornerRadius = 8

        addSubview(titleLabel)
        addSubview(imageView)
        addSubview(button)

        titleLabel.font = theme.fonts.font14Bold
        titleLabel.textColor = theme.colors.badgeTitleColor

        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = theme.colors.badgeIconColor

        button.addTarget(self, action: #selector(buttonDidTap(_:)), for: .touchUpInside)
    }

    // MARK: - Private. Layout

    private func performLayout() {
        titleLabel.pin
            .vertically()
            .left()
            .marginLeft(.titleMargins.left)
            .sizeToFit(.height)

        imageView.pin
            .vCenter()
            .left(to: titleLabel.edge.right)
            .marginLeft(.titleMargins.right + .imageMargins.left)
            .size(.imageSize)

        button.pin.all()
    }

    // MARK: - Private. Actions

    @objc
    private func buttonDidTap(_ sender: UIButton) {
        guard let item = item else { return }
        delegate?.badgeView(self, didTapWith: item)
    }
}

private extension CGFloat {
    static let titleMargins = UIEdgeInsets(top: 0, left: 13, bottom: 0, right: 13)
    static let imageMargins = UIEdgeInsets(top: 0, left: -3, bottom: 0, right: 13)
}

private extension CGSize {
    static let imageSize = CGSize(width: 15, height: 15)
}
