//
//  ContainerMovableView.swift
//  EldoSales
//
//  Created by Bondar Nikita on 21.06.2021.
//

import UIKit

/**
 *  Протокол для контроллера контента
 */
protocol ContainerMovableContentViewController: UIViewController {
    // MARK: - Properties

    /**
     *  Указание scrollView не обязательно, можно вернуть nil
     *  В случае, когда вы хотите, чтобы жест скролл вью тоже двигал контейнер, вы указываете необходимый элемент
     */
    var scrollView: UIScrollView? { get }
}

/**
 *  Протокол для вьюхи контента
 */
protocol ContainerMovableContentView: UIView {
    // MARK: - Properties

    /**
     *  Указание scrollView не обязательно, можно вернуть nil
     *  В случае, когда вы хотите, чтобы жест скролл вью тоже двигал контейнер, вы указываете необходимый элемент
     */
    var scrollView: UIScrollView? { get }
}

protocol ContainerMovableViewDelegate: AnyObject {
    // MARK: - Interface

    /**
     *  Метод для оповещения делагата о начале анимации.
     *
     *  - Parameters:
     *      - containerMovableView: Вьюха, которая отвечает за двигательные действия
     *      - position: Конечная позиция контейнера после анимации
     *      - coordinator: Объект для применения кастомной анимации вместе с передвижением контейнера
     *
     *  Пример использования coordinator.
     *  Задача: изменить button.alpha при движении контейнера к .hidden
     *  ~~~
     *  // внутри делегата
     *  coordinator.animate(alongside: { context in
     *      self.button.alpha = position == .hidden ? 0 : 1
     *  }) { context, finished in
     *      self.fetchSomeDataAfterAnimation()
     *  }
     *  ~~~
     */
    func containerMovableView(_ containerMovableView: ContainerMovableView,
                              willStartAnimationTo position: ContainerMoverPosition, with coordinator: IContainerMoverCoordinator)

    /**
     *  Метод для оповещения делегата о конце анимации к позиции
     *
     *  - Parameters:
     *      - containerMovableView: Вьюха, которая отвечает за двигательные действия
     *      - position: Конечная позиция контейнера после анимации
     */
    func containerMovableView(_ containerMovableView: ContainerMovableView, didEndAnimationTo position: ContainerMoverPosition)

    /**
     *  Метод для оповещения делегата о интерактивном перемещении контейнера на высоту
     *
     *  - Parameters:
     *      - containerMovableView: Вьюха, которая отвечает за двигательные действия
     *      - height: Текущая высота контейнера
     *
     *  Пример использования интерактивной анимации.
     *  Задача: менять button.alpha интерактивно при движении контейнера от .medium к .hidden
     *  ~~~
     *  // внутри делегата
     *  let mediumPositionHeight = containerMovableView.height(for: .medium)
     *  let hiddenPositionHeight = containerMovableView.height(for: .hidden)
     *
     *  if mediumPositionHeight >= currentHeight {
     *      let fullDistance = mediumPositionHeight - hiddenPositionHeight
     *      let currentDistance = currentHeight - hiddenPositionHeight
     *      button.alpha = min(1, max(0, currentDistance / fullDistance))
     *  } else {
     *      button.alpha = 1
     *  }
     *  ~~~
     */
    func containerMovableView(_ containerMovableView: ContainerMovableView, didInteractiveAnimateTo height: CGFloat)
}

protocol ContainerMovableViewDataSource: AnyObject {
    // MARK: - Interface

    /**
     *  Данные для кастомного определения высот заданным позициям
     *
     *  - Parameters:
     *      - containerMovableView: Вьюха, которая отвечает за двигательные действия
     *      - position: Позиция контейнера
     *
     *  - Returns: Высота соответствующая position
     */
    func containerMovableView(_ containerMovableView: ContainerMovableView, heightFor position: ContainerMoverPosition) -> CGFloat
}

class ContainerMovableView: UIView {
    // MARK: - Delegate

    weak var delegate: ContainerMovableViewDelegate?

    // MARK: - Data source

    weak var dataSource: ContainerMovableViewDataSource?

    // MARK: - Properties

    var allowedPositions: [ContainerMoverPosition] {
        get { containerMover.allowedPositions }
        set { containerMover.allowedPositions = newValue }
    }

    var currentPosition: ContainerMoverPosition { containerMover.currentPosition }

    var cornerRadius: CGFloat = 20 { didSet { updateContainerViewCorners() } }
    var corners: UIRectCorner = [.topLeft, .topRight] { didSet { updateContainerViewCorners() } }

    var movableBackgroundColor: UIColor? {
        get { containerView.backgroundColor }
        set { containerView.backgroundColor = newValue; containerShadowView.backgroundColor = newValue }
    }

    var shadow: Shadow {
        get { _shadow }
        set { _shadow = newValue; containerShadowView.apply(shadow: newValue) }
    }
    private var _shadow: Shadow = Shadow()

    private let containerMover: IContainerMover = ContainerMover(animationDuration: 0.5, dampingRatio: 0.8)
    private let theme: Theme

    private var additionalSpaceToPreventDropAnimation: CGFloat { bounds.height / 5 }

    // MARK: - Views. Internal

    private(set) weak var panGestureRecognizer: UIPanGestureRecognizer!
    private weak var additionalScrollPanGestureRecognizer: UIPanGestureRecognizer?

    private weak var containerShadowView: UIView!
    private weak var containerView: UIView!

    // MARK: - Views. Content

    private weak var contentViewController: ContainerMovableContentViewController?
    private weak var contentView: ContainerMovableContentView?

    private weak var _contentView: UIView? { contentViewController?.view ?? contentView }
    private weak var _contentScrollView: UIScrollView? { contentViewController?.scrollView ?? contentView?.scrollView }

    // MARK: - Init

    init(theme: Theme) {
        self.theme = theme
        super.init(frame: .zero)

        setupUI()
        setupMover()
    }

    @available(*, unavailable, message: "Use init(theme:) instead")
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life cycle

    override func layoutSubviews() {
        super.layoutSubviews()

        performLayout()
        updateContainerViewCorners()
    }

    override func sizeThatFits(_ size: CGSize) -> CGSize {
        autoSizeThatFits(size, layoutClosure: performLayout)
    }

    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let view = super.hitTest(point, with: event)
        if view === self {
            return nil
        }
        return view
    }

    func willTransition() {
        containerMover.willTransition()
    }

    // MARK: - Interface

    func move(to position: ContainerMoverPosition, animated: Bool, completion: (() -> Void)? = nil) {
        containerMover.move(to: position, animated: animated, completion: completion)
    }

    func setContentViewController(_ contentViewController: ContainerMovableContentViewController) {
        _contentView?.removeFromSuperview()

        self.contentViewController = contentViewController

        setupContent()
        setNeedsLayout()
    }

    func setContentView(_ contentView: ContainerMovableContentView) {
        _contentView?.removeFromSuperview()

        self.contentView = contentView

        setupContent()
        setNeedsLayout()
    }

    func height(for position: ContainerMoverPosition) -> CGFloat {
        if let dataSource = dataSource {
            return dataSource.containerMovableView(self, heightFor: position)
        }

        switch position {
        case .top:
            return bounds.height - pin.safeArea.top - 10

        case .medium:
            return bounds.height / 2 + pin.safeArea.bottom

        case .bottom:
            return 50 + pin.safeArea.bottom

        case .hidden:
            return 0
        }
    }

    // MARK: - Private. Setup

    private func setupUI() {
        backgroundColor = .clear

        let containerShadowView = UIView()
        containerShadowView.apply(shadow: shadow)
        containerShadowView.backgroundColor = .clear
        addSubview(containerShadowView)
        self.containerShadowView = containerShadowView

        let containerView = UIView()
        containerView.backgroundColor = .clear
        addSubview(containerView)
        self.containerView = containerView

        let panGestureRecognizer = UIPanGestureRecognizer()
        panGestureRecognizer.minimumNumberOfTouches = 1
        panGestureRecognizer.maximumNumberOfTouches = 1
        containerView.addGestureRecognizer(panGestureRecognizer)
        self.panGestureRecognizer = panGestureRecognizer

        updateContainerViewCorners()
    }

    private func setupMover() {
        containerMover.setup(panGestureRecognizer: panGestureRecognizer, scrollPanGestureRecognizer: nil)
        containerMover.delegate = self
        containerMover.dataSource = self
    }

    private func setupContent() {
        if let additionalScrollPanGestureRecognizer = additionalScrollPanGestureRecognizer {
            additionalScrollPanGestureRecognizer.view?.removeGestureRecognizer(additionalScrollPanGestureRecognizer)
        }

        guard let contentView = _contentView else { return }
        containerView.addSubview(contentView)

        if let scrollView = _contentScrollView {
            let additionalScrollPanGestureRecognizer = UIPanGestureRecognizer()
            additionalScrollPanGestureRecognizer.delegate = self
            scrollView.addGestureRecognizer(additionalScrollPanGestureRecognizer)
            self.additionalScrollPanGestureRecognizer = additionalScrollPanGestureRecognizer
        }

        containerMover.setup(panGestureRecognizer: panGestureRecognizer,
                             scrollPanGestureRecognizer: additionalScrollPanGestureRecognizer)
    }

    // MARK: - Private. Layout

    private func performLayout() {
        containerView.pin
            .horizontally()
            .bottom()
            .marginLeft(pin.safeArea.left)
            .marginRight(pin.safeArea.right)
            .marginBottom(-additionalSpaceToPreventDropAnimation)
            .height(containerMover.currentHeight + additionalSpaceToPreventDropAnimation)

        containerShadowView.pin
            .vCenter(to: containerView.edge.vCenter)
            .hCenter(to: containerView.edge.hCenter)
            .width(containerView.bounds.width)
            .height(containerView.bounds.height)

        if let contentView = _contentView {
            contentView.pin
                .all()
                .marginBottom(additionalSpaceToPreventDropAnimation)
        }
    }

    // MARK: - Private. Help

    private func updateContainerViewCorners() {
        containerShadowView.layer.cornerRadius = cornerRadius
        containerView.roundCorners(corners, radius: cornerRadius)
    }

    private func changeAlphaOfShadowViewIfNeeded(_ nextPosition: ContainerMoverPosition) {
        let needToHideShadow = nextPosition == .hidden
        guard needToHideShadow && containerShadowView.alpha == 1 || !needToHideShadow && containerShadowView.alpha == 0 else { return }

        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut) { [weak self] in
            self?.containerShadowView.alpha = needToHideShadow ? 0 : 1
        }
    }
}

extension ContainerMovableView: IContainerMoverDelegate {
    func containerMover(_ containerMover: IContainerMover,
                               willStartAnimationTo position: ContainerMoverPosition, with coordinator: IContainerMoverCoordinator) {
        changeAlphaOfShadowViewIfNeeded(position)
        delegate?.containerMovableView(self, willStartAnimationTo: position, with: coordinator)
    }

    func containerMoverNeedToLayout(_ containerMover: IContainerMover) {
        setNeedsLayout()
        layoutIfNeeded()
    }

    func containerMover(_ containerMover: IContainerMover, didEndAnimationTo position: ContainerMoverPosition) {
        delegate?.containerMovableView(self, didEndAnimationTo: position)
    }

    func containerMover(_ containerMover: IContainerMover, didStartInteractionOn height: CGFloat) {
        guard let scrollView = _contentScrollView else { return }
        guard scrollView.contentOffset.y != 0 else { return }

        scrollView.setContentOffset(CGPoint(x: scrollView.contentOffset.x, y: 0), animated: true)
    }

    func containerMover(_ containerMover: IContainerMover, didInteractiveAnimateTo height: CGFloat) {
        changeAlphaOfShadowViewIfNeeded(.top) // any position besides hidden
        delegate?.containerMovableView(self, didInteractiveAnimateTo: height)
    }
}

extension ContainerMovableView: IContainerMoverDataSource {
    func containerMover(_ containerMover: IContainerMover, heightFor position: ContainerMoverPosition) -> CGFloat {
        height(for: position)
    }
}

extension ContainerMovableView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
                                  shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        otherGestureRecognizer.view is UIScrollView && otherGestureRecognizer is UIPanGestureRecognizer
    }
}
