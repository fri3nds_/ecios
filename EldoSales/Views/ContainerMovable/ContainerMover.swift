//
//  ContainerMover.swift
//  EldoSales
//
//  Created by Bondar Nikita on 21.06.2021.
//

import UIKit

enum ContainerMoverPosition: Int {
    // MARK: - Cases

    case top = 0
    case medium
    case bottom
    case hidden
}

protocol IContainerMover: AnyObject {
    // MARK: - Delegate

    var delegate: IContainerMoverDelegate? { get set }

    // MARK: - Data source

    var dataSource: IContainerMoverDataSource? { get set }

    // MARK: - Properties

    var animationDuration: TimeInterval { get }
    var dampingRatio: CGFloat { get }

    var allowedPositions: [ContainerMoverPosition] { get set }
    var currentPosition: ContainerMoverPosition { get }
    var currentHeight: CGFloat { get }

    // MARK: - Interface

    func setup(panGestureRecognizer: UIPanGestureRecognizer, scrollPanGestureRecognizer: UIPanGestureRecognizer?)
    func move(to position: ContainerMoverPosition, animated: Bool, completion: (() -> Void)?)

    func willTransition()
}

protocol IContainerMoverDelegate: AnyObject {
    // MARK: - Interface

    func containerMover(_ containerMover: IContainerMover, willStartAnimationTo position: ContainerMoverPosition, with coordinator: IContainerMoverCoordinator)
    func containerMoverNeedToLayout(_ containerMover: IContainerMover)
    func containerMover(_ containerMover: IContainerMover, didEndAnimationTo position: ContainerMoverPosition)

    func containerMover(_ containerMover: IContainerMover, didStartInteractionOn height: CGFloat)
    func containerMover(_ containerMover: IContainerMover, didInteractiveAnimateTo height: CGFloat)
}

protocol IContainerMoverDataSource: AnyObject {
    // MARK: - Interface

    func containerMover(_ containerMover: IContainerMover, heightFor position: ContainerMoverPosition) -> CGFloat
}

class ContainerMover: IContainerMover {
    // MARK: - Properties

    weak var delegate: IContainerMoverDelegate?

    // MARK: - Data source

    weak var dataSource: IContainerMoverDataSource?

    // MARK: - Properties

    let animationDuration: TimeInterval
    let dampingRatio: CGFloat

    var allowedPositions: [ContainerMoverPosition] = [.top, .medium, .bottom]
    private(set) var currentPosition: ContainerMoverPosition = .hidden
    private(set) var currentHeight: CGFloat = 0

    private var startHeight: CGFloat = 0
    private var koefToMoveNextPosition: CGFloat = 0.25

    private var isNewScrollViewSession: Bool = false

    // MARK: - Gestures

    private weak var panGestureRecognizer: UIPanGestureRecognizer?
    private weak var scrollPanGestureRecognizer: UIPanGestureRecognizer?

    // MARK: - Init

    init(animationDuration: TimeInterval, dampingRatio: CGFloat) {
        self.animationDuration = animationDuration
        self.dampingRatio = dampingRatio
    }

    // MARK: - Life cycle

    func setup(panGestureRecognizer: UIPanGestureRecognizer, scrollPanGestureRecognizer: UIPanGestureRecognizer?) {
        self.panGestureRecognizer?.removeTarget(self, action: #selector(panGestureRecognizerAction(_:)))
        self.scrollPanGestureRecognizer?.removeTarget(self, action: #selector(scrollPanGestureRecognizerAction(_:)))

        self.panGestureRecognizer = panGestureRecognizer
        self.scrollPanGestureRecognizer = scrollPanGestureRecognizer

        panGestureRecognizer.addTarget(self, action: #selector(panGestureRecognizerAction(_:)))
        scrollPanGestureRecognizer?.addTarget(self, action: #selector(scrollPanGestureRecognizerAction(_:)))
    }

    // MARK: - Interface

    func move(to position: ContainerMoverPosition, animated: Bool, completion: (() -> Void)?) {
        guard allowedPositions.contains(position) else { assertionFailure("position is not allowed"); return }
        guard let dataSource = dataSource else { assertionFailure("data source is required"); return }

        currentPosition = position
        panGestureRecognizer?.cancel()
        scrollPanGestureRecognizer?.cancel()

        currentHeight = dataSource.containerMover(self, heightFor: position)

        if animated {
            let coordinator = ContainerMoverCoordinator()
            let coordinatorContext = ContainerMoverCoordinatorContext(animationDuration: animationDuration)
            delegate?.containerMover(self, willStartAnimationTo: position, with: coordinator)

            UIView.animate(
                withDuration: animationDuration,
                delay: 0,
                usingSpringWithDamping: dampingRatio,
                initialSpringVelocity: 0,
                options: .allowUserInteraction,
                animations: { [weak self] in
                    guard let self = self else { return }

                    self.delegate?.containerMoverNeedToLayout(self)
                    coordinator.animation?(coordinatorContext)
                },
                completion: { [weak self] finished in
                    guard let self = self else { return }

                    coordinator.completion?(coordinatorContext, finished)
                    coordinator.clear()

                    completion?()
                    self.delegate?.containerMover(self, didEndAnimationTo: position)
                }
            )
        } else {
            delegate?.containerMoverNeedToLayout(self)
            completion?()
        }
    }

    func willTransition() {
        panGestureRecognizer?.cancel()
        scrollPanGestureRecognizer?.cancel()

        move(to: currentPosition, animated: true, completion: nil)
    }

    // MARK: - Private. Actions

    @objc
    private func panGestureRecognizerAction(_ sender: UIPanGestureRecognizer) {
        guard let dataSource = dataSource else { assertionFailure("data source is required"); return }
        guard !allowedPositions.isEmpty else { assertionFailure("allowed positions is empty"); return }

        let translation = sender.translation(in: sender.view)
        let sortedAllowedPositions = allowedPositions.sorted(by: { $0.rawValue < $1.rawValue })

        let maxHeight = dataSource.containerMover(self, heightFor: sortedAllowedPositions.first!)
        let minHeight = dataSource.containerMover(self, heightFor: sortedAllowedPositions.last!)

        switch sender.state {
        case .began:
            startHeight = currentHeight
            currentHeight = bouncyHeightIfNeeded(startHeight - translation.y, minHeight: minHeight, maxHeight: maxHeight)

            delegate?.containerMover(self, didStartInteractionOn: startHeight)
            delegate?.containerMoverNeedToLayout(self)
            delegate?.containerMover(self, didInteractiveAnimateTo: currentHeight)

        case .changed:
            currentHeight = bouncyHeightIfNeeded(startHeight - translation.y, minHeight: minHeight, maxHeight: maxHeight)

            delegate?.containerMoverNeedToLayout(self)
            delegate?.containerMover(self, didInteractiveAnimateTo: currentHeight)

        case .cancelled,
             .failed,
             .ended:
            currentPosition = nearestPosition(to: currentHeight)
            move(to: currentPosition, animated: true, completion: nil)

        default:
            break
        }
    }

    @objc
    private func scrollPanGestureRecognizerAction(_ sender: UIPanGestureRecognizer) {
        guard let scrollView = sender.view as? UIScrollView else { return }
        guard let dataSource = dataSource else { assertionFailure("data source is required"); return }
        guard !allowedPositions.isEmpty else { assertionFailure("allowed positions is empty"); return }

        if sender.state == .began {
            isNewScrollViewSession = true
        }

        let sortedAllowedPositions = allowedPositions.sorted { $0.rawValue < $1.rawValue }
        let highestPosition = sortedAllowedPositions.first!
        let highestPositionHeight = dataSource.containerMover(self, heightFor: highestPosition)

        if highestPositionHeight < currentHeight {
            currentHeight = highestPositionHeight
            delegate?.containerMoverNeedToLayout(self)
        } else if highestPositionHeight > currentHeight {
            scrollView.setContentOffset(CGPoint(x: scrollView.contentOffset.x, y: 0), animated: false)
        }

        if scrollView.contentOffset.y <= .zero {
            if currentPosition == highestPosition, isNewScrollViewSession {
                isNewScrollViewSession = false

                startHeight = currentHeight
                sender.setTranslation(.zero, in: sender.view)
            }

            panGestureRecognizerAction(sender)
        }
    }

    // MARK: - Private. Help

    private func nearestPosition(to currentHeight: CGFloat) -> ContainerMoverPosition {
        guard let dataSource = dataSource else { assertionFailure("data source is required"); return .hidden }
        guard !allowedPositions.isEmpty else { assertionFailure("allowed positions is empty"); return .hidden }
        guard allowedPositions.count > 1 else { return allowedPositions[0] }

        var positionHeights: [ContainerMoverPosition: CGFloat] = [:]
        allowedPositions.forEach { positionHeights[$0] = dataSource.containerMover(self, heightFor: $0) }

        let sortedAllowedPositions = allowedPositions.sorted(by: { $0.rawValue < $1.rawValue })
        for index in 0 ..< sortedAllowedPositions.count - 1 {
            let currPosition = sortedAllowedPositions[index]
            let nextPosition = sortedAllowedPositions[index + 1]

            guard let currHeight = positionHeights[currPosition] else { continue }
            guard let nextHeight = positionHeights[nextPosition] else { continue }

            if isCurrentHeightBetween(currentHeight, height1: currHeight, height2: nextHeight) {
                if currentPosition == currPosition {
                    return newPositionIfKoefDistanceComplete(currentHeight, anchorHeight: currHeight, anchorPosition: currPosition)
                } else {
                    return newPositionIfKoefDistanceComplete(currentHeight, anchorHeight: nextHeight, anchorPosition: nextPosition)
                }
            } else if currPosition == sortedAllowedPositions.first, currentHeight > currHeight {
                return currPosition
            } else if nextPosition == sortedAllowedPositions.last, currentHeight < nextHeight {
                return nextPosition
            }
        }

        return currentPosition
    }

    private func isCurrentHeightBetween(_ currentHeight: CGFloat, height1: CGFloat, height2: CGFloat) -> Bool {
        let maxHeight = max(height1, height2)
        let minHeight = min(height1, height2)
        return minHeight <= currentHeight && currentHeight <= maxHeight
    }

    private func newPositionIfKoefDistanceComplete(_ height: CGFloat, anchorHeight: CGFloat, anchorPosition: ContainerMoverPosition) -> ContainerMoverPosition {
        guard let dataSource = dataSource else { assertionFailure("data source is required"); return anchorPosition }

        let potentialNewPosition = height < anchorHeight ? nextPosition(anchorPosition) : prevPosition(anchorPosition)
        guard potentialNewPosition != anchorPosition else { return anchorPosition }

        let anchorPositionHeight = dataSource.containerMover(self, heightFor: anchorPosition)
        let potentialNewPositionHeight = dataSource.containerMover(self, heightFor: potentialNewPosition)

        let maxHeight = max(anchorPositionHeight, potentialNewPositionHeight)
        let minHeight = min(anchorPositionHeight, potentialNewPositionHeight)

        let distance = abs(height - anchorHeight)
        let fullDistance = maxHeight - minHeight

        return fullDistance * koefToMoveNextPosition < distance ? potentialNewPosition : anchorPosition
    }

    private func nextPosition(_ position: ContainerMoverPosition) -> ContainerMoverPosition {
        guard var potentialPosition = ContainerMoverPosition(rawValue: position.rawValue + 1) else { return position }

        while !allowedPositions.contains(potentialPosition) {
            guard let nextPosition = ContainerMoverPosition(rawValue: potentialPosition.rawValue + 1) else { return position }
            potentialPosition = nextPosition
        }

        return potentialPosition
    }

    private func prevPosition(_ position: ContainerMoverPosition) -> ContainerMoverPosition {
        guard var potentialPosition = ContainerMoverPosition(rawValue: position.rawValue - 1) else { return position }

        while !allowedPositions.contains(potentialPosition) {
            guard let prevPosition = ContainerMoverPosition(rawValue: potentialPosition.rawValue - 1) else { return position }
            potentialPosition = prevPosition
        }

        return potentialPosition
    }

    private func bouncyHeightIfNeeded(_ height: CGFloat, minHeight: CGFloat, maxHeight: CGFloat) -> CGFloat {
        guard height < minHeight || height > maxHeight else { return height }

        if height < minHeight {
            return minHeight - (minHeight - height) * 0.1
        } else {
            return maxHeight + (height - maxHeight) * 0.1
        }
    }
}
