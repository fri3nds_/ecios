//
//  ContainerMoverCoordinator.swift
//  EldoSales
//
//  Created by Bondar Nikita on 21.06.2021.
//

import Foundation

/**
    Внутренние параметры для анимации внутри coordinator
 */
struct ContainerMoverCoordinatorContext {
    // MARK: - Properties

    let animationDuration: TimeInterval
}

/**
    Протокол для применения кастомной анимации вместе с целевой
 */
protocol IContainerMoverCoordinator: AnyObject {
    // MARK: - Interface

    /**
     *  Метод для запуска кастомной анимации во время целевой
     *
     *  - Parameters:
     *      - animation: Кастомная анимация, которая выполнится вместе с целевой
     *      - completion: Окончание кастомной анимации, которое выполнится в конце всех анимаций
     */
    func animate(alongside animation: ((ContainerMoverCoordinatorContext) -> Void)?,
                 completion: ((ContainerMoverCoordinatorContext, Bool) -> Void)?)
}

class ContainerMoverCoordinator: IContainerMoverCoordinator {
    // MARK: - Properties

    private(set) var animation: ((ContainerMoverCoordinatorContext) -> Void)?
    private(set) var completion: ((ContainerMoverCoordinatorContext, Bool) -> Void)?

    // MARK: - Interface

    func animate(alongside animation: ((ContainerMoverCoordinatorContext) -> Void)?,
                 completion: ((ContainerMoverCoordinatorContext, Bool) -> Void)?) {
        self.animation = animation
        self.completion = completion
    }

    func clear() {
        animation = nil
        completion = nil
    }
}
