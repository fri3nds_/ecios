//
//  TitleSeparatorButton.swift
//  EldoSales
//
//  Created by Nikita Bondar on 21.06.2021.
//

import UIKit

protocol DateButtonDelegate: AnyObject {
    // MARK: - Interface

    func dateButton(_ dateButton: DateButton, didTapWith item: DateItem)
}

class DateButton: UIView {
    // MARK: - Delegate

    weak var delegate: DateButtonDelegate?

    // MARK: - Properties

    private let theme: Theme

    private var item: DateItem?

    // MARK: - Views

    private let titleLabel = UILabel()
    private let subtitleLabel = UILabel()
    private let separatorView = UIView()
    private let button = UIButton()

    // MARK: - Init

    init(theme: Theme) {
        self.theme = theme

        super.init(frame: .zero)

        setup()
    }

    @available(*, unavailable, message: "Use init(theme:) instead")
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life cycle

    override func layoutSubviews() {
        super.layoutSubviews()

        performLayout()
    }

    override func sizeThatFits(_ size: CGSize) -> CGSize {
        CGSize(width: size.width, height: 70)
    }

    // MARK: - Interface

    func configure(_ item: DateItem) {
        self.item = item

        titleLabel.text = item.title
        titleLabel.textColor = item.isSelected ? theme.colors.textGreen : theme.colors.textGray
        subtitleLabel.text = item.subtitle
        subtitleLabel.textColor = theme.colors.textBlack
        separatorView.backgroundColor = item.isSelected ? theme.colors.elementGreen : theme.colors.elementLightGray

        setNeedsLayout()
    }

    // MARK: - Private. Setup

    private func setup() {
        backgroundColor = .clear
        clipsToBounds = true

        addSubview(titleLabel)
        addSubview(subtitleLabel)
        addSubview(separatorView)
        addSubview(button)

        titleLabel.font = theme.fonts.font15Bold
        titleLabel.textColor = theme.colors.badgeTitleColor
        subtitleLabel.font = theme.fonts.font17Bold
        subtitleLabel.textColor = theme.colors.textBlack

        button.addTarget(self, action: #selector(buttonDidTap(_:)), for: .touchUpInside)
    }

    // MARK: - Private. Layout

    private func performLayout() {
        titleLabel.pin
            .top()
            .horizontally()
            .sizeToFit(.width)

        subtitleLabel.pin
            .top(to: titleLabel.edge.bottom)
            .horizontally()
            .marginTop(20)
            .sizeToFit(.width)

        separatorView.pin
            .horizontally()
            .bottom()
            .height(1)

        button.pin.all()
    }

    // MARK: - Private. Actions

    @objc
    private func buttonDidTap(_ sender: UIButton) {
        guard let item = item else { return }
        delegate?.dateButton(self, didTapWith: item)
    }
}
