//
//  DateItem.swift
//  EldoSales
//
//  Created by Nikita Bondar on 21.06.2021.
//

import Foundation

class DateItem {
    // MARK: - Properties

    let identifier: String

    let title: String
    var subtitle: String
    var date: Date
    var isSelected: Bool

    // MARK: - Init

    init(identifier: String = UUID().uuidString, title: String, subtitle: String, date: Date, isSelected: Bool) {
        self.identifier = identifier
        self.title = title
        self.subtitle = subtitle
        self.date = date
        self.isSelected = isSelected
    }
}

extension DateItem: Equatable {
    // MARK: - Interface

    static func ==(lhs: DateItem, rhs: DateItem) -> Bool {
        lhs.identifier == rhs.identifier
    }
}
