//
//  DatePicker.swift
//  EldoSales
//
//  Created by Nikita Bondar on 21.06.2021.
//

import UIKit

class DatePicker: UIDatePicker {
    // MARK: Properties

    private var needsToLayoutSubviews: Bool = true

    // MARK: - Init

    init() {
        super.init(frame: .zero)

        if #available(iOS 13.4, *) {
            preferredDatePickerStyle = .wheels
        }
    }

    @available(*, unavailable, message: "Use init() instead")
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Interface

    override func setNeedsLayout() {
        guard needsToLayoutSubviews else { return }
        needsToLayoutSubviews = false

        super.setNeedsLayout()
    }

    func setNeedsToLayoutSubviews() {
        needsToLayoutSubviews = true
        setNeedsLayout()
    }
}
