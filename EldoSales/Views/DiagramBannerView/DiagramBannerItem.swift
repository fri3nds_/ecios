//
//  DiagramBannerItem.swift
//  EldoSales
//
//  Created by Nikita Bondar on 21.06.2021.
//

import Foundation

class DiagramBannerItem {
    // MARK: - Properties

    let mainTitleItem: DiagramBannerTitleItem
    let mainPercent: Double

    let backgroundTitleItem: DiagramBannerTitleItem
    let backgroundPercent: Double

    // MARK: - Init

    init(mainTitleItem: DiagramBannerTitleItem,
         mainPercent: Double,
         backgroundTitleItem: DiagramBannerTitleItem,
         backgroundPercent: Double) {
        self.mainTitleItem = mainTitleItem
        self.mainPercent = mainPercent
        self.backgroundTitleItem = backgroundTitleItem
        self.backgroundPercent = backgroundPercent
    }
}
