//
//  DiagramBannerTitleItem.swift
//  EldoSales
//
//  Created by Nikita Bondar on 21.06.2021.
//

import Foundation

class DiagramBannerTitleItem {
    // MARK: - Properties

    let title: String
    let subtitle: String
    let isMain: Bool

    // MARK: - Init

    init(title: String, subtitle: String, isMain: Bool) {
        self.title = title
        self.subtitle = subtitle
        self.isMain = isMain
    }
}
