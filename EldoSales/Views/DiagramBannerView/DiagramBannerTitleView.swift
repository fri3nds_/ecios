//
//  DiagramBannerTitleView.swift
//  EldoSales
//
//  Created by Nikita Bondar on 21.06.2021.
//

import UIKit

class DiagramBannerTitleView: UIView {
    // MARK: - Properties

    private let theme: Theme

    // MARK: - Views

    private let dotView = UIView()
    private let titleLabel = UILabel()
    private let subtitleLabel = UILabel()

    // MARK: - Init

    init(theme: Theme) {
        self.theme = theme

        super.init(frame: .zero)

        setup()
    }

    @available(*, unavailable, message: "Use init(theme:) instead")
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life cycle

    override func layoutSubviews() {
        super.layoutSubviews()

        performLayout()
    }

    override func sizeThatFits(_ size: CGSize) -> CGSize {
        CGSize(width: size.width, height: 28)
    }

    // MARK: - Interface

    func reloadData(_ item: DiagramBannerTitleItem) {
        titleLabel.text = item.title
        subtitleLabel.text = item.subtitle
        dotView.backgroundColor = item.isMain ? theme.colors.elementGreen : theme.colors.elementLightGray

        setNeedsLayout()
    }

    // MARK: - Private. Setup

    private func setup() {
        backgroundColor = .clear

        addSubview(dotView)
        addSubview(titleLabel)
        addSubview(subtitleLabel)

        dotView.clipsToBounds = true
        dotView.layer.cornerRadius = 5

        titleLabel.font = theme.fonts.font16Bold
        titleLabel.textColor = theme.colors.textBlack

        subtitleLabel.font = theme.fonts.font16Bold
        subtitleLabel.textColor = theme.colors.textBlack
    }

    // MARK: - Private. Layout

    private func performLayout() {
        dotView.pin
            .vCenter()
            .size(10)

        subtitleLabel.pin
            .vCenter()
            .right()
            .sizeToFit()

        titleLabel.pin
            .vCenter()
            .left(to: dotView.edge.right)
            .right(to: subtitleLabel.edge.left)
            .marginLeft(14)
            .marginRight(10)
            .sizeToFit(.width)
    }
}
