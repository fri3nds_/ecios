//
//  DiagramCircleTitlesView.swift
//  EldoSales
//
//  Created by Nikita Bondar on 21.06.2021.
//

import UIKit

class DiagramBannerView: UIView {
    // MARK: - Properties

    private let theme: Theme

    // MARK: - Views & Layers

    private let diagramCircleView: DiagramCircleView

    private let titlesContainerView = UIView()
    private let mainTitleView: DiagramBannerTitleView
    private let backgroundTitleView: DiagramBannerTitleView

    // MARK: - Init

    init(theme: Theme) {
        self.theme = theme
        self.diagramCircleView = DiagramCircleView(theme: theme)

        self.mainTitleView = DiagramBannerTitleView(theme: theme)
        self.backgroundTitleView = DiagramBannerTitleView(theme: theme)

        super.init(frame: .zero)

        setup()
    }

    @available(*, unavailable, message: "Use init(theme:) instead")
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life cycle

    override func layoutSubviews() {
        super.layoutSubviews()

        performLayout()
    }

    // MARK: - Interface

    func reloadData(_ item: DiagramBannerItem, animated: Bool) {
        diagramCircleView.reloadData(title: nil,
                                     subtitle: nil,
                                     mainPercent: item.mainPercent,
                                     backgroundPercent: item.backgroundPercent,
                                     animated: animated)
        mainTitleView.reloadData(item.mainTitleItem)
        backgroundTitleView.reloadData(item.backgroundTitleItem)

        setNeedsLayout()
    }

    // MARK: - Private. Setup

    private func setup() {
        backgroundColor = theme.colors.background
        diagramCircleView.backgroundColor = .clear

        addSubview(diagramCircleView)
        addSubview(titlesContainerView)
        titlesContainerView.addSubview(mainTitleView)
        titlesContainerView.addSubview(backgroundTitleView)
    }

    // MARK: - Private. Layout

    private func performLayout() {
        diagramCircleView.pin
            .vCenter()
            .left()
            .marginLeft(pin.safeArea.left + 16)
            .size(100)

        titlesContainerView.pin
            .vCenter()
            .left(to: diagramCircleView.edge.right)
            .right()
            .marginLeft(14)
            .marginRight(pin.safeArea.right + 16)

        mainTitleView.pin
            .top()
            .horizontally()
            .sizeToFit(.width)

        backgroundTitleView.pin
            .top(to: mainTitleView.edge.bottom)
            .horizontally()
            .sizeToFit(.width)

        titlesContainerView.pin
            .wrapContent(.vertically)
            .vCenter()
    }
}
