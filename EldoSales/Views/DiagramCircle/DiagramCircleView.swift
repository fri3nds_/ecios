//
//  DiagramCircle.swift
//  EldoSales
//
//  Created by Nikita Bondar on 19.06.2021.
//

import UIKit

class DiagramCircleView: UIView {
    // MARK: - Properties

    private var mainCirclePercent: Double = 0
    private var backgroundCirclePercent: Double = 0

    private var lineWidth: CGFloat = 15

    private let theme: Theme

    private var circleSize: CGFloat {
        min(frame.width - lineWidth, frame.height - lineWidth)
    }

    // MARK: - Views & Layers

    private weak var backgroundCircleLayer: CAShapeLayer!
    private weak var mainCircleLayer: CAShapeLayer!

    private let containerView: UIView
    private let titleLabel: UILabel
    private let subtitleLabel: UILabel

    // MARK: - Init

    init(theme: Theme) {
        self.theme = theme
        containerView = UIView()
        titleLabel = UILabel()
        subtitleLabel = UILabel()

        super.init(frame: .zero)

        setup()
    }

    @available(*, unavailable, message: "Use init(theme:) instead")
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life cycle

    override func layoutSubviews() {
        super.layoutSubviews()

        CATransaction.begin()
        CATransaction.setDisableActions(true)

        performLayout()

        CATransaction.commit()
    }

    override func sizeThatFits(_ size: CGSize) -> CGSize {
        size
    }

    // MARK: - Interface

    func reloadData(title: String?,
                    subtitle: String?,
                    mainPercent: Double,
                    backgroundPercent: Double,
                    lineWidth: CGFloat = 15,
                    animated: Bool) {
        self.lineWidth = lineWidth

        titleLabel.text = title
        subtitleLabel.text = subtitle

        mainCirclePercent = mainPercent
        backgroundCirclePercent = backgroundPercent

        backgroundCircleLayer.path = drawCircle(backgroundCirclePercent).cgPath
        mainCircleLayer.path = drawCircle(mainCirclePercent).cgPath

        if animated {
            let backgroundAnimation = CABasicAnimation(keyPath: "strokeEnd")
            backgroundAnimation.fromValue = 0
            backgroundAnimation.toValue = 1
            backgroundAnimation.duration = 0.7
            backgroundAnimation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)

            backgroundCircleLayer.add(backgroundAnimation, forKey: "strokeEnd")

            let mainAnimation = CABasicAnimation(keyPath: "strokeEnd")
            mainAnimation.fromValue = 0
            mainAnimation.toValue = 1
            mainAnimation.duration = 0.7
            backgroundAnimation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)

            mainCircleLayer.add(mainAnimation, forKey: "strokeEnd")
        }

        setNeedsLayout()
    }

    // MARK: - Private. Setup

    private func setup() {
        backgroundColor = theme.colors.background

        addSubview(containerView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(subtitleLabel)

        let backgroundCircleLayer = CAShapeLayer()
        backgroundCircleLayer.fillColor = UIColor.clear.cgColor
        backgroundCircleLayer.strokeColor = theme.colors.elementLightGray.cgColor
        backgroundCircleLayer.lineWidth = lineWidth
        backgroundCircleLayer.lineCap = .round
        layer.addSublayer(backgroundCircleLayer)
        self.backgroundCircleLayer = backgroundCircleLayer

        let mainCircleLayer = CAShapeLayer()
        mainCircleLayer.fillColor = UIColor.clear.cgColor
        mainCircleLayer.strokeColor = theme.colors.elementGreen.cgColor
        mainCircleLayer.lineWidth = lineWidth
        mainCircleLayer.lineCap = .round
        layer.addSublayer(mainCircleLayer)
        self.mainCircleLayer = mainCircleLayer

        containerView.backgroundColor = .clear

        titleLabel.font = theme.fonts.font24Bold
        titleLabel.textColor = theme.colors.diagramItemBackgroundColor
        titleLabel.textAlignment = .center

        subtitleLabel.font = theme.fonts.font13
        subtitleLabel.textColor = theme.colors.textLightGray
        subtitleLabel.textAlignment = .center
    }

    // MARK: - Private. Layout

    private func performLayout() {
        backgroundCircleLayer.pin
            .center()
            .size(circleSize)

        mainCircleLayer.pin
            .center()
            .size(circleSize)

        backgroundCircleLayer.path = drawCircle(backgroundCirclePercent).cgPath
        backgroundCircleLayer.isHidden = backgroundCirclePercent == 0

        mainCircleLayer.path = drawCircle(mainCirclePercent).cgPath
        mainCircleLayer.isHidden = mainCirclePercent == 0

        containerView.pin
            .width(circleSize - lineWidth * 2)

        titleLabel.pin
            .top()
            .horizontally()
            .sizeToFit(.width)

        subtitleLabel.pin
            .top(to: titleLabel.edge.bottom)
            .horizontally()
            .sizeToFit(.width)

        containerView.pin
            .wrapContent(.vertically)
            .center()
    }

    // MARK: - Private. Help

    private func drawCircle(_ percent: Double) -> UIBezierPath {
        let endAngle = min(.pi * 1.5, -.pi / 2 + 2 * .pi * percent)

        let path = UIBezierPath()
        path.move(to: CGPoint(x: circleSize / 2, y: 0))
        path.addArc(withCenter: CGPoint(x: circleSize / 2, y: circleSize / 2),
                    radius: circleSize / 2,
                    startAngle: -.pi / 2,
                    endAngle: CGFloat(endAngle),
                    clockwise: true)

        return path
    }
}
