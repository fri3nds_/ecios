//
//  ColumnDiagramCollectionLayout.swift
//  EldoSales
//
//  Created by Nikita Bondar on 19.06.2021.
//

import UIKit

protocol DiagramColumnCollectionLayoutDelegate: AnyObject {
    // MARK: - Interface

    func collectionView(_ collectionView: UICollectionView, sizeForItemAt indexPath: IndexPath, insets: UIEdgeInsets) -> CGSize
}

class DiagramColumnCollectionLayout: UICollectionViewLayout {
    // MARK: - Properties

    override var collectionViewContentSize: CGSize {
        CGSize(width: (attributesCache.last?.frame.maxX ?? 0) + insets.right, height: collectionView?.frame.height ?? 0)
    }

    var insets: UIEdgeInsets = .zero
    var interItemInset: CGFloat = 0

    private var attributesCache: [UICollectionViewLayoutAttributes] = []

    // MARK: - Interface

    override func prepare() {
        guard let collectionView = collectionView else { fatalError("collection view is nil") }
        guard let delegate = collectionView.delegate as? DiagramColumnCollectionLayoutDelegate else { fatalError("delegate of layout is required") }
        guard let dataSource = collectionView.dataSource else { fatalError("collection view data source is required") }

        attributesCache.removeAll()

        let itemsCount = dataSource.collectionView(collectionView, numberOfItemsInSection: 0)
        var originX: CGFloat = insets.left

        for index in 0 ..< itemsCount {
            let indexPath = IndexPath(row: index, section: 0)
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            let itemSize = delegate.collectionView(collectionView, sizeForItemAt: indexPath, insets: insets)

            attributes.frame = CGRect(x: originX, y: insets.top, width: itemSize.width, height: itemSize.height)
            attributesCache.append(attributes)

            originX += interItemInset
            originX += attributes.frame.width
        }
    }

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        attributesCache.filter { $0.frame.intersects(rect) }
    }
}
