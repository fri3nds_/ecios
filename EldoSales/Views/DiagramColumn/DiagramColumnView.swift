//
//  ColumnDiagramView.swift
//  EldoSales
//
//  Created by Nikita Bondar on 19.06.2021.
//

import UIKit

class DiagramColumnView: UIView {
    // MARK: - Properties

    private let cellRegistrator: CellRegistrator
    private let theme: Theme

    private var items: [DiagramColumnItem] = []
    private var animated: Bool = false

    // MARK: - Views & Layers

    private let dottedLinesView: DottedLinesView
    private let collectionView: UICollectionView

    // MARK: - Init

    init(theme: Theme) {
        self.theme = theme
        dottedLinesView = DottedLinesView(theme: theme)
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: DiagramColumnCollectionLayout())
        cellRegistrator = CellRegistrator(collectionView: collectionView)

        super.init(frame: .zero)

        setup()
    }

    @available(*, unavailable, message: "Use init(theme:) instead")
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life cycle

    override func layoutSubviews() {
        super.layoutSubviews()

        performLayout()
    }

    func willTransition() {
        collectionView.collectionViewLayout.invalidateLayout()
    }

    // MARK: - Interface

    func reloadData(_ items: [DiagramColumnItem],
                    titles: [String],
                    lineColor: UIColor? = nil,
                    interItemInset: CGFloat = 25,
                    itemsInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 25, bottom: 0, right: 25),
                    animated: Bool) {
        if let collectionLayout = collectionView.collectionViewLayout as? DiagramColumnCollectionLayout {
            collectionLayout.interItemInset = interItemInset
            collectionLayout.insets = itemsInsets
        }

        dottedLinesView.lineColor = lineColor ?? theme.colors.diagramLineColor
        dottedLinesView.countOfHorizontalLines = titles.count
        dottedLinesView.horizontalTitles = titles
        dottedLinesView.reloadData()

        self.animated = animated

        self.items = items
        collectionView.reloadData()
    }

    // MARK: - Private. Setup

    private func setup() {
        backgroundColor = theme.colors.background

        addSubview(dottedLinesView)
        addSubview(collectionView)

        collectionView.backgroundColor = .clear
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.alwaysBounceHorizontal = true
        collectionView.delegate = self
        collectionView.dataSource = self

        guard let collectionLayout = collectionView.collectionViewLayout as? DiagramColumnCollectionLayout else { fatalError("layout is wrong") }

        collectionLayout.interItemInset = 25
        collectionLayout.insets = UIEdgeInsets(top: 0, left: collectionLayout.interItemInset, bottom: 0, right: collectionLayout.interItemInset)

        dottedLinesView.delegate = self
    }

    // MARK: - Private. Layout

    private func performLayout() {
        dottedLinesView.pin
            .all()
            .marginTop(20)
            .marginHorizontal(5)
            .marginBottom(40)
        
        collectionView.pin
            .all()
            .marginLeft(dottedLinesView.startHorizontalOffset + 5)
    }
}

extension DiagramColumnView: UICollectionViewDelegate {

}

extension DiagramColumnView: DiagramColumnCollectionLayoutDelegate {
    // MARK: - Interface

    func collectionView(_ collectionView: UICollectionView, sizeForItemAt indexPath: IndexPath, insets: UIEdgeInsets) -> CGSize {
        items[indexPath.row].size(in: CGSize(width: collectionView.frame.width - insets.left - insets.right, height: collectionView.frame.height - insets.top - insets.bottom))
    }
}

extension DiagramColumnView: UICollectionViewDataSource {
    // MARK: - Interface

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        items.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = items[indexPath.row]
        cellRegistrator.registerIfNeeded(item.cellClass, for: item.reuseIdentifier)

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: item.reuseIdentifier, for: indexPath) as? CollectionCell
        cell?.delegate = self
        cell?.setup(with: theme)
        cell?.fill(with: item, animated: animated)

        return cell ?? UICollectionViewCell()
    }
}

extension DiagramColumnView: CollectionCellDelegate {
    // MARK: - Interface

    func collectionCell(_ collectionCell: CollectionCell, didTap item: CollectionItem) {
        print("did tap \(item.identifier)")
    }
}

extension DiagramColumnView: DiagramColumnCellDelegate {
    // MARK: - Interface

    func diagramColumnCellDottedFrame(_ diagramColumnCell: DiagramColumnCell) -> CGRect {
        dottedLinesView.frame
    }
}

extension DiagramColumnView: DottedLinesViewDelegate {
    // MARK: - Interface
    
    func dottedLinesView(_ dottedLinesView: DottedLinesView, didChangeStartOffset offset: CGFloat) {
        performLayout()
    }
}
