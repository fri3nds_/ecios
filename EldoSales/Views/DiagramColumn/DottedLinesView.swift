//
//  DottedLinesView.swift
//  EldoSales
//
//  Created by Nikita Bondar on 19.06.2021.
//

import UIKit

protocol DottedLinesViewDelegate: AnyObject {
    // MARK: - Interface

    func dottedLinesView(_ dottedLinesView: DottedLinesView, didChangeStartOffset offset: CGFloat)
}

class DottedLinesView: UIView {
    // MARK: - Delegate

    weak var delegate: DottedLinesViewDelegate?

    // MARK: - Properties

    var countOfHorizontalLines: Int = 5
    var countOfVerticalLines: Int = 1

    var horizontalTitles: [String] = []

    lazy var lineColor: UIColor = theme.colors.diagramLineColor

    private let theme: Theme
    private(set) var startHorizontalOffset: CGFloat = 0

    // MARK: - Views & Layers

    private var horizontalDottedLines: [CAShapeLayer] = []
    private var verticalDottedLines: [CAShapeLayer] = []

    private var horizontalLabels: [UILabel] = []

    // MARK: - Init

    init(theme: Theme) {
        self.theme = theme
        super.init(frame: .zero)

        setup()
    }

    @available(*, unavailable, message: "Use init(theme:)")
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life cycle

    override func layoutSubviews() {
        super.layoutSubviews()

        performLayout()
    }

    // MARK: - Interface

    func reloadData() {
        reloadLabels()
        reloadLines()

        setNeedsLayout()
    }

    // MARK: - Private. Setup

    private func setup() {
        backgroundColor = theme.colors.background
    }

    // MARK: - Private. Layout

    private func performLayout() {
        var horizontalCurrentOffset: CGFloat = 0
        let horizontalOffset = frame.height / max(1, CGFloat(countOfHorizontalLines - 1))

        for (index, horizontalLine) in horizontalDottedLines.enumerated() {
            horizontalLine.pin
                .top()
                .horizontally()
                .marginTop(horizontalCurrentOffset)
                .marginLeft(startHorizontalOffset)
                .height(1)

            if let label = horizontalLabels.object(at: index) {
                label.pin
                    .top()
                    .left()
                    .marginTop(horizontalCurrentOffset - 1 - label.intrinsicContentSize.height / 2)
                    .width(startHorizontalOffset)
                    .sizeToFit(.width)
            }

            horizontalCurrentOffset += horizontalOffset
        }

        var verticalCurrentOffset: CGFloat = startHorizontalOffset
        let verticalOffset = (frame.width - startHorizontalOffset) / max(1, CGFloat(countOfVerticalLines - 1))

        for verticalLine in verticalDottedLines {
            verticalLine.pin
                .left()
                .vertically()
                .marginLeft(verticalCurrentOffset)
                .width(1)

            verticalCurrentOffset += verticalOffset
        }

        drawDottedLines()
    }

    // MARK: - Private. Help

    private func reloadLabels() {
        horizontalLabels.forEach { $0.removeFromSuperview() }
        horizontalLabels.removeAll()

        var maxLabelWidth: CGFloat = 0

        for i in 0 ..< countOfHorizontalLines {
            let label = UILabel()
            label.font = theme.fonts.font12Bold
            label.textColor = theme.colors.textBlue
            label.text = horizontalTitles.object(at: i) ?? ""
            label.textAlignment = .center

            maxLabelWidth = max(maxLabelWidth, label.intrinsicContentSize.width)

            addSubview(label)
            horizontalLabels.append(label)
        }

        startHorizontalOffset = maxLabelWidth + 5
        delegate?.dottedLinesView(self, didChangeStartOffset: startHorizontalOffset)
    }

    private func reloadLines() {
        horizontalDottedLines.forEach { $0.removeFromSuperlayer() }
        horizontalDottedLines.removeAll()

        verticalDottedLines.forEach { $0.removeFromSuperlayer() }
        verticalDottedLines.removeAll()

        for _ in 0 ..< countOfHorizontalLines {
            let dottedLine = CAShapeLayer()
            dottedLine.strokeColor = lineColor.cgColor
            dottedLine.lineWidth = 1
            dottedLine.lineDashPattern = [3, 2]

            layer.addSublayer(dottedLine)
            horizontalDottedLines.append(dottedLine)
        }

        for _ in 0 ..< countOfVerticalLines {
            let dottedLine = CAShapeLayer()
            dottedLine.strokeColor = lineColor.cgColor
            dottedLine.lineWidth = 1
            dottedLine.lineDashPattern = [3, 2]

            layer.addSublayer(dottedLine)
            verticalDottedLines.append(dottedLine)
        }
    }

    private func drawDottedLines() {
        for dottedLine in horizontalDottedLines {
            let path = CGMutablePath()
            let cgPoint = [.zero, CGPoint(x: dottedLine.frame.width, y: 0)]
            path.addLines(between: cgPoint)

            dottedLine.path = path
        }

        for dottedLine in verticalDottedLines {
            let path = CGMutablePath()
            let cgPoint = [.zero, CGPoint(x: 0, y: dottedLine.frame.height)]
            path.addLines(between: cgPoint)

            dottedLine.path = path
        }
    }
}
